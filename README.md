## CodedLight : 3D scanning from images using coded light ##

CodedLight is a library that implements several coded (structured and unstructured)
light methods that can be used to scan objects and produces 3D models of them.

The documentation is accessible [here](http://nicolasmartin3d.bitbucket.org/codedlight/index.html).
