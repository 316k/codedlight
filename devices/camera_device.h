/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CAMERA_DEVICE_H
#define CAMERA_DEVICE_H

#include <opencv2/core/core.hpp>
#include "../core/factory.h"
#include "../core/export.h"

namespace cl3ds
{
    // needed ?
    enum
    {
        CAPTURE_FORMAT_MONO8   = 0,            // Monochrome, 8 bits
        CAPTURE_FORMAT_MONO16  = 1,            // Monochrome, 16 bits, data is LSB aligned
        CAPTURE_FORMAT_BAYER8  = 2,            // Bayer-color, 8 bits
        CAPTURE_FORMAT_BAYER16 = 3,            // Bayer-color, 16 bits, data is LSB aligned
        CAPTURE_FORMAT_RGB8    = 4,            // RGB, 8 bits x 3
        CAPTURE_FORMAT_RGB16   = 5,            // RGB, 16 bits x 3, data is LSB aligned
        CAPTURE_FORMAT_YUV411  = 6,            // YUV 411
        CAPTURE_FORMAT_YUV422  = 7,            // YUV 422
        CAPTURE_FORMAT_YUV444  = 8,            // YUV 444
        CAPTURE_FORMAT_BGR8    = 9,            // BGR, 8 bits x 3
        CAPTURE_FORMAT_RGBA8   = 10,           // RGBA, 8 bits x 4
        CAPTURE_FORMAT_BGRA8   = 11,           // BGRA, 8 bits x 4
        CAPTURE_FORMAT_MONO12  = 12,           // Monochrome, 12 bits,
        CAPTURE_FORMAT_BAYER12 = 13            // Bayer-color, 12 bits, packed
    };

    class CL3DS_DECLSPEC CameraDevice : public Object
    {
        public:
            static Factory<std::string, CameraDevice> * classFactory();

            virtual ~CameraDevice();

            virtual bool open(int);
            virtual void close();
            virtual bool isOpened() const;

            virtual bool get(cv::Mat &image);

            virtual bool setProperty(int    propId,
                                     double value);
            virtual double getProperty(int propId);

            virtual bool synchronized();
            virtual bool setSynchronized(bool);

            void setExposureTime(double ms);
            double exposureTime() const;
            static double defaultExposureTime();

            void setRepeats(int repeats);
            int repeats() const;
            static int defaultRepeats();

            void setRetries(int retries);
            int retries() const;
            static int defaultRetries();

        protected:
            struct AbstractPrivate;

            CameraDevice(AbstractPrivate *ptr,
                         int              retries=defaultRetries(),
                         double           exposureTime=defaultExposureTime(),
                         int              repeats=defaultRepeats());

        protected:
            void setPrivateImplementation(AbstractPrivate *pimpl);
            AbstractPrivate * privateImplementation();
            const AbstractPrivate * privateImplementation() const;

        private:
            struct Private;
    };
}

#endif // CAMERA_DEVICE_H
