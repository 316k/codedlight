/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODBUS_TRANSLATION_STAGE_H
#define MODBUS_TRANSLATION_STAGE_H

#include "../core/config.h"
#ifdef HAVE_MODBUS

#include "translation_stage.h"

namespace cl3ds
{
    class CL3DS_DECLSPEC ModbusTranslationStage : public TranslationStage
    {
        public:
            ModbusTranslationStage(int device=0);
            ~ModbusTranslationStage();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("modbus_stage")
    };
}

#endif

#endif // MODBUS_TRANSLATION_STAGE_H
