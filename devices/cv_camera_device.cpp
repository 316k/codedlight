/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include "../core/parameter.h"
#include "camera_device_p.h"
#include "cv_camera_device.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct CvCameraDevice::Private : public AbstractPrivate
    {
        Private(CameraDevice *ptr,
                int           device) :
            AbstractPrivate(ptr)
        {
            paramDevice = TypeParameter<int>("device",
                                             "Channel id of the device (see OpenCV's doc)",
                                             CvCameraDevice::defaultDevice());
            open(device);
        }
        ~Private();

        bool read(const FileNode &fn)
        {
            paramDevice.read(fn);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramDevice.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            paramDevice.configure(is, os);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramDevice;
        }

        bool open(int device)
        {
            paramDevice = device;

            capture.release();
            if (!capture.open(device))
            {
                logWarning() << "Unable to open the device id" << device;

                return false;
            }

            return true;
        }

        void close()
        {
            capture.release();
        }

        bool isOpened() const
        {
            return capture.isOpened();
        }

        bool get(cv::Mat &image)
        {
            return capture.read(image);
        }

        bool setProperty(int    propId,
                         double value)
        {
            return capture.set(propId, value);
        }

        double getProperty(int propId)
        {
            return capture.get(propId);
        }

        bool synchronized()
        {
            return false;
        }

        bool setSynchronized(bool)
        {
            return false;
        }

        TypeParameter<int> paramDevice;
        VideoCapture       capture;

        CL3DS_PUB_IMPL(CvCameraDevice)
    };

    CvCameraDevice::CvCameraDevice(int    device,
                                   int    retries,
                                   double exposureTime,
                                   int    repeats) :
        CameraDevice(new Private(this, device), retries, exposureTime, repeats)
    { }

    CvCameraDevice::~CvCameraDevice()
    { }

    int CvCameraDevice::device() const
    {
        CL3DS_PRIV_CONST_PTR(CvCameraDevice);

        return priv->paramDevice;
    }

    void CvCameraDevice::setDevice(int device)
    {
        CL3DS_PRIV_PTR(CvCameraDevice);

        priv->open(device);
    }

    int CvCameraDevice::defaultDevice()
    {
        return 0;
    }

    CvCameraDevice::Private::~Private()
    { }
}
