/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_CAMERA_DEVICE_H
#define CV_CAMERA_DEVICE_H

#include "camera_device.h"

namespace cl3ds
{
    class CL3DS_DECLSPEC CvCameraDevice : public CameraDevice
    {
        public:
            CvCameraDevice(int    device=defaultDevice(),
                           int    retries=defaultRetries(),
                           double exposureTime=defaultExposureTime(),
                           int    repeats=defaultRepeats());
            ~CvCameraDevice();

            /** OpenCV's camera indice, see their doc. */
            int device() const;
            void setDevice(int device);
            static int defaultDevice();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("cv_cam")
    };
}

#endif /* #ifndef CV_CAMERA_DEVICE_H */
