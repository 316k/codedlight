/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../core/common.h"
#include <mvg/logger.h>
#include <modbus.h>
#ifndef OS_WIN
#include <termios.h>
#endif
#include <errno.h>

#include "../core/parameter.h"
#include "translation_stage_p.h"
#include "modbus_translation_stage.h"

using namespace std;
using namespace cv;

namespace cl3ds
{

    struct ModbusTranslationStage::Private : public AbstractPrivate
    {
        Private(ModbusTranslationStage *ptr,
                int) :
            AbstractPrivate(ptr),
            ctx(0)
        {
            paramPath = TypeParameter<string>("path",
                                              "path to the device associated to the modbus.\n"
                                              "on Linux, it should look like /dev/ttyUSB*"
                                              "you should have all the permission to access this device.",
                                              "/dev/something");

            open(0);
        }
        ~Private();

        bool read(const FileNode &fn)
        {
            close();

            paramPath.read(fn);

            open(0);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramPath.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            close();

            paramPath.configure(is, os);

            open(0);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramPath;
        }

        bool open(int index);
        void close() { }
        bool isOpened() const { return ctx != 0; }

        bool reset();
        bool home();
        bool servo();
        bool modbus();

        bool setPositionUnchecked(double mm);
        bool position(double &mm);
        double minPosition() const { return 0; }
        double maxPosition() const { return 400; }

        bool setProperty(int    propId,
                         double value);
        double getProperty(int propId);

        TypeParameter<string> paramPath;
        modbus_t             *ctx;

        CL3DS_PUB_IMPL(ModbusTranslationStage)
    };

    ModbusTranslationStage::Private::~Private()
    {
        close();

        modbus_free(ctx);
        ctx = 0;
    }

    bool ModbusTranslationStage::Private::open(int)
    {
        if (string(paramPath).empty())
        {
            return false;
        }

        ctx = modbus_new_rtu(string(paramPath).c_str(), 9600, 'N', '8', '1');
        modbus_set_slave(ctx, 1);

        if (modbus_connect(ctx) == 0)
        {
            int s = modbus_get_socket(ctx);
#ifndef OS_WIN
            // send a break
            if (tcsendbreak(s, 0) >= 0)
            {

                // send whatever after the break
                uint16_t tab_reg[32];
                modbus_read_registers(ctx, 0x9000, 2, tab_reg);
#endif
            // initialize the commands
            if (modbus() && servo() && home())
            {
                return true;
            }
#ifndef OS_WIN
        }
#endif
        }

        logError() << "Opening failed:" << modbus_strerror(errno);

        modbus_free(ctx);
        ctx = 0;

        return false;
    }

    bool ModbusTranslationStage::Private::reset()
    {
        return true;
    }

    bool ModbusTranslationStage::Private::home()
    {
        uint8_t raw_req[] = { 0x01, 0x05, 0x04, 0x0b, 0xff, 0x00 };
        uint8_t rsp[MODBUS_TCP_MAX_ADU_LENGTH];

        int req_length = modbus_send_raw_request(ctx, raw_req, 6 * sizeof(uint8_t));
        if (req_length < 0)
        {
            return false;
        }

        int rc = modbus_receive_confirmation(ctx, rsp);
        if (rc >= 0)
        {
            bool ok = true;
            for (int i = 0; i < 6; ++i)
            {
                if (rsp[i] != raw_req[i])
                {
                    ok = false;
                    break;
                }
            }

            return ok;
        }
        else if ((rc == -1) && (errno == ETIMEDOUT))
        {
            return false;
        }

        return false;
    }

    bool ModbusTranslationStage::Private::servo()
    {
        uint8_t raw_req[] = { 0x01, 0x05, 0x04, 0x03, 0xff, 0x00 };
        uint8_t rsp[MODBUS_TCP_MAX_ADU_LENGTH];

        int req_length = modbus_send_raw_request(ctx, raw_req, 6 * sizeof(uint8_t));
        if (req_length < 0)
        {
            return false;
        }

        int rc = modbus_receive_confirmation(ctx, rsp);
        if (rc >= 0)
        {
            bool ok = true;
            for (int i = 0; i < 6; ++i)
            {
                if (rsp[i] != raw_req[i])
                {
                    ok = false;
                    break;
                }
            }

            return ok;
        }
        else if ((rc == -1) && (errno == ETIMEDOUT))
        {
            return false;
        }

        return false;
    }

    bool ModbusTranslationStage::Private::modbus()
    {
        uint8_t raw_req[] = { 0x01, 0x05, 0x04, 0x27, 0xff, 0x00 };
        uint8_t rsp[MODBUS_TCP_MAX_ADU_LENGTH];

        int req_length = modbus_send_raw_request(ctx, raw_req, 6 * sizeof(uint8_t));
        if (req_length < 0)
        {
            return false;
        }

        int rc = modbus_receive_confirmation(ctx, rsp);
        if (rc >= 0)
        {
            bool ok = true;
            for (int i = 0; i < 6; ++i)
            {
                if (rsp[i] != raw_req[i])
                {
                    ok = false;
                    break;
                }
            }

            return ok;
        }
        else if ((rc == -1) && (errno == ETIMEDOUT))
        {
            return false;
        }

        return false;
    }

    bool ModbusTranslationStage::Private::setPositionUnchecked(double mm)
    {
        int dest = cvRound(mm * 100);

        const int speed     = 50 * 100;
        uint16_t  tab_reg[] = {
            static_cast<uint16_t>((dest >> 16) & 0xffff),
            static_cast<uint16_t>(dest & 0xffff), // pos
            0x0000, 0x000a, // band
            static_cast<uint16_t>((speed >> 16) & 0xffff),
            static_cast<uint16_t>(speed & 0xffff), // speed
            0x001e // accel
        };

        int rc = modbus_write_registers(ctx, 0x9900, 7, tab_reg);
        sleep_for(250);

        if (rc == -1)
        {
            if (errno == ETIMEDOUT)
            {
                return false;
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    bool ModbusTranslationStage::Private::position(double &mm)
    {
        uint16_t tab_reg[32];
        int      rc = modbus_read_registers(ctx, 0x9000, 2, tab_reg);
        sleep_for(250);

        if (rc == -1)
        {
            if (errno == ETIMEDOUT)
            {
                return false;
            }
            else
            {
                return false;
            }
        }

        mm = cvRound(((tab_reg[0] << 16) | tab_reg[1]) / 100.);

        return true;
    }

    bool ModbusTranslationStage::Private::setProperty(int    propId,
                                                      double value)
    {
        CL3DS_UNUSED(propId);
        CL3DS_UNUSED(value);

        return true;
    }

    double ModbusTranslationStage::Private::getProperty(int propId)
    {
        CL3DS_UNUSED(propId);

        return -1;
    }

    ModbusTranslationStage::ModbusTranslationStage(int device) :
        TranslationStage(new Private(this, device))
    { }

    ModbusTranslationStage::~ModbusTranslationStage()
    { }
}
