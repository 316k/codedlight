/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <set>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <mvg/concurrency.h>
#include <mvg/logger.h>

#include "../core/common.h"
#include "../core/parameter.h"
#include "camera_device_p.h"
#include "gige_camera_device.h"

// #define TEMP_HACK_TSTAMP

#if !defined(WIN32) && !defined(_LINUX)
#define _LINUX
#endif

#if defined _WIN32
#define _STDCALL __stdcall
#else
#define _STDCALL
#endif

#if defined(ARCH_64)
#define _x64 1
#else
#define _x86 1
#endif

#include <PvApi.h>

#if defined WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Winsock2.h>
#endif

#if defined UNIX
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#include <strings.h>
#endif

#if !defined WIN32
#define sprintf_s(dest, len, format, args ...) sprintf(dest, format, args)
#define strcpy_s(dst, len, src) strcpy(dst, src)
#endif

using namespace cv;
using namespace std;

namespace cl3ds
{

#define MAX_CAMERAS 10

    struct GigeCameraDevice::CallbackData
    {
        Ptr< ConcurrentQueue< tPvFrame * > > imagesIn;
        Ptr< ConcurrentQueue< tPvFrame * > > imagesOut;
        GigeCameraDevice::Private           *p;
    };

    struct GigeCameraDevice::Private : public AbstractPrivate
    {
        static tPvCameraInfo cameraList[MAX_CAMERAS];
        static size_t        numCameras;

        class Sentinel
        {
            private:
                Sentinel() { }
                Sentinel(const Sentinel &);
                Sentinel & operator=(const Sentinel &);

            public:
                static Ptr<Sentinel> instance()
                {
                    static Ptr<Sentinel> sentinel;
                    if (sentinel.empty())
                    {
                        // initialize API
                        if (PvInitialize() != ePvErrSuccess)
                        {
                            logError() << "Could not initialize PvAPI";
                        }

                        // wait a little
                        for (int i = 0; i < 10; ++i)
                        {
                            sleep_for(250);
                            numCameras = PvCameraList(cameraList, MAX_CAMERAS, NULL);
                            if (numCameras > 0)
                            {
                                break;
                            }
                        }
                        sentinel = Ptr<Sentinel>(new Sentinel);
                    }

                    return sentinel;
                }
                ~Sentinel()
                {
                    PvUnInitialize();
                }
        };

        struct PvCamera
        {
            unsigned long UID;
            tPvHandle     Handle;
        };

        Private(GigeCameraDevice *device,
                int               paramId);
        ~Private();

        bool open(int index);
        void close();
        bool isOpened() const { return opened; }

        double getProperty(int);
        bool setProperty(int,
                         double);

        bool synchronized()
        {
            return int(getProperty(CAP_PROP_TRIGGER)) ==
                   GigeCameraDevice::CAPTURE_TRIGGER_SYNCIN1;
        }

        bool setSynchronized(bool sync)
        {
            return
                setProperty(CAP_PROP_TRIGGER, (sync ?
                                               GigeCameraDevice::CAPTURE_TRIGGER_SYNCIN1 :
                                               GigeCameraDevice::CAPTURE_TRIGGER_SOFTWARE));
        }

        bool grabFrame();
        bool retrieveFrame(Mat &frame);
        bool get(Mat &image)
        {
            if (grabFrame()) { retrieveFrame(image); }
            else { image.release(); }

            return !image.empty();
        }

        bool write(FileStorage &fs) const;
        bool read(const FileNode &fn);
        bool configure(istream *is,
                       ostream *os);
        size_t hash() const
        {
            return Hasher() << paramCamId;
        }

        void stopCapture();
        bool startCapture();
        bool resizeCaptureFrame(int frameWidth,
                                int frameHeight);

        GigeCameraDevice::CallbackData       data;
        Ptr< ConcurrentQueue< tPvFrame * > > imagesIn;
        Ptr< ConcurrentQueue< tPvFrame * > > imagesOut;
        PvCamera                             Camera;
        tPvErr                               Errcode;
        bool                                 opened;
        Ptr<Sentinel>                        sentinel;

        TypeParameter<int> paramCamId;

        CL3DS_PUB_IMPL(GigeCameraDevice)
    };

    tPvCameraInfo GigeCameraDevice::Private::cameraList[MAX_CAMERAS];
    size_t        GigeCameraDevice::Private::numCameras;

    static void _STDCALL callback(tPvFrame *pFrame)
    {
        // static ElapsedTimer timer;
        GigeCameraDevice::CallbackData *data =
            reinterpret_cast<GigeCameraDevice::CallbackData *>(pFrame->Context[0]);

        // logInfo() << "got frame from driver in" << timer.elapsed() << "ms";

#ifdef TEMP_HACK_TSTAMP
        static unsigned long long ltstamp, tstamp;
        tstamp = (pFrame->TimestampHi << 32) + pFrame->TimestampLo;
        logInfo() << "delta timestamp : "
                  << (tstamp - ltstamp) / 36LL << tstamp
                  << "adding to out" << data->imagesOut->size();
        Mat                *mat = reinterpret_cast<Mat *>(pFrame->Context[1]);
        unsigned long long *p   = mat->ptr<unsigned long long>(0);
        p[0]    = tstamp;
        ltstamp = tstamp;
#endif

        if (pFrame->Status == ePvErrSuccess)
        {
            if (!data->imagesOut->try_push(pFrame))
            {
                // enleve la plus vieille image de sortie et l'ajoute a la queue
                // d'entree
                tPvFrame *dummy;
                data->imagesOut->pop(dummy);
                data->imagesIn->push(dummy);
                // rajoute la nouvelle iamge
                data->imagesOut->push(pFrame);
            }

            // recupere une image d'entree : cela ne peut pas bloquer ..
            tPvFrame *image;
            data->imagesIn->pop(image);

            // et la met en queue
            if (PvCaptureQueueFrame(data->p->Camera.Handle, image,
                                    callback) != ePvErrSuccess)
            {
                // si ca ne reussit pas, on la remet dans la queue d'entree, elle pourra etre
                // reutilisee
                data->imagesIn->push(image);
            }
        }
        else
        {
            if (PvCaptureQueueFrame(data->p->Camera.Handle, pFrame,
                                    callback) != ePvErrSuccess)
            {
                // si ca ne reussit pas, on la remet dans la queue d'entree, elle pourra etre
                // reutilisee
                data->imagesIn->push(pFrame);
            }
        }

        // timer.restart();
    }

    GigeCameraDevice::Private::Private(GigeCameraDevice *ptr,
                                       int               camId) :
        AbstractPrivate(ptr),
        opened(false)
    {
        memset(&this->Camera, 0, sizeof(this->Camera));
        imagesIn  = makePtr< ConcurrentQueue< tPvFrame * > >();
        imagesOut = makePtr< ConcurrentQueue< tPvFrame * > >();

        imagesIn->set_capacity(10);
        for (size_t i = 0; i < imagesIn->capacity(); ++i)
        {
            Mat      *mat   = new Mat;
            tPvFrame *frame = new tPvFrame;
            memset(frame, 0, sizeof(tPvFrame));
            frame->Context[0] = &data;
            frame->Context[1] = mat;
            imagesIn->push(frame);
        }
        data.imagesIn  = imagesIn;
        data.imagesOut = imagesOut;
        data.p         = this;

        // every instance shares the sentinel
        // when the last one is closed, it gets destroyed and calls PvUnInitialize
        sentinel = Sentinel::instance();

        paramCamId = TypeParameter<int>("id",
                                        "Unique id of the camera. If negative, select the (-id)'th available camera.\n"
                                        "i.e an id of -1 select the first camera, -2 the second, and so on",
                                        -1);
        open(camId);
    }

    GigeCameraDevice::Private::~Private()
    {
        close();
    }

    void GigeCameraDevice::Private::close()
    {
        // Stop the acquisition & free the camera
        stopCapture();
        PvCameraClose(Camera.Handle);
    }

    bool GigeCameraDevice::Private::open(int index)
    {
        opened = false;

        paramCamId = index;

        if (numCameras <= 0)
        {
            return false;
        }

        if (index >= 0)
        {
            size_t i = 0;
            for (; i < numCameras; ++i)
            {
                if (static_cast<int>(cameraList[i].UniqueId) == index) { break; }
            }

            if (i >= numCameras)
            {
                return false;
            }
        }
        else
        {
            index = -index - 1;
            if (index >= static_cast<int>(numCameras))
            {
                return false;
            }

            index = static_cast<int>(cameraList[index].UniqueId);
        }

        tPvCameraInfo camInfo;
        tPvIpSettings ipSettings;
        Camera.UID = static_cast<unsigned long>(index);
        if (!PvCameraInfo(Camera.UID,
                          &camInfo) && !PvCameraIpSettingsGet(Camera.UID, &ipSettings))
        { }
        else
        {
            logError() << "could not retrieve camera IP settings.";

            return false;
        }

        tPvErr err = ePvErrAccessDenied;
        // wait for the camera to be available
        for (int i = 0; i < 50; ++i)
        {
            err = PvCameraOpen(Camera.UID, ePvAccessMaster, &(Camera.Handle));
            if (err == ePvErrSuccess) { break; }
            sleep_for(250);
        }

        if (err == ePvErrSuccess)
        {
            tPvUint32 frameWidth, frameHeight;
            char      pixelFormat[256];

            PvAttrUint32Get(Camera.Handle, "Width", &frameWidth);
            PvAttrUint32Get(Camera.Handle, "Height", &frameHeight);

            PvAttrEnumGet(Camera.Handle, "PixelFormat", pixelFormat, 256, NULL);
            if (strcmp(pixelFormat, "Rgb24") == 0)
            {
                if (PvAttrEnumSet(Camera.Handle, "PixelFormat", "Bgr24") != ePvErrSuccess)
                {
                    return false;
                }
                PvAttrEnumGet(Camera.Handle, "PixelFormat", pixelFormat, 256, NULL);
                if (strcmp(pixelFormat, "Bgr24") != 0) { return false; }
            }

            if ((strcmp(pixelFormat, "Mono8") != 0) &&
                (strcmp(pixelFormat, "Mono16") != 0) &&
                (strcmp(pixelFormat, "Bgr24") != 0))
            {
                return false;
            }

            if (!resizeCaptureFrame(static_cast<int>(frameWidth),
                                    static_cast<int>(frameHeight)))
            {
                return false;
            }

            // Determine the maximum packet size supported by the system (ethernet adapter)
            // and then configure the camera to use this value.  If the system's NIC only supports
            // an MTU of 1500 or lower, this will automatically configure an MTU of 1500.
            // 8228 is the optimal size described by the API in order to enable jumbo frames
            /*
             *   unsigned long maxSize = 8228;
             *   if (PvCaptureAdjustPacketSize(Camera.Handle,maxSize) != ePvErrSuccess)
             *   return false;
             */

            if (!startCapture())
            {
                return false;
            }

            opened = true;

            return true;
        }

        logError() << "Error cannot open camera";

        return false;
    }

    bool GigeCameraDevice::Private::grabFrame()
    {
        if (int(getProperty(CAP_PROP_TRIGGER)) ==
            GigeCameraDevice::CAPTURE_TRIGGER_SOFTWARE)
        {
            // software trigger camera
            if (PvCommandRun(Camera.Handle, "FrameStartTriggerSoftware") != ePvErrSuccess)
            {
                return false;
            }
        }

        return true;
    }

    bool GigeCameraDevice::Private::retrieveFrame(Mat &frame)
    {
        tPvFrame *image;
        // with a timeout of 1 sec
        // logInfo() << "getting an image out of" << imagesOut->size();
        if (!imagesOut->timeout_pop(image, 1000000UL))
        {
            // logInfo() << "\ttimed out";
            frame.release();

            return false;
        }

        Mat *mat = reinterpret_cast<Mat *>(image->Context[1]);
        mat->copyTo(frame);
        imagesIn->push(image);

        return true;
    }

    // set camera or driver attribute based on string value
    static bool String2Value(tPvHandle   aCamera,
                             const char *aLabel,
                             tPvDatatype aType,
                             const char *aValue)
    {
        switch (aType)
        {
            case ePvDatatypeString:
            {
                if (strcmp(aLabel, "CameraName") == 0) { return true; }

                return PvAttrStringSet(aCamera, aLabel, aValue) == ePvErrSuccess;
            }
            case ePvDatatypeEnum:
            {
                return PvAttrEnumSet(aCamera, aLabel, aValue) == ePvErrSuccess;
            }
            case ePvDatatypeUint32:
            {
                tPvUint32 lValue = static_cast<tPvUint32>(atol(aValue));
                tPvUint32 lMin, lMax;

                if (PvAttrRangeUint32(aCamera, aLabel, &lMin, &lMax) == ePvErrSuccess)
                {
                    if (lMin > lValue)
                    {
                        lValue = lMin;
                    }
                    else if (lMax < lValue)
                    {
                        lValue = lMax;
                    }

                    return PvAttrUint32Set(aCamera, aLabel, lValue) == ePvErrSuccess;
                }
                else
                {
                    return false;
                }
            }
            case ePvDatatypeFloat32:
            {
                tPvFloat32 lValue = (tPvFloat32)atof(aValue);
                tPvFloat32 lMin, lMax;

                if (PvAttrRangeFloat32(aCamera, aLabel, &lMin, &lMax) == ePvErrSuccess)
                {
                    if (lMin > lValue)
                    {
                        lValue = lMin;
                    }
                    else if (lMax < lValue)
                    {
                        lValue = lMax;
                    }

                    return PvAttrFloat32Set(aCamera, aLabel, lValue) == ePvErrSuccess;
                }
                else
                {
                    return false;
                }
            }
            case ePvDatatypeBoolean:
            {
                if (!(strcmp(aValue, "true")))
                {
                    return PvAttrBooleanSet(aCamera, aLabel, true) == ePvErrSuccess;
                }
                else
                {
                    return PvAttrBooleanSet(aCamera, aLabel, false) == ePvErrSuccess;
                }
            }
            case ePvDatatypeUnknown:
            case ePvDatatypeCommand:
            case ePvDatatypeRaw:
            case ePvDatatypeInt64:
            case __ePvDatatypeforce_32:

                return false;
        }

        return true;
    }

    // encode the value of a given attribute in a string
    static bool Value2String(tPvHandle     aCamera,
                             const char   *aLabel,
                             tPvDatatype   aType,
                             char         *aString,
                             unsigned long aLength)
    {
        switch (aType)
        {
            case ePvDatatypeString:
            {
                return PvAttrStringGet(aCamera, aLabel, aString, aLength,
                                       NULL) == ePvErrSuccess;
            }
            case ePvDatatypeEnum:
            {
                return PvAttrEnumGet(aCamera, aLabel, aString, aLength,
                                     NULL) == ePvErrSuccess;
            }
            case ePvDatatypeUint32:
            {
                tPvUint32 lValue;

                if (PvAttrUint32Get(aCamera, aLabel, &lValue) == ePvErrSuccess)
                {
                    sprintf_s(aString, aLength, "%lu", lValue);

                    return true;
                }
                else
                {
                    return false;
                }

            }
            case ePvDatatypeFloat32:
            {
                tPvFloat32 lValue;

                if (PvAttrFloat32Get(aCamera, aLabel, &lValue) == ePvErrSuccess)
                {
                    sprintf_s(aString, aLength, "%g", lValue);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            case ePvDatatypeBoolean:
            {
                tPvBoolean lValue;

                if (PvAttrBooleanGet(aCamera, aLabel, &lValue) == ePvErrSuccess)
                {
                    if (lValue)
                    {
                        strcpy_s(aString, aLength, "true");
                    }
                    else
                    {
                        strcpy_s(aString, aLength, "false");
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            case ePvDatatypeUnknown:
            case ePvDatatypeCommand:
            case ePvDatatypeRaw:
            case ePvDatatypeInt64:
            case __ePvDatatypeforce_32:

                return false;
        }

        return true;
    }

    bool GigeCameraDevice::Private::write(FileStorage &fs) const
    {
        paramCamId.write(fs);

        fs << "attributes" << "[";

        tPvAttrListPtr lAttrs;
        tPvUint32      lCount;

        // Get all attributes
        if (PvAttrList(Camera.Handle, &lAttrs, &lCount) != ePvErrSuccess)
        {
            fs << "]";

            return true;
        }

        // Write attributes to file
        for (tPvUint32 i = 0; i < lCount; i++)
        {
            tPvAttributeInfo lInfo;

            const char *label = lAttrs[i];
            if (PvAttrInfo(Camera.Handle, label, &lInfo) == ePvErrSuccess)
            {
                if ((lInfo.Datatype != ePvDatatypeCommand) &&
                    (lInfo.Flags & ePvFlagWrite))
                {
                    char value[128];
                    // get attribute
                    if (Value2String(Camera.Handle, label, lInfo.Datatype, value, 128))
                    {
                        // fs << "{" << "label" << label << "}";
                        // fs << "{" << "value" << value << "}";
                        fs << "[" << label << value << "]";
                        /*
                         *   TypeParameter<string> param(label, "Automatically generated from the
                         * driver, refer to PvAPI manual", value);
                         *   param = value;
                         *   param.write(fs);
                         */
                        // fs << "}";
                    }
                    else
                    {
                        logWarning() << "Cannot write attribute" << label;
                    }
                }
            }
        }

        fs << "]";

        return true;
    }

    bool GigeCameraDevice::Private::read(const FileNode &fn)
    {
        close();

        paramCamId.read(fn);
        open(paramCamId);

        FileNode node = fn["attributes"];
        if (node.type() != FileNode::SEQ)
        {
            logWarning() << "Attributes are not stored in a sequence..."
                         << "This is not supposed to happen";

            return false;
        }

        FileNodeIterator it = node.begin(), it_end = node.end();
        for (; it != it_end; ++it)
        {
            FileNodeIterator it2 = (*it).begin(), it2_end = (*it).end();
            if (it2 == it2_end)
            {
                logWarning() << "Attribute has no label";
                continue;
            }
            string label = *it2++;
            if (it2 == it2_end)
            {
                logWarning() << "Attribute" << label << "has no value";
                continue;
            }
            string value = *it2++;

            tPvAttributeInfo lInfo;
            if (PvAttrInfo(Camera.Handle, label.c_str(), &lInfo) == ePvErrSuccess)
            {
                if ((lInfo.Datatype != ePvDatatypeCommand) &&
                    (lInfo.Flags & ePvFlagWrite))
                {
                    // set attribute
                    // logInfo() << "reading attribute" << label << "with value" << value;
                    if (!String2Value(Camera.Handle, label.c_str(),
                                      lInfo.Datatype, value.c_str()))
                    {
                        logWarning() << "Cannot read attribute" << label;
                    }
                }
            }
        }

        stopCapture();
        startCapture();

        return true;
    }

    bool GigeCameraDevice::Private::configure(istream *in,
                                              ostream *out)
    {
        close();

        paramCamId.configure(in, out);
        open(paramCamId);

        if (!out && !in)
        {
            return true;
        }

        tPvAttrListPtr lAttrs;
        tPvUint32      lCount;

        // Get all attributes
        if (PvAttrList(Camera.Handle, &lAttrs, &lCount) != ePvErrSuccess)
        {
            return true;
        }

        set<string> attributes;

        for (tPvUint32 i = 0; i < lCount; i++)
        {
            tPvAttributeInfo lInfo;
            const char      *label = lAttrs[i];
            if (PvAttrInfo(Camera.Handle, label, &lInfo) == ePvErrSuccess)
            {
                if ((lInfo.Datatype != ePvDatatypeCommand) &&
                    (lInfo.Flags & ePvFlagWrite))
                {
                    attributes.insert(label);
                }
            }
        }

        if (out)
        {
            *out << "Which attributes would you like to configure ?" << endl;
            *out << "Press none to stop." << endl;

            for (set<string>::const_iterator it = attributes.begin(),
                 itE = attributes.end();
                 it != itE;)
            {
                *out << *it;
                if (++it != itE) { *out << ", "; }
                else { *out << endl; }
            }
        }

        string attribute, value;
        if (in)
        {
            for (;;)
            {
                if (out)
                {
                    *out << "Enter the attribute :";
                }
                *in >> attribute;
                if (attribute == "none") { break; }

                set<string>::const_iterator it = attributes.find(attribute);
                if (it == attributes.end())
                {
                    if (out)
                    {
                        *out << "The attribute " << attribute << " does not exist" <<
                            endl;
                    }
                }
                else
                {
                    if (out)
                    {
                        *out << "Enter the value :";
                    }
                    *in >> value;

                    // set attribute
                    tPvAttributeInfo lInfo;
                    if (PvAttrInfo(Camera.Handle, attribute.c_str(),
                                   &lInfo) == ePvErrSuccess)
                    {
                        if (!String2Value(Camera.Handle, attribute.c_str(),
                                          lInfo.Datatype, value.c_str()))
                        {
                            logWarning() << "The value given is probably incorrect";
                        }
                        else
                        {
                            if (out)
                            {
                                *out << "Success!" << endl;
                            }
                        }
                    }
                    else
                    {
                        if (out)
                        {
                            *out << "The attribute " << attribute <<
                                " cannot be configured" << endl;
                        }
                    }
                }
            }
        }

        stopCapture();
        startCapture();

        return true;
    }

    double GigeCameraDevice::Private::getProperty(int property_id)
    {
        tPvUint32 nTemp;

        switch (property_id)
        {
            case CAP_PROP_FRAME_WIDTH:
            {
                PvAttrUint32Get(Camera.Handle, "Width", &nTemp);

                return (double)nTemp;
            }
            case CAP_PROP_FRAME_HEIGHT:
            {
                PvAttrUint32Get(Camera.Handle, "Height", &nTemp);

                return (double)nTemp;
            }
            case CAP_PROP_EXPOSURE:
            {
                PvAttrUint32Get(Camera.Handle, "ExposureValue", &nTemp);

                return (double)nTemp;
            }
            case CAP_PROP_FPS:
            {
                tPvFloat32 nfTemp;
                PvAttrFloat32Get(Camera.Handle, "StatFrameRate", &nfTemp);

                return (double)nfTemp;
            }
            case CAP_PROP_PVAPI_MULTICASTIP:
            {
                char mEnable[2];
                char mIp[11];
                PvAttrEnumGet(Camera.Handle, "MulticastEnable", mEnable, sizeof(mEnable),
                              NULL);
                if (strcmp(mEnable, "Off") == 0)
                {
                    return -1;
                }
                else
                {
                    long int ip;
                    int      a, b, c, d;
                    PvAttrStringGet(Camera.Handle, "MulticastIPAddress", mIp, sizeof(mIp),
                                    NULL);
                    sscanf(mIp, "%d.%d.%d.%d", &a, &b, &c,
                           &d); ip = ((a * 256 + b) * 256 + c) * 256 + d;

                    return (double)ip;
                }
            }
            case CAP_PROP_GAIN:
            {
                PvAttrUint32Get(Camera.Handle, "GainValue", &nTemp);

                return (double)nTemp;
            }
            case CAP_PROP_TRIGGER:
            {
                char trigger[40];
                PvAttrEnumGet(Camera.Handle, "FrameStartTriggerMode", trigger,
                              sizeof(trigger), NULL);
                const char *triggers[] = { "Freerun", "Software", "SyncIn1" };
                int         i          = 0, count = sizeof(triggers);
                for (i = 0; i < count; ++i)
                {
                    if (strcmp(triggers[i], trigger) == 0) { break; }
                }
                if (i < count) { return (double)i; }

                return -1;
            }
            case CAP_PROP_FORMAT:
            {
                char format[40];
                PvAttrEnumGet(Camera.Handle, "PixelFormat", format, sizeof(format), NULL);
                const char *formats[] =
                { "Mono8", "Mono16", 0 /*"Bayer8"*/, 0 /*"Bayer16"*/, 0 /*"Rgb24"*/,
                  0 /*"Rgb48"*/,
                  0 /*"Yuv411"*/, 0 /*"Yuv422"*/, 0 /*"Yuv444"*/,
                  "Bgr24", 0 /*"Rgba32"*/, 0 /*"Bgra32"*/,
                  0 /*"Mono12"*/, 0 /*"Bayer12"*/ };
                int i = 0, count = sizeof(formats);
                for (i = 0; i < count; ++i)
                {
                    if (formats[i] && (strcmp(formats[i], format) == 0)) { break; }
                }
                if (i < count) { return (double)i; }

                return -1;
            }
        }

        return -1.0;
    }

    bool GigeCameraDevice::Private::setProperty(int    property_id,
                                                double value)
    {
        switch (property_id)
        {
            case CAP_PROP_FPS:
            {
                if ((PvAttrFloat32Set(Camera.Handle, "FrameRate",
                                      (tPvFloat32)value) == ePvErrSuccess))
                {
                    break;
                }
                else
                {
                    return false;
                }
            }
            case CAP_PROP_EXPOSURE:
            {
                if ((PvAttrUint32Set(Camera.Handle, "ExposureValue",
                                     (tPvUint32)value) == ePvErrSuccess))
                {
                    break;
                }
                else
                {
                    return false;
                }
            }
            case CAP_PROP_PVAPI_MULTICASTIP:
            {
                if (value < 0)
                {
                    if ((PvAttrEnumSet(Camera.Handle, "MulticastEnable",
                                       "Off") == ePvErrSuccess))
                    {
                        break;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    string ip =
                        cv::format("%d.%d.%d.%d", ((int)value >> 24) & 255,
                                   ((int)value >> 16) & 255, ((int)value >> 8) & 255,
                                   (int)value & 255);
                    if ((PvAttrEnumSet(Camera.Handle, "MulticastEnable",
                                       "On") == ePvErrSuccess) &&
                        (PvAttrStringSet(Camera.Handle, "MulticastIPAddress",
                                         ip.c_str()) == ePvErrSuccess))
                    {
                        break;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            case CAP_PROP_GAIN:
            {
                if (PvAttrUint32Set(Camera.Handle, "GainValue",
                                    (tPvUint32)value) != ePvErrSuccess)
                {
                    return false;
                }
                break;
            }
            case CAP_PROP_TRIGGER:
            {
                const char *triggers[] = { "Freerun", "Software", "SyncIn1" };

                int count = sizeof(triggers);
                if ((value < 0) || (value >= count)) { return false; }

                stopCapture();

                int         triggerMode = int(value);
                const char *trigger     = triggers[triggerMode];
                if (PvAttrEnumSet(Camera.Handle, "FrameStartTriggerMode",
                                  trigger) != ePvErrSuccess)
                {
                    logError() << "Could not set Prosilica Acquisition Mode.";
                    startCapture();

                    return false;
                }

                startCapture();

                break;
            }
            case CAP_PROP_FRAME_WIDTH:
            {
                tPvUint32 currHeight;

                PvAttrUint32Get(Camera.Handle, "Height", &currHeight);

                stopCapture();
                // Reallocate Frames
                if (!resizeCaptureFrame(static_cast<int>(value),
                                        static_cast<int>(currHeight)))
                {
                    startCapture();

                    return false;
                }

                startCapture();

                break;
            }
            case CAP_PROP_FRAME_HEIGHT:
            {
                tPvUint32 currWidth;

                PvAttrUint32Get(Camera.Handle, "Width", &currWidth);

                stopCapture();

                // Reallocate Frames
                if (!resizeCaptureFrame(static_cast<int>(currWidth),
                                        static_cast<int>(value)))
                {
                    startCapture();

                    return false;
                }

                startCapture();

                break;
            }
            case CAP_PROP_FORMAT:
            {
                const char *formats[] =
                { "Mono8", "Mono16", 0 /*"Bayer8"*/, 0 /*"Bayer16"*/, 0 /*"Rgb24"*/,
                  0 /*"Rgb48"*/,
                  0 /*"Yuv411"*/, 0 /*"Yuv422"*/, 0 /*"Yuv444"*/,
                  "Bgr24", 0 /*"Rgba32"*/, 0 /*"Bgra32"*/,
                  0 /*"Mono12"*/, 0 /*"Bayer12"*/ };
                int nbFormats = sizeof(formats);
                if ((value < 0) || (value >= nbFormats)) { return false; }

                const char *format = formats[int(value)];
                if (!format) { return false; }

                tPvUint32 currHeight;
                PvAttrUint32Get(Camera.Handle, "Height", &currHeight);

                tPvUint32 currWidth;
                PvAttrUint32Get(Camera.Handle, "Width", &currWidth);

                if (PvAttrEnumSet(Camera.Handle, "PixelFormat", format) != ePvErrSuccess)
                {
                    return false;
                }

                stopCapture();
                // Reallocate Frames
                if (!resizeCaptureFrame(static_cast<int>(currWidth),
                                        static_cast<int>(currHeight)))
                {
                    startCapture();

                    return false;
                }

                startCapture();

                break;
            }
            default:

                return false;
        }

        return true;
    }

    bool GigeCameraDevice::Private::startCapture()
    {
        // Start the camera
        if (PvCaptureStart(Camera.Handle) != ePvErrSuccess)
        {
            logError() << "Could not start the camera.";

            return false;
        }

        // Queue all frames
        const unsigned int capacity = imagesIn->capacity();
        for (unsigned int i = 0; i < capacity; ++i)
        {
            tPvFrame *frame;
            imagesIn->pop(frame);

            if (PvCaptureQueueFrame(Camera.Handle, frame, callback) != ePvErrSuccess)
            {
                logError() << "Could not queue a frame.";

                return false;
            }
        }

        // Set the camera to capture continuously
        if (PvAttrEnumSet(Camera.Handle, "AcquisitionMode",
                          "Continuous") != ePvErrSuccess)
        {
            logError() << "Could not set PvAPI Acquisition Mode.";

            return false;
        }

        // Start the acquisition
        if (PvCommandRun(Camera.Handle, "AcquisitionStart") != ePvErrSuccess)
        {
            logError() << "Could not start PvAPI acquisition.";

            return false;
        }

        return true;
    }

    void GigeCameraDevice::Private::stopCapture()
    {
        PvCommandRun(Camera.Handle, "AcquisitionStop");
        sleep_for(200);
        Mat img;
        while (retrieveFrame(img))
        { }
        PvCaptureQueueClear(Camera.Handle);
        PvCaptureEnd(Camera.Handle);
    }

    bool GigeCameraDevice::Private::resizeCaptureFrame(int frameWidth,
                                                       int frameHeight)
    {
        char      pixelFormat[256];
        tPvUint32 frameSize;
        tPvUint32 sensorHeight;
        tPvUint32 sensorWidth;

        if (PvAttrUint32Get(Camera.Handle, "SensorWidth", &sensorWidth) != ePvErrSuccess)
        {
            return false;
        }

        if (PvAttrUint32Get(Camera.Handle, "SensorHeight",
                            &sensorHeight) != ePvErrSuccess)
        {
            return false;
        }

        // Cap out of bounds widths to the max supported by the sensor
        if ((frameWidth < 0) || ((tPvUint32)frameWidth > sensorWidth))
        {
            frameWidth = static_cast<int>(sensorWidth);
        }

        if ((frameHeight < 0) || ((tPvUint32)frameHeight > sensorHeight))
        {
            frameHeight = static_cast<int>(sensorHeight);
        }

        if (PvAttrUint32Set(Camera.Handle, "Height",
                            static_cast<tPvUint32>(frameHeight)) != ePvErrSuccess)
        {
            return false;
        }

        if (PvAttrUint32Set(Camera.Handle, "Width",
                            static_cast<tPvUint32>(frameWidth)) != ePvErrSuccess)
        {
            return false;
        }

        PvAttrEnumGet(Camera.Handle, "PixelFormat", pixelFormat, 256, NULL);

        if ((strcmp(pixelFormat, "Mono8") != 0) &&
            (strcmp(pixelFormat, "Mono16") != 0) &&
            (strcmp(pixelFormat, "Bgr24") != 0))
        {
            return false;
        }

        PvAttrUint32Get(Camera.Handle, "TotalBytesPerFrame", &frameSize);

        // imagesIn.clear();
        const unsigned int capacity = imagesIn->capacity();
        for (unsigned int i = 0; i < capacity; ++i)
        {
            tPvFrame *frame;
            imagesIn->pop(frame);

            Mat *mat = reinterpret_cast<Mat *>(frame->Context[1]);

            if (strcmp(pixelFormat, "Mono8") == 0)
            {
                mat->create(Size(int(frameWidth), int(frameHeight)), CV_8U);
            }
            else if (strcmp(pixelFormat, "Mono16") == 0)
            {
                mat->create(Size(int(frameWidth), int(frameHeight)), CV_16U);
            }
            else if (strcmp(pixelFormat, "Bgr24") == 0)
            {
                mat->create(Size(int(frameWidth), int(frameHeight)), CV_8UC3);
            }

            frame->ImageBuffer     = mat->data;
            frame->ImageBufferSize =
                static_cast<unsigned long>(mat->total() * mat->elemSize());

            imagesIn->push(frame);
        }

        return true;
    }

    // Initialize camera input
    GigeCameraDevice::GigeCameraDevice(int    camId,
                                       int    retries,
                                       double exposureTime,
                                       int    repeats) :
        CameraDevice(new Private(this, camId), retries, exposureTime, repeats)
    { }

    GigeCameraDevice::~GigeCameraDevice()
    { }

    int GigeCameraDevice::camId() const
    {
        CL3DS_PRIV_CONST_PTR(GigeCameraDevice);

        return priv->paramCamId;
    }

    void GigeCameraDevice::setCamId(int camId)
    {
        CL3DS_PRIV_PTR(GigeCameraDevice);

        priv->open(camId);
    }

    int GigeCameraDevice::defaultCamId()
    {
        return 0;
    }

}
