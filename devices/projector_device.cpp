/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include "../core/config.h"

#if defined HAVE_QT
#include <QApplication>
#include <QCursor>
#include <QDesktopWidget>
#endif

#include "../core/parameter.h"
#include "../patterns/gamma_filter.h"
#include "../patterns/seq_buffer.h"
#include "projector_device.h"
#include "projector_device_p.h"
#include "projector_devices.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
#define PRIV_PTR Private * const priv = \
    static_cast<Private *>(Object::privateImplementation());
#define PRIV_CONST_PTR const Private * const priv = \
    static_cast<const Private *>(Object::privateImplementation());

    struct ProjectorDevice::Private : public Object::AbstractPrivate
    {
        Private(ProjectorDevice                  *ptr,
                ProjectorDevice::AbstractPrivate *childPtr,
                double                            exposureTime,
                double                            gamma) :
            Object::AbstractPrivate(ptr),
            impl(childPtr)
        {
            paramExposure = TypeParameter<double>("exposure",
                                                  "Time in microseconds each pattern should be exposed. "
                                                  "Note that both the projector and camera exposures should be "
                                                  "coherent. "
                                                  "The projector will start exposing the next pattern after "
                                                  "exposure microseconds."
                                                  "Thus you should always have a longer projector exposure than "
                                                  "camera's to account for the delay between camera acquisition.",
                                                  ProjectorDevice::defaultExposureTime(),
                                                  makePtr< PositiveConstraint<double> >());
            paramExposure = exposureTime;

            paramGamma = TypeParameter<double>("gamma",
                                               "Gamma correction coefficient (>=1.)",
                                               ProjectorDevice::defaultGamma(),
                                               makePtr< PositiveConstraint<double> >(3.5));
            setGamma(gamma);
        }
        ~Private();

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Object constructor";
            }
        }

        void setFps(double fps)
        {
            while (fps > 1 && !impl->setProperty(CAP_PROP_FPS, fps))
            {
                logDebug() << "Trying to set the fps to" << fps;
                fps--;
            }
            if (fps < 1)
            {
                logDebug() << "Unable to set the fps or find an appropriate fps";
            }
        }

        bool read(const FileNode &fn)
        {
            paramExposure.read(fn);
            paramGamma.read(fn);

            checkImplPtr();

            impl->read(fn);

            setExposureTime(paramExposure);
            setGamma(paramGamma);
            // adjust the projector's fps (if available) to be consistent with the exposure
            double fps = 1E6 / paramExposure;
            setFps(fps);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramExposure.write(fs);
            paramGamma.write(fs);

            checkImplPtr();

            impl->write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            paramExposure.configure(is, os);
            paramGamma.configure(is, os);

            checkImplPtr();

            impl->configure(is, os);

            setExposureTime(paramExposure);
            setGamma(paramGamma);
            // adjust the projector's fps (if available) to be consistent with the exposure
            double fps = 1E6 / paramExposure;
            setFps(fps);

            return true;
        }

        size_t hash() const
        {
            checkImplPtr();

            return impl->hash();
        }

        bool open(int device)
        {
            checkImplPtr();

            return impl->open(device);
        }

        void close()
        {
            checkImplPtr();

            impl->close();
        }

        bool isOpened() const
        {
            checkImplPtr();

            return impl->isOpened();
        }

        void setGamma(double gamma)
        {
            gammaFilter.setGamma(gamma);
            paramGamma = gamma;
        }

        void setExposureTime(double exposure)
        {
            paramExposure = exposure;
            setProperty(CAP_PROP_EXPOSURE, exposure);
        }

        // this is needed to de-activate the screen saver !
        void simulateUserInput()
        {
#if defined HAVE_QT
            // create an application in case none was created
            if (!qApp)
            {
                int   *argc = new int[1]; argc[0] = 1;
                char **argv = new char *[1]; argv[0] = new char[1]; argv[0][0] = 0;
                new QApplication(*argc, argv);
            }
            QPoint          pos     = QCursor::pos();
            QDesktopWidget *desktop = qApp->desktop();
            int             screen  = desktop->screenNumber(pos);
            QRect           rect    = desktop->screenGeometry(screen);
            // moving the cursor at random to prevent idle
            QCursor::setPos(QPoint(qrand() % rect.width(), qrand() % rect.height()));
            // putting it back to not bother the user
            QCursor::setPos(pos);
#else
            logWarning() <<
                "Qt is not activated. Impossible to deactivate the screen saver, you may"
                "have to do it manually..";
#endif
        }

        bool put(const Mat &image)
        {
            if (getProperty(DEACTIVATE_SCREENSAVER) > 0)
            {
                simulateUserInput();
            }

            Ptr<SequenceBuffer> seq = makePtr<SequenceBuffer>(image);
            gammaFilter.setGenerator(seq);
            gammaFilter.reset();
            Mat filteredImage = gammaFilter.get();

            checkImplPtr();

            return impl->put(filteredImage);
        }

        bool put(Ptr<Generator> gen)
        {
            if (getProperty(DEACTIVATE_SCREENSAVER) > 0)
            {
                simulateUserInput();
            }

            gammaFilter.setGenerator(gen);
            gammaFilter.reset();

            checkImplPtr();

            return impl->put(&gammaFilter);
        }

        void clear()
        {
            checkImplPtr();

            impl->clear();
        }

        bool setProperty(int    propId,
                         double value)
        {
            checkImplPtr();

            return impl->setProperty(propId, value);
        }

        double getProperty(int propId)
        {
            checkImplPtr();

            return impl->getProperty(propId);
        }

        bool synchronized()
        {
            checkImplPtr();

            return impl->synchronized();
        }

        bool setSynchronized(bool sync)
        {
            checkImplPtr();

            return impl->setSynchronized(sync);
        }

        TypeParameter<double>             paramExposure;
        TypeParameter<double>             paramGamma;
        ProjectorDevice::AbstractPrivate *impl;
        GammaFilter                       gammaFilter;
    };

    ProjectorDevice::Private::~Private()
    {
        delete impl;
    }

    static Ptr< Factory<string, ProjectorDevice> > makeFactory()
    {
        Ptr< Factory<string,
                     ProjectorDevice> > f = makePtr< Factory<string, ProjectorDevice> >();
#ifdef HAVE_QT
        f->reg<QtProjectorDevice>();
#endif
#ifdef HAVE_LCRAPI
        f->reg<LCR4500ProjectorDevice>();
#endif

        return f;
    }

    Factory<string, ProjectorDevice> *ProjectorDevice::classFactory()
    {
        static Ptr< Factory<string, ProjectorDevice> > factory = makeFactory();

        return factory;
    }

    ProjectorDevice::ProjectorDevice(AbstractPrivate *ptr,
                                     double           exposureTime,
                                     double           gamma) :
        Object(new Private(this, ptr, exposureTime, gamma))
    { }

    ProjectorDevice::~ProjectorDevice()
    { }

    void ProjectorDevice::setPrivateImplementation(ProjectorDevice::AbstractPrivate *pimpl)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = pimpl;
    }

    ProjectorDevice::AbstractPrivate *
    ProjectorDevice::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const ProjectorDevice::AbstractPrivate *
    ProjectorDevice::privateImplementation() const
    {
        PRIV_CONST_PTR;

        return priv->impl;
    }

    bool ProjectorDevice::open(int device)
    {
        PRIV_PTR;

        return priv->open(device);
    }

    void ProjectorDevice::close()
    {
        PRIV_PTR;

        priv->close();
    }

    bool ProjectorDevice::isOpened() const
    {
        PRIV_CONST_PTR;

        return priv->isOpened();
    }

    bool ProjectorDevice::put(const Mat &image)
    {
        PRIV_PTR;

        return priv->put(image);
    }

    bool ProjectorDevice::put(Ptr<Generator> gen)
    {
        PRIV_PTR;

        return priv->put(gen);
    }

    void ProjectorDevice::clear()
    {
        PRIV_PTR;

        priv->clear();
    }

    bool ProjectorDevice::setProperty(int    propId,
                                      double value)
    {
        PRIV_PTR;

        return priv->setProperty(propId, value);
    }

    double ProjectorDevice::getProperty(int propId)
    {
        PRIV_PTR;

        return priv->getProperty(propId);
    }

    bool ProjectorDevice::synchronized()
    {
        PRIV_PTR;

        return priv->synchronized();
    }

    bool ProjectorDevice::setSynchronized(bool sync)
    {
        PRIV_PTR;

        return priv->setSynchronized(sync);
    }

    void ProjectorDevice::setExposureTime(double exposure)
    {
        PRIV_PTR;

        priv->setExposureTime(exposure);
    }

    double ProjectorDevice::exposureTime() const
    {
        PRIV_CONST_PTR;

        return priv->paramExposure;
    }

    double ProjectorDevice::defaultExposureTime()
    {
        return 45000;
    }

    void ProjectorDevice::setGamma(double gamma)
    {
        PRIV_PTR;

        priv->setGamma(gamma);
    }

    double ProjectorDevice::gamma() const
    {
        PRIV_CONST_PTR;

        return priv->paramGamma;
    }

    double ProjectorDevice::defaultGamma()
    {
        return 1;
    }

    ProjectorDevice::AbstractPrivate::AbstractPrivate(ProjectorDevice *device) :
        Object::AbstractPrivate(device)
    { }

    ProjectorDevice::AbstractPrivate::~AbstractPrivate()
    { }
}
