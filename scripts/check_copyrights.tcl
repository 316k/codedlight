#!/usr/bin/tclsh

set liste {}
set k [catch {exec git ls-files . | grep -e {\.cpp} -e {\.h} -e {CMakeLists} -e {\.in} -e {\.tpp}} liste]
#set k [catch {exec git diff-index HEAD --name-only} liste]
#if {$k} {
    #puts "Erreur : $liste"
    #exit 1
#}
#
#

set cmake_fid [open [file join [file dirname [file normalize $argv0]] "../CMakeLists.txt"] r]
set cmake_contents [read $cmake_fid]
close $cmake_fid

regexp {CODEDLIGHT_AUTHOR "([^"]*)"} ${cmake_contents} -> author
regexp {CODEDLIGHT_EMAIL "([^"]*)"} ${cmake_contents} -> email
regexp {CODEDLIGHT_YEARS "([^"]*)"} ${cmake_contents} -> years
regexp {CODEDLIGHT_PROJECT "([^"]*)"} ${cmake_contents} -> project
regexp {CODEDLIGHT_DESCRIPTION "([^"]*)"} ${cmake_contents} -> description

set copyright_software "$project"
set copyright_description "$description"
set copyright_holder "$author ($email)"
set copyright_year "$years"

set cpp_license_template {/*
 * %1$s - %2$s.
 * Copyright (C) %3$s %4$s
 *
 * This file is part of %1$s.
 *
 * %1$s is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * %1$s is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with %1$s.  If not, see <http://www.gnu.org/licenses/>.
 */}

set cpp_license_template_bsd3 {/*
 * License Agreement for %1$s
 *
 * Copyright (c) %3$s, %4$s
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */}

set cmake_license_template {#
# %1$s - %2$s.
# Copyright (C) %3$s %4$s
#
# This file is part of %1$s.
#
# %1$s is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# %1$s is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with %1$s.  If not, see <http://www.gnu.org/licenses/>.
#}

set k [catch { set cin [open /dev/tty] } infos]
if {$k} { exit 0 }

set cpp_license [format $cpp_license_template $copyright_software $copyright_description $copyright_year $copyright_holder]
set cpp_license_bsd3 [format $cpp_license_template_bsd3 $copyright_software $copyright_description $copyright_year $copyright_holder]
set cmake_license [format $cmake_license_template $copyright_software $copyright_description $copyright_year $copyright_holder]

set modified 0
foreach f $liste {
    if {[string match "*core/ceres_include.h*" $f]} { continue; }

    set fd [open $f r]
    set data [read  $fd]
    close $fd

    set license ""
    set header ""
    set code ""
    regexp {^[^.]*\.(.*)} $f -> ext

    #check the extension first !
    switch $ext {
        "cpp" -
        "c" -
        "hpp" -
        "h" -
        "h.in" -
        "tpp" {
            regexp {^(/\*.*?\*/)(.*)$} $data -> header code
            if {[string match "*applications/*" $f] || [string match "*test/*" $f]} {
                set license $cpp_license_bsd3
            } else {
                set license $cpp_license
            }
        }
        "txt" {
            if {[string equal [file tail $f] "CMakeLists.txt"]} {
                regexp  {^(#.*?)\n([^#].*)$} $data -> header code
                set license $cmake_license
            }
        }
        "cmake.in" {
            regexp  {^(#.*?)\n([^#].*)$} $data -> header code
            set license $cmake_license
        }
    }

    if {[string equal $license ""]} { continue }

    if {![string equal $header $license]} {
        puts "File $f has a non-valid license."
        puts "Found copyright : <$header>"
        puts "Expected : <$license>"
        puts "Would you like to change it automatically ? \[y/N\]"
        set ans [gets $cin]
        if {[string equal $ans "y"]} {
            set data "${license}${code}"
            set fd [open $f w]
            puts -nonewline $fd $data
            close $fd
            set modified 1
        }
    }
}
if {$modified} {
    puts "At least one file has been automatically modified, check it using git difftool"
    exit 1
}

exit 0
