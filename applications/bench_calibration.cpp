/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <fstream>
#include <limits>
#include <set>

#include <opencv2/highgui/highgui.hpp>
#include <xtclap/CmdLine.h>
#include <mvg/logger.h>
#include <tinytag/tinytag.h>

#include "../patterns/generator.h"
#include "../patterns/phaseshift.h"
#include "../patterns/seq_buffer.h"
#include "../patterns/matcher.h"
#if 0
#include "../devices/translation_stage_device.h"
#include "../devices/camera_device.h"
#include "../devices/projector_device.h"
#endif
#include "../calib/calib.h"

using namespace cv;
using namespace std;
using namespace TCLAP;
using namespace cl3ds;

int main(int   argc,
         char *argv[])
{
    CmdLine cmd("Calibrates a system with a projector and (possibly) multiple cameras "
                "using a linear translation stage "
                "moving a board with fiducial markers printed on them");

    ValueArg<string> tagsImagePathArg("i", "tags-image-path",
                                      "Path to the image of the tags printed on the board.\n"
                                      "This will tell what tags to look for in the image.\n"
                                      "This will also define the 3D positions of the grid, so "
                                      "it should be oriented appropriately.",
                                      true, "", "string",
                                      cmd);

    ValueArg<string> configGeneratorPathArg("g", "config-generator-path",
                                            "Path to the pattern generator configuration used to calibrate."
                                            "Defaults to a phase shift generator.",
                                            false, "", "string",
                                            cmd);

    ValueArg<string> configMatcherPathArg("m", "config-matcher-path",
                                          "Path to the pattern matcher configuration used to calibrate."
                                          "Defaults to a phase shift matcher.",
                                          false, "", "string",
                                          cmd);

    ValueArg<string> stageConfigPathArg("S", "stage-config-path",
                                        "Path to the translation stage configuration."
                                        "Ignored in offline mode.",
                                        false, "", "string",
                                        cmd);

    MultiArg<string> cameraConfigPathsArg("C", "camera-config-path",
                                          "Path to the camera configuration."
                                          "Ignored in offline mode.",
                                          false, "string",
                                          cmd);

    ValueArg<string> projectorConfigPathArg("P", "projector-config-path",
                                            "Path to the projector configuration."
                                            "Ignored in offline mode.",
                                            false, "", "string",
                                            cmd);

    ValueArg<string> cameraTagImagesPathArg("", "camera-imagesTag-path",
                                            "Path to capatured tag camera images in offline mode",
                                            false, "", "string",
                                            cmd);

    ValueArg<string> cameraSTLImagesPathArg("", "camera-imagesSTL-path",
                                            "Path to capatured STL camera images in offline mode",
                                            false, "", "string",
                                            cmd);
    /*
     *   //don't need them : always assume we use PS
     *   ValueArg<string> projectorImagesPathArg("", "projector-images-path"
     *                                        "Path to projected projector images in offline mode",
     *                                        false, "", "string",
     *                                        cmd);
     */

    ValueArg<int> numCamerasArg("", "num-cameras",
                                "Number of cameras to calibrate, ignored in online mode.",
                                false, -1, "int",
                                cmd);

    ValueArg<int> projectorWidthArg("", "proj-width",
                                    "Projector image width, ignored in online mode.",
                                    false, -1, "int",
                                    cmd);

    ValueArg<int> projectorHeightArg("", "proj-height",
                                     "Projector image height, ignored in online mode.",
                                     false, -1, "int",
                                     cmd);

    ValueArg<double> dimensionArg("d", "largest-tag-size",
                                  "This is the size in mm of the largest printed tag.\n"
                                  "This will define the overall scale of the scene.",
                                  false, 25.2, "double",
                                  cmd);

    ValueArg<double> zminArg("", "zmin",
                             "Position (inclusive) where to begin on the stage.\n"
                             "If not given, the minimum position returned by the stage driver "
                             "is assumed.",
                             false, -1, "double",
                             cmd);

    ValueArg<double> zmaxArg("", "zmax",
                             "Position (inclusive) where to end on the stage.\n"
                             "If not given, the maximum position returned by the stage driver "
                             "is assumed.",
                             false, -1, "double",
                             cmd);

    ValueArg<double> zstepArg("", "zstep",
                              "Step size on the Z axis between two shots.\n"
                              "If given, nbsteps is ignored.",
                              false, -1, "double",
                              cmd);

    ValueArg<int> numStepsArg("", "nbsteps",
                              "Number of steps on the Z axis between two shots.\n"
                              "If given, zstep is ignored.",
                              false, -1, "int",
                              cmd);

    ValueArg<string> pointsPatternArg("", "output-points-pattern",
                                      "Pattern to the output file that will contain 2D and 3D points.\n"
                                      "It should be of the form [[<some path>/]<some subpath>/]<some prefix>_%d.xml,"
                                      "the argument will be the index of the camera (#cameras for the proj).\n"
                                      "Both 2D and 3D points will be saved to node points2D, resp. points3D.\n"
                                      "If not given, points are not saved",
                                      false, "", "string",
                                      cmd);

    ValueArg<string> camerasPatternArg("o", "output-camera-pattern",
                                       "Pattern to the output file that will contain camera matrices.\n"
                                       "It should be of the form [[<some path>/]<some subpath>/]<some prefix>_%d.xml,"
                                       "the argument will be the index of the camera (#cameras for the proj).\n"
                                       "If not given, cameras are saved to cam_%d.xml",
                                       true, "cam_%d.xml", "string",
                                       cmd);

    SwitchArg verboseArg("v", "verbose",
                         "Print some debug information during the calibration",
                         cmd, false);

    cmd.parse(argc, argv);

    const string projWinName   = "proj";
    const string camWinName    = "cam";
    const string projOutPrefix = "proj";
    const string camOutPrefix  = "cam";

    Size         projSize;
    vector<Size> camSizes;

    // string         projectorImagesPath = projectorImagesPathArg.getValue();
    string         configGeneratorPath = configGeneratorPathArg.getValue();
    string         configMatcherPath   = configMatcherPathArg.getValue();
    string         tagsImagePath       = tagsImagePathArg.getValue();
    string         stageConfigPath     = stageConfigPathArg.getValue();
    vector<string> cameraConfigPaths   = cameraConfigPathsArg.getValue();
    string         projectorConfigPath = projectorConfigPathArg.getValue();
    string         cameraTagImagesPath = cameraTagImagesPathArg.getValue();
    string         cameraSTLImagesPath = cameraSTLImagesPathArg.getValue();
    size_t         nbCameras           = static_cast<size_t>(numCamerasArg.getValue());
    double         dimension           = dimensionArg.getValue();
    double         zmin                = zminArg.getValue();
    double         zmax                = zmaxArg.getValue();
    double         zstep               = zstepArg.getValue();
    int            nbSteps             = numStepsArg.getValue();
    string         pointsPattern       = pointsPatternArg.getValue();
    string         camerasPattern      = camerasPatternArg.getValue();
    bool           verbose             = verboseArg.getValue();

    projSize = Size(projectorWidthArg.getValue(), projectorHeightArg.getValue());

    if (nbCameras != 1)
    {
        logError() << "Only one camera is supported for the moment";
    }

    bool online;
    if (cameraTagImagesPathArg.isSet() /*&& projectorImagesPath.isSet()*/ &&
        cameraSTLImagesPathArg.isSet() &&
        numCamerasArg.isSet())
    {
        logInfo() << "Offline mode : reading images from the disk";
        online = false;
    }
    else
    {
        logInfo() << "Online mode: capturing and projecting images live";
        if (!stageConfigPathArg.isSet() ||
            !projectorConfigPathArg.isSet() ||
            !cameraConfigPathsArg.isSet())
        {
            logError() <<
                "Online mode requires configuration files for the camera(s), projector and translation stage";
        }
        online = true;
    }

    if (online)
    {
        logError() << "Online mode not supported yet.";
    }

    if (!configGeneratorPathArg.isSet() &&
        !projectorWidthArg.isSet() &&
        !projectorHeightArg.isSet())
    {
        logWarning() <<
            "If the generator config is not given then the projector size is assumed " <<
            " to be 800x600. If it is not the case, you have to specify the projector " <<
            "resolution using --projector-width and --projector-height.";
        projSize = Size(800, 600);
    }

#if 0
    Ptr<TranslationStage>       stage;
    Ptr<ProjectorDevice>        projDevice;
    vector< Ptr<CameraDevice> > camDevices;
    bool                        synchronized = false;
#endif

    FileStorage fs;
    // Generator setup
    Ptr<Generator> generator;
    if (configGeneratorPath.empty())
    {
        int    _shifts[]  = { 9, 9, 9, 9, 9 };
        double _periods[] = { 16, 48, 144, 432, 1296 };
        generator = makePtr<PhaseShiftGenerator>(projSize,
                                                 Generator::BOTH,
                                                 makeVector(_shifts),
                                                 makeVector(_periods));
    }
    else
    {
        if (!fs.open(configGeneratorPath, FileStorage::READ))
        {
            logError() << "Config file could not be found";
        }
        fs["generator"] >> generator;
        projSize = generator->size();
    }

    size_t nbImages = generator->count();

    // Matcher setup
    Ptr<Matcher> matcher;
    if (configMatcherPath.empty())
    {
        matcher = makePtr<PhaseShiftMatcher>(
            dynamicPtrCast<PhaseShiftGenerator>(generator));
    }
    else
    {
        if (!fs.open(configMatcherPath, FileStorage::READ))
        {
            logError() << "Config file could not be found";
        }
        fs["matcher"] >> matcher;
    }

    if (online)
    {
#if 0
        // Stage setup
        if (!fs.open(stageConfigPath, FileStorage::READ))
        {
            logError() << "Config file could not be found";
        }

        FileNode n = fs[TranslationStage::XML_KEYWORD];
        stage = Ptr<TranslationStage>(TranslationStageFactory::instance().
                                      create(n["name"]));

        if (stage.empty())
        {
            logError() << "The translation stage can not be created : " << string(
                n["name"]);

            return 1;
        }

        stage->read(n["params"]);
        if (!*stage)
        {
            logError() << "The translation stage could not be configured";

            return 1;
        }
        fs.release();

        // Projector setup
        projDevice = makePtr<ProjectorDevice>();
        if (!fs.open(projectorConfigPath, FileStorage::READ))
        {
            logError() << "Unable to open projector config file :"
                       << projectorConfigPath;

            return 1;
        }
        projDevice->read(fs[ProjectorDevice::XML_KEYWORD]);
        fs.release();

        // Cameras setup
        nbCameras = static_cast<int>(cameraConfigPaths.size());
        camSizes.resize(nbCameras);
        camDevices.resize(nbCameras);
        for (int i = 0; i < nbCameras; ++i)
        {
            if (!fs.open(cameraConfigPaths[i], FileStorage::READ))
            {
                logError() << "Unable to open camera config file :"
                           << cameraConfigPaths[i];

                return 1;
            }
            camDevices[i] = makePtr<CameraDevice>();
            camDevices[i]->read(fs[CameraDevice::XML_KEYWORD]);
            fs.release();

            camSizes[i].width =
                static_cast<int>(camDevices[i]->driver()->getProperty(CAP_PROP_FRAME_WIDTH));
            camSizes[i].height =
                static_cast<int>(camDevices[i]->driver()->getProperty(
                                     CAP_PROP_FRAME_HEIGHT));
        }
        fs.release();

        ostringstream oss;
        for (int i = 0; i < nbCameras; ++i)
        {
            if (!*camDevices[i])
            {
                logError() << "Could not grab from camera" << i;

                return 1;
            }
        }
        logInfo() << "Grabbing" << nbCameras << "cameras";

        // check to see if we are in sync or async mode
        synchronized = (nbCameras == 1 &&
                        projDevice->driver()->setSynchronized(true));
        if (synchronized)
        {
            for (int i = 0; i < nbCameras; ++i)
            {
                if (!camDevices[i]->driver()->setSynchronized(true))
                {
                    synchronized = false;
                }
            }
        }
        if (!synchronized)
        {
            // makes sure no one is in synchronized mode ...
            projDevice->driver()->setSynchronized(false);
            for (int i = 0; i < nbCameras; ++i)
            {
                camDevices[i]->driver()->setSynchronized(false);
            }

        }

        sleep_for(500); // just to be sure everyone is setup
#endif
    }
    else
    {
        Mat tmp;
        camSizes.resize(nbCameras);
        for (size_t i = 0; i < nbCameras; ++i)
        {
            string path = format(cameraTagImagesPath.c_str(), 0, i);
            tmp = imread(path, -1);
            if (tmp.empty())
            {
                logError() << "Unable to open camera image file :" << path;
            }
            camSizes[i] = tmp.size();
        }
    }

    CameraProjectorLinearStageCalibrator calibrator(camSizes[0], projSize);

    // find the tags in the image
    vector<TinyTagInfo> tags;
    TinyTagDetector     detector;

    Mat tagImage = imread(tagsImagePath, -1);
    detector.detectTags(tags, tagImage);

    size_t nbTags = tags.size();
    if (nbTags < 10)
    {
        // most probably the image need to be inverted ..
        tagImage ^= Scalar::all(255);
        detector.detectTags(tags, tagImage);
        nbTags = tags.size();

        if (nbTags < 10)
        {
            logError() << "Too few tags detected in the image provided... Are you sure"
                " this is the one you printed?";
        }
    }

    map<int, Point2f> gridPoints;
    int               maxSize = 0;
    for (size_t i = 0; i < nbTags; ++i)
    {
        Rect r    = boundingRect(Mat(tags[i].corners()));
        int  size = std::max(r.width, r.height);
        if (size > maxSize)
        {
            maxSize = size;
        }
        float x = tags[i].center().x;
        float y = tags[i].center().y;
        gridPoints[tags[i].id()] = Point2f(x, y);
    }

    logDebug() << dimension << maxSize;
    double scale = dimension / maxSize;

    calibrator.setScale(scale);
    calibrator.setGridPoints(gridPoints);

    // set z range
    if (online)
    {
#if 0
        if (zmin < 0) { zmin = stage->minPosition(); }
        if (zmax < 0) { zmax = stage->maxPosition(); }
#endif
    }
    else
    {
        if ((zmin < 0) || (zmax < 0))
        {
            logError() << "In offline mode, zmin and zmax must be given.";
        }
    }
    if (zstepArg.isSet())
    {
        nbSteps = 0;
        for (double z = zmin; z <= zmax; z += zstep)
        {
            nbSteps++;
        }
    }
    else if (numStepsArg.isSet())
    {
        zstep = (zmax - zmin) / (nbSteps - 1.0);
    }
    else
    {
        logError() << "One of zstep or nbzsteps must be set";
    }

    logDebug() << zmin << zmax << zstep;
    calibrator.setZRange(zmin, zmax, zstep);

    vector< vector<Mat> >       camImages(nbCameras, vector<Mat>(nbImages));
    vector< map<int, Point2f> > camPoints(nbCameras);
    vector<Mat>                 camMatches(nbCameras);
    Mat                         frame, image;

#if 0
    vector<int> camExposures(nbCameras);
    int         projExposure = 0;
#endif

    Mat whiteImage(projSize, CV_8U, Scalar::all(255));

    // move the stage to zmin
#if 0
    if (online) { stage->setPosition(zmin); }
#endif

    const int      operations = nbSteps * 2;
    ProgressLogger prog       = logProgress(operations);

    for (int step = 0; step < nbSteps; ++step)
    {
        logDebug() << "Step" << step;
        logDebug() << "Detecting tags";

        // even if we are in synchronized mode .. there is only one image to project
        // so we act as if we are not synchronized

        if (online)
        {
#if 0
            // projete un pattern blanc pour eclairer les tags
            // we pump the exposure so as to project a still image
            projExposure = static_cast<int>(projDevice->getProperty(CAP_PROP_EXPOSURE));
            projDevice->setProperty(CAP_PROP_EXPOSURE, 10E9);
            /*
             *   for (int cam=0; cam<nbCameras; ++cam) {
             *    camExposures[cam] = camDevices[cam]->getProperty(CAP_PROP_EXPOSURE);
             *    camDevices[cam]->setProperty(CAP_PROP_EXPOSURE, 5000);
             *   }
             */

            int tries;
            for (tries = 0; tries < 10; ++tries)
            {

                if (!projDevice->put(whiteImage))
                {
                    logError() << "Could not put an image in projector";
                }

                for (int cam = 0; cam < nbCameras; ++cam)
                {
                    if (!camDevices[cam]->get(camImages[cam][0]))
                    {
                        logError() << "Could not get an image from camera" << cam;
                    }
                }

                bool ok = true;
                for (int cam = 0; cam < nbCameras; ++cam)
                {
                    // detecte les tags
                    frame = camImages[cam][0] ^ Scalar::all(255);
                    detector.detectTags(tags, frame);

                    // stocke les points
                    map<int, Point2f> &points = camPoints[cam];
                    points.clear();
                    for (vector<TinyTagInfo>::iterator it = tags.begin();
                         it != tags.end(); ++it)
                    {
                        if (gridPoints.find(it->id) == gridPoints.end())
                        {
                            logWarning() <<
                                "Found a point with an id that does not exist in the tags "
                                "image given.. Ignoring it";
                            continue;
                        }
                        points[it->id] = it->center;
                    }
                    logDebug() << "Camera" << cam << "detected" << points.size() <<
                        "out of" << nbTags;

                    // affiche les tags
                    drawTags(image, frame, tags);
                    ostringstream oss;
                    oss << "tags" << cam;
                    imshow(oss.str(), image);
                    waitKey(1);

                    // !!TODO : change this
                    // si une d'entre elles ne reussit pas .. on le refait pour toutes ...
                    if (points.size() < 20)
                    {
                        ok = false;
                    }
                }

                if (ok) { break; }
            }
            if (tries == 10)
            {
                imshow("tags", frame);
                waitKey(0);
                logError() << "Could not detect tags";
            }
#endif
        }
        else
        {
            for (size_t cam = 0; cam < nbCameras; ++cam)
            {
                string path = format(cameraTagImagesPath.c_str(), step, cam);
                image = imread(path, -1);
                if (image.empty())
                {
                    logError() << "Unable to open camera image file :" << path;
                }
                // detecte les tags
                image.copyTo(frame);
                detector.detectTags(tags, frame);

                // stocke les points
                map<int, Point2f> &points = camPoints[cam];
                points.clear();
                for (vector<TinyTagInfo>::iterator it = tags.begin();
                     it != tags.end(); ++it)
                {
                    if (gridPoints.find(it->id()) == gridPoints.end())
                    {
                        logWarning() <<
                            "Found a point with an id that does not exist in the tags "
                            "image given.. Ignoring it";
                        continue;
                    }
                    points[it->id()] = it->center();
                }
                logDebug() << "Camera" << cam << "detected" << points.size() <<
                    "out of" << nbTags;

                /*
                 *   // affiche les tags
                 *   drawTags(image, frame, tags);
                 *   ostringstream oss;
                 *   oss << "tags" << cam;
                 *   imshow(oss.str(), image);
                 *   waitKey(1);
                 */
            }
        }

        prog++;
        logDebug() << "Matching";

        generator->reset();
        if (online)
        {
#if 0
            // we revert the exposure to its original
            projDevice->setProperty(CAP_PROP_EXPOSURE, projExposure);
            /*
             *   for (int cam=0; cam<nbCameras; ++cam) {
             *    camDevices[cam]->setProperty(CAP_PROP_EXPOSURE, camExposures[cam]);
             *   }
             */

            // d'abord on capture les patterns
            if (synchronized)
            {
                if (!projDevice->put(generator))
                {
                    logError() << "Could not put an image in projector";

                    return -1;
                }
                for (int i = 0; i < nbImages; ++i)
                {
                    for (int cam = 0; cam < nbCameras; ++cam)
                    {
                        if (!camDevices[cam]->get(camImages[cam][i]))
                        {
                            logError() << "Could not get an image from camera" << cam;

                            return -1;
                        }
                    }
                }
                for (int cam = 0; cam < nbCameras; ++cam)
                {
                    /*
                     *   for (int i=0; i<nbImages; ++i) {
                     *   imwrite(format("/tmp/cam_%02d_%03d_%04d.png", cam, step, i).c_str(),
                     * camImages[cam][i]);
                     *   }
                     */
                    ostringstream oss;
                    oss << camWinName << cam;
                    imshow(oss.str(), camImages[cam][nbImages - 1]);
                    waitKey(1);
                }
            }
            else
            {
                for (size_t i = 0; i < nbImages; ++i)
                {
                    Mat img = generator->get();
                    if (!projDevice->put(img))
                    {
                        logError() << "Could not put an image in projector";

                        return -1;
                    }

                    for (int cam = 0; cam < nbCameras; ++cam)
                    {
                        if (!camDevices[cam]->get(camImages[cam][i]))
                        {
                            logError() << "Could not get an image from camera" << cam;

                            return -1;
                        }

                        ostringstream oss;
                        oss << camWinName << cam;
                        imshow(oss.str(), camImages[cam][i]);
                        waitKey(1);
                    }
                }
            }
#endif
        }
        else
        {
            for (size_t i = 0; i < nbImages; ++i)
            {
                for (size_t cam = 0; cam < nbCameras; ++cam)
                {
                    string path = format(cameraSTLImagesPath.c_str(), step, cam, i);
                    camImages[cam][i] = imread(path, -1);
                    if (camImages[cam][i].empty())
                    {
                        logError() << "Unable to open camera image file :" << path;
                    }
                    /*
                     *   ostringstream oss;
                     *   oss << camWinName << cam;
                     *   imshow(oss.str(), camImages[cam][i]);
                     *   waitKey(1);
                     */
                }
            }
        }

        for (size_t cam = 0; cam < nbCameras; ++cam)
        {
            // ensuite on match !
            SequenceBuffer buffer(camImages[cam].begin(), camImages[cam].end());
            MatTable       result = matcher->match(&buffer, 0);
            camMatches[cam] = result.camMatchMat();
#if 0
            if (online)
            {
                imshow("match", camMatches[cam]);
                // imwrite(format("/tmp/match_%02d_%03d.png", cam, step).c_str(), camMatches[cam]);
                waitKey(1);
            }
#endif
        }

        calibrator.addStep(step, camPoints[0], camMatches[0]);

        if (online)
        {
#if 0
            if (step < nbSteps - 1)
            {
                stage->move(zstep);
            }
#endif
        }
        prog++;
    }

    logInfo() << "Calibrating";

    vector<CameraGeometricParameters>                  cameras(nbCameras + 1);
    vector<CameraProjectorLinearStageProjectionOption> options(nbCameras + 1);
    for (size_t i = 0; i < nbCameras + 1; ++i)
    {
        PinholeProjectionOption opt = PinholeProjectionOption().skewFixed();
        options[i]              = CameraProjectorLinearStageProjectionOption(opt);
        options[i].m_zStepFixed = true;
        options[i].m_scaleFixed = true;
    }

    calibrator.calibrate(cameras[0], cameras[1], options[0], options[1], pointsPattern,
                         verbose);
    for (size_t cam = 0; cam < nbCameras + 1; ++cam)
    {
        string path = format(camerasPattern.c_str(), cam);
        cameras[cam].write(path);
    }

    if (online)
    {
#if 0
        projDevice.release();
        for (int cam = 0; cam < nbCameras; ++cam)
        {
            camDevices[cam].release();
        }
        stage.release();
#endif
    }

    return 0;
}
