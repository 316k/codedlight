/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include <mvg/mvg.h>
#include <mvg/recon.h>
#include <xtclap/CmdLine.h>
#include "../core/common.h"

using namespace std;
using namespace cv;
using namespace TCLAP;
using namespace cl3ds;

int main(int   argc,
         char *argv[])
{
    string matchImagePath;
    string maskImagePath;
    string outMeshPath;
    string camCalibConfigPath, projCalibConfigPath;
    string texturePath;
    double zmin                    = 0;
    double zmax                    = 0;
    bool   inverseNormals          = false;
    double maxEdgeLength           = -1;
    double minAngle                = -1;
    bool   completeCorrespondences = false;

    CmdLine cmd("Reconstructs a mesh based on a match map");

    ValueArg<string> maskArg("m", "mask",
                             "Path to the confidence mask image",
                             false, "", "string", cmd);

    ValueArg<string> textureArg("t", "texture",
                                "Path to the texture image",
                                false, "", "string", cmd);

    ValueArg<string> matchArg("r", "match",
                              "Path to the computed match map",
                              true, "", "string", cmd);

    ValueArg<double> zminArg("", "zmin",
                             "Minimum z value",
                             false, 1, "double", cmd);

    ValueArg<double> zmaxArg("", "zmax",
                             "Maximum z value",
                             false, -1, "double", cmd);

    ValueArg<double> maxEdgeLengthArg("", "max-edge-length",
                                      "Maximum edge length in the mesh",
                                      false, -1, "double", cmd);

    ValueArg<double> minAngleArg("", "min-angle",
                                 "Minimum angle in a triangle of the mesh",
                                 false, -1, "double", cmd);

    ValueArg<string> camCalibConfigArg("c", "cam-calib-config",
                                       "Path to camera config file in xml",
                                       true, "", "string", cmd);

    ValueArg<string> projCalibConfigArg("p", "proj-calib-config",
                                        "Path to projector config file in xml",
                                        true, "", "string", cmd);

    ValueArg<string> outMeshArg("o", "out-mesh",
                                "Path to output mesh in .ply format",
                                true, "", "string", cmd);

    SwitchArg inverseNormalsArg("i", "inverse-normals",
                                "Inverse the normals of the mesh (for osgviewer compatibility)",
                                cmd, false);

    SwitchArg completeCorrespondencesArg("", "complete-correspondences",
                                         "Use both X and Y correspondences coordinates from the match map.\n"
                                         "When epipolar geometry is available (through known camera and projector "
                                         "calibration), only X or Y correspondences are needed to reconstruct the mesh.\n"
                                         " With this flag, the mesh is computed using both and enforcing the epipolar "
                                         "implicitly through triangulation.",
                                         cmd,
                                         false);

    // Parse the argv array.
    cmd.parse(argc, argv);

    // Get the value parsed by each arg.
    matchImagePath          = matchArg.getValue();
    maskImagePath           = maskArg.getValue();
    camCalibConfigPath      = camCalibConfigArg.getValue();
    projCalibConfigPath     = projCalibConfigArg.getValue();
    outMeshPath             = outMeshArg.getValue();
    texturePath             = textureArg.getValue();
    zmin                    = zminArg.getValue();
    zmax                    = zmaxArg.getValue();
    inverseNormals          = inverseNormalsArg.getValue();
    minAngle                = minAngleArg.getValue();
    maxEdgeLength           = maxEdgeLengthArg.getValue();
    completeCorrespondences = completeCorrespondencesArg.getValue();

    CameraGeometricParameters camParameters;
    camParameters.read(camCalibConfigPath);

    CameraGeometricParameters projParameters;
    projParameters.read(projCalibConfigPath);

    Mat match = imread(matchImagePath, -1);
    if (!match.data)
    {
        cerr << "unable to read " << matchImagePath << endl;

        return -1;
    }
    if (match.depth() != CV_16U)
    {
        cerr << "input image is not 16 bit " << matchImagePath << endl;

        return -1;
    }

    // load le masque
    Mat mask;
    if (!maskImagePath.empty())
    {
        mask = imread(maskImagePath, -1);
        if (!mask.data)
        {
            cout << "could not load mask : " <<  maskImagePath << endl;

            return -1;
        }

        if ((mask.cols != match.cols) ||
            (mask.rows != match.rows))
        {
            cout << "mask and camera sizes do not match" << endl;

            return -1;
        }
    }

    Mat texture;
    if (!texturePath.empty())
    {
        texture = imread(texturePath, -1);
        if (texture.empty())
        {
            logError() << "Image" << texturePath << "could not be loaded";
        }
    }

    Mat camPoints;
    Mat projPoints;
    pointsFromMatchMap(match, projParameters.imageSize(), camPoints, projPoints);

    Ptr<VertexFilter> filter;
    if (minAngle > 0)
    {
        if (maxEdgeLength > 0)
        {
            filter = makePtr<MaxEdgeLengthMinAngleFilter>(maxEdgeLength, minAngle);
        }
        else
        {
            filter = makePtr<MinAngleFilter>(minAngle);
        }
    }
    else if (maxEdgeLength > 0)
    {
        filter = makePtr<MaxEdgeLengthFilter>(maxEdgeLength);
    }
    else
    {
        filter = makePtr<NoFilter>();
    }

    ElapsedTimer timer;
    Ptr<Mesh>    mesh = reconstructMesh(camPoints, projPoints,
                                        camParameters, projParameters,
                                        texture, mask,
                                        *filter, completeCorrespondences);
    logInfo() << "Reconstruction took" << timer.elapsed() / 1000.0 << "seconds";

    mesh->computeNormals();
    if (inverseNormals)
    {
        mesh->reverseNormals();
    }

    if (zmin < zmax)
    {
        mesh->clip(-numeric_limits<double>::max(), -numeric_limits<double>::max(), zmin,
                   numeric_limits<double>::max(), numeric_limits<double>::max(), zmax);
    }

    mesh->write(outMeshPath);

    return 0;
}

