.. _chapter-concepts:

============
Key concepts
============

The 3D shape of an object can be recovered by first sampling points
of its surface and then constructing a mesh joining them together. This is sometimes referred
to as building a **point cloud** and a **3D model** out of an object.

While several methods can be used to produce a point cloud, most of them
are based on **3D triangulation**. Based on (2D) positions of the same point in 
several views, one computes the most likely position of the corresponding (3D) 
point in the world. Thus, one needs to identify where a point is projected
in multiple (at least two) views and a way to combine these views together, namely
the **geometric calibration** of the setup.

The projections of the same 3D point in different views are called 
**corresponding points**. Finding a set of correspondences is a challenging 
problem without additional information. 3D active methods are one kind of 
computer vision methods that use devices and sensorsthat interact (at distance)
with the scene to simplify the task of finding correspondences. One special
case of active methods is called **coded light reconstruction**.

Coded light reconstructions use the deformation of a known projected image on
an unknown surface to infer corresponding points. A projector is used to emit
*coded* light (a **pattern**) on the scene which is observed and captured by
one or more camera. Since the projected pattern is known, the identification of
corresponding points is now guided by the content of the pattern, a much easier
task.

.. table:: 
    :class: borderless 

    +---------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------+
    | .. thumbnail:: /images/leopard_0000.png                                               | .. thumbnail:: /images/cam_00_0000.png                                                | .. thumbnail:: /images/escalier.png                                                   |
    |    :width: 95%                                                                        |    :width: 95%                                                                        |    :width: 95%                                                                        |
    |    :align: center                                                                     |    :align: center                                                                     |    :align: center                                                                     |
    |    :group: concepts                                                                   |    :group: concepts                                                                   |    :group: concepts                                                                   |
    |    :title: An example of coded light pattern                                          |    :title: An example of captured image of the scene with a projected pattern onto it |    :title: An example of reconstructed 3D model of an object                          |
    +---------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------+

The **codification** (or design of the projected pattern) is what differentiates
every coded light reconstruction method. Some methods require few patterns
but are sensitive to noise or textures, while others may use more patterns
to yield better accuraries. Those trade-offs must be considered when deciding which
coded light method should be used for one application.

Coded light reconstruction methods
==================================

The most simple method that can be used to associate a projector pixel
to a camera pixel, is to turn them on one at a time. For each projector pixel
that is lit, one could identify its correspondence in each view, by
finding the camera pixel with the highest intesity. While this
method would work, it uses way too many patterns (one for each projector pixel).
Instead, several projector pixels can be lit together, creating a 
**coded light pattern**, which can be used to identify correspondences effectively.

**Structured light** methods effectively encode each projector pixel positions
through a sequence of patterns. Correspondences are easily found by decoding
the position of the projector pixel responsible for the sequence of captured
intensities at each camera pixel. In contrast, **unstructured light** methods
only guarantee that each projector pixel is represented by a unique sequence
of intensities, and relies on more involved algorithms to compute correspondences.

Coded light generators
======================

The first step towards 3D reconstruction using coded light is to generate patterns,
project them on a scene and capture them using a camera. The process of 
generating patterns is abstracted in what the library calls **generators**.
Once configured through various parameters (size, count or more specialized 
ones like frequency or randomness...), they can create patterns on demand for,
projection or matching purposes.

In ``CodedLight``, the abstract base class ``Generator`` represents any kind of
objects that can generate patterns. It contains several virtual functions to
query the state of a generator, obtain the current pattern and switch to the next
one.

Every kind of codification implemented in the library is represented by a class
deriving from ``Generator``. For example, a ``GrayCodeGenerator`` implements the
Gray code structured light pattern generation. This hierarchy of class also
contains several class that are not directly related to coded light reconstructions.
For example, a ``SequenceReader`` is a kind of generator that read patterns from
images saved on the disk.

.. TIP::
    Generators are not required to store the patterns they generate, and are encouraged
    to constructs them on the fly to reduce memory footprints. Some generators can
    however store images in memory for caching performance or to avoid concurrency
    problems, like ``SequenceBuffer``.

Coded light matchers
====================

The process of computing correspondences is called **matching** a sequence
of captured images with the set of projected patterns that were used during
the acquisition. 

In ``CodedLight``, the abstract base class ``Matcher`` represents any kind of
objects that can match two sets of pattern. It only has one important function
``Matcher::match`` which can be called with two instances of ``Generator``
and some other parameters to compute the correspondence map between them. 

.. NOTE::
    The generators passed to the matcher need not necessarily be coded light
    generators. In fact, it is much more common to pass a ``SequenceReader`` as
    the camera generator, since images will often be saved on the disk prior
    to matching.

Matchers are very specific and can only be used with the correct set of projected
patterns. For example, one can not expect good correspondences if using a 
``PhaseShiftMatcher`` to match two sets of Gray code projected and captured images.

Correspondences can be computed between two point of view.

.. NOTE::
    The correspondence map represents correspondences from a point of view to another.
    Thus it transfers coordinates from one image to coordinates in another.
    Most of the time, correspondences map camera pixels to projector
    pixels, but it the method supports it, it can also represent 
    correspondences from the projector to the camera.

Once computed, the correspondences are often stored in a bitmap
image called a **correspondence map**. For the rest of the discussion, we refer
to a correspondence map as the set of correspondences relating one view to
another (arbitray) one. Better reconstructions can be achieved 
by merging correspondence maps computed between several point of views.

Correspondence map
==================

A compact representation of correspondences is used in ``CodedLight``.

A **correspondence map** :math:`I` is a `16 bit` color coded image that
represents correspondences via the *red* and *green* channel :

.. math::
    I(x',y') &= \left( x \left \lfloor \frac{65535}{w-1}+0.5 \right \rfloor, \left \lfloor y \frac{65535}{h-1}+0.5 \right \rfloor, 0 \right)

where :math:`(x',y')` is the position of an (integer) pixel in one view,
and :math:`(x,y)` is its corresponding point in an other view which has
dimension :math:`w \times h`. 

.. NOTE::
    Sometimes the *blue* channel is also used to carry out other information,
    like a measure of the likelihood of the correspondence.


To convert a correspondence map back to correspondence points, one uses :

.. math::
    x &= I_r(x',y') \frac{w-1}{65535}\\
    y &= I_g(x',y') \frac{h-1}{65535}

where :math:`I_r(x',y')` and :math:`I_g(x',y')` corresponds to taking the red and green 
channel value of the image at the position :math:`(x',y')`.

In `CodedLight`, correspondences map can be computed using the 
``match()`` function of a particular coded light method. Matchers are implemented
in classes deriving from the ``Matcher`` abstract base class.
    
This function returns a ``MatTable`` object which contains the correspondence map
for the chosen direction (camera to projector, or projector to camera). It
can also contain other informations like the actual codes decoded by the method,
confidence masks, etc.


For example, let's say one want to compute the camera-projector correspondence 
map from a camera to a projector using **Gray codes** patterns captured and saved
on the disk and encapsulated in a ``SequenceReader``, one could use the following code :

.. code-block:: c++

    #include <codedlight/core/common.h>
    #include <codedlight/patterns/seq_reader.h>
    #include <codedlight/patterns/graycode.h>

    SequenceReader reader(/*the actual parameters of the reader*/);
    GrayCodeMatcher matcher(/*the actual parameters of the gray code matching method*/);

    // computes the match between the camera captured images and the projector
    // implicitly described patterns : no generator needed for the projector
    MatTable result = matcher.match(&reader, 0);

    // access the camera-projector correspondence map
    Mat match = result.camMatchMat();

    // extract the correspondence for a particular point (xc, yc) in the camera image
    int xc = 10, yc = 20;
    Vec3w cor = match.at<Vec3w>(xc, yc); // match is a 3 channels 16 bit color image
                                         // represented by a cv::Vec3w object 

    // convert it to floating point coordinates using the projector dimension w x h
    double xp, yp;
    pointFromMatch(cor, Size(w, h), xp, yp);

.. HINT::
    Here we used ``pointFromMatch`` to convert a color coded match correspondance
    to a floating point coordinates. You can use ``pointToMatch`` to do the 
    opposite conversion (i.e from coordinates to color coded match). Finally,
    if you want to convert a whole match map to two arrays of correspondences,
    you can use ``pointsFromMatchMap``.

.. WARNING::
    One of the common mistake when accesing the value of a *RGB* pixel,
    is to forget that ``cv::Mat`` images
    are stored in *BGR* format, meaning that :math:`I_r` and :math:`I_g` 
    actually refer to channel **2** and **1** respectively (not *0* and *1*).
