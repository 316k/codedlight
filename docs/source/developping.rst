.. _chapter-developping:

===========
Developping
===========

Here, we sum up some of the informations that would be useful to continue
the development of `CodedLight`.

Adding an object
================

When adding a class that is derived from ``Object``, it is mandatory to provide
a private implementation (or **pimpl**) pointer when calling the parent
constructor. Using an opaque pointer has several advantages :

+   it simplifies the public API of an object, and hides private details of implementation
+   it encourage backward binary compatibility
+   it reduces the compilation time, by not requiring to compile the whole library for each change in a class member
+   it enables ``CodedLight`` to make use of the *template pattern* by calling public functions in ``Object`` (or a child class) that eventually forwards its call to the pimpl

The base class for private implementation is ``AbstractPrivate`` and
it should be a nested ``Private`` class. The easy way to declare this class
is simplify to make use of the ``CL3DS_PRIV_IMPL`` macro in the declaration
of your class.

.. code-block:: cpp

    #include <core/object.h>

    class MyObject : public Object
    {
        public:
            MyObject();

            void foo(); //a public function

            CL3DS_PRIV_IMPL //declares a friend nested Private class
                            //and ways to access it safely
    }

To define the class in your source code, there are again several useful macros.
``CL3DS_PUB_IMPL`` provides access to the public instance of the object from
inside the pimpl. ``CL3DS_PRIV_PTR`` and ``CL3DS_PRIV_CONST_PTR`` declares
a pointer (resp. a constant pointer) to the pimpl named ``priv`` inside the scope.
Note that those macros, and the ``AbstractPrivate`` class declarations are
available in the ``object_p.h`` private header, which is not distributed outside
the source code of the library.

The pure virtual function ``AbstractPrivate::read`` is implemented by using the
``CL3DS_CLASS_NAME`` macro. It defines a static class name for the underlying
object, and is mandatory for the associated class factory to work properly.
This name will be returned by a call to :func:`Object::className()`.
However, you must override the pure virtual functions ``read``, ``write``,
``configure`` and ``hash``.

.. code-block:: cpp

    #include <core/object_p.h>

    struct MyObject::Private : public AbstractPrivate
    {
        //you must call the parent constructor with an instance of Object*
        Private(MyObject* ptr) : AbstractPrivate(ptr) {} 

        void foo()
        {
            //private implementation of foo
        }

        //those pure virtual functions must be defined !
        bool read(const cv::FileNode &fn);
        bool write(cv::FileStorage &fs) const;
        bool configure(std::istream *is, std::ostream *os);
        size_t hash() const;

        //you can store any member you'd like here !

        CL3DS_PUB_IMPL(MyObject) //defines a way to access the public MyObject
                                 //pointer via pubPtr()
        CL3DS_CLASS_NAME("myobject") //defines a static class name for all
                                     //instances of this class
    }

    //you must call the parent constructor with a Ptr<AbstractPrivate>
    MyObject::MyObject() : Object(makePtr<Private>(this)) {}

    void MyObject::foo()
    {
        //do something ...

        //then calls pimpl implementation
        CL3DS_PRIV_PTR(MyObject);
        priv->foo(); //with the priv pointer
    }

To implement the serialization or configuring functions, one *template* class
is very handy : ``TypeParameter``. It acts as a transparent wrapper to the template
type, while optionally enforcing constraints on its possible values. It also knows
how to (de)serialize itself in/to a XML file, or how to confirure itself by
querying the user.

For example, let's say your object needs to store an **integer** variable ``length``
whose values are constrained to the range :math:`5 < \mathsf{length} < 25`.
You wold use the following code :

.. code-block:: cpp

    #include <core/parameter.h> //for TypeParameter
    #include <core/hasher.h> //for Hasher

    struct MyObject::Private : public AbstractPrivate
    {
        /* ... */
        TypeParameter<int> paramLength;
        /* ... */
    };

    MyObject::Private(Object *ptr) : AbstractPrivate(ptr)
    {
        paramLength = TypeParameter<int>("length", /* a name */
                                         "Length of my object", /* a description */
                                         18, /* a default value */
                                         makePtr< BoundConstraint<int> >(5, 25));
    }

    bool MyObject::read(const FileNode &node)
    {
        paramLength.read(node); //reads the variable from the XML file
        return true;
    }

    bool MyObject::write(FileStorage &fs) const
    {
        paramLength.write(fs); //writes the variable to the XML file
        return true;
    }

    bool MyObject::configure(istream *is,
                             ostream *os)
    {
        paramLength.configure(is, os); //queries the user for the value
        return true;
    }

    size_t MyObject::hash() const
    {
        return Hasher() << paramLength; //hash the variable
    }

Now, if a user tries to configure your object with a length that is outside
the allowed interval, the function will inform him and retry until a valid value
is given. If your object is unserialized with an invalid value, it will be given
the default value **18**.

Adding a generator
==================

Since a ``Generator`` is a kind of ``Object``, each information given in the
previous section also applies here. A ``Generator``'s pimpl must also implement
some functions related to the generation of each pattern in the sequence. 

Here we implement a generator that generates only two patterns : a white and a
black one. 

.. code-block:: cpp

    struct MyGenerator::Private : public AbstractPrivate
    {
        bool white;
        TypeParameter<Size> paramSize; //TypeParameter knows about the type cv::Size

        Private(MyGenerator *ptr, Size size) : AbstractPrivate(ptr), white(true)
        {
            /* .. initialize the TypeParameter accordingly ..  */
            paramSize = size; //then sets its value
        }

        //prepares the generator for the next pattern
        void next()
        {
            white = !white;
        }

        //creates and returns the current pattern
        Mat get()
        {
            return Mat(paramSize, CV_8U, Scalar::all(white*255)); //a white or black pattern
        }

        //resets the generator to its original state
        void reset()
        {
            white = true; //white for index 0, then black
        }

        //returns the number of patterns in the generator sequence
        size_t count() const
        {
            return 2;
        }

        //returns the size of a generated pattern
        Size size() const
        {
            return paramSize;
        }

        //returns the type of a generated pattern
        int type() const
        {
            return CV_8U; //most patterns are 8 bits grayscale (or color) images
        }
    };

For the ``Generator``'s factory to be able to create your generator for the associated
key value, you must use the ``CL3DS_CLASS_NAME`` in your pimpl, and manually
register the mapping between your name and your class. This is done inside
the constructor of ``Generator::Factory`` :

.. code-block:: cpp

    Generator::Factory::Factory()
    {
        /* ... */
        reg(MyGenerator()); //the class name is MyGenerator().className()
    }

You must also include your header file in ``generators.h`` for the ``Generator``
class to be able to see your class. This header is often used to include
all the generators that are inside the library.

Adding a matcher
================

Since a ``Matcher`` is a kind of ``Object``, each information given in the
previous section also applies here. A ``Matcher``'s pimpl must also implement
one important function that is responsible for matching two sequences
of generators. 

There are two important things to know about the :func:`Matcher::match` function :

    1. It should not presume of the (*OpenCV*) type of the images stored in the
    generators passed to it.

    2. It should take into account the fact that the epipolar geometry of the
    camera-projector system is known or not.

To account for the second remark, ``Matcher::match`` actually calls one of two
functions : ``Matcher::AbstractPrivate::matchWithEpipolarGeometry`` or
``Matcher::AbstractPrivate::matchWithoutEpipolarGeometry`` depending on the 
availability of the epipolar geometry. You must implement both functions in your
pimpl, even if you don't make use of this information and forwards one function
call to the other :

.. code-block:: cpp

    #include <core/matcher_p.h>

    MyMatcher::Private::matchWithEpipolarGeometry(Generator *const camGenerator,
                                                  Generator *const projGenerator,
                                                  const cv::Mat   &camMask,
                                                  const cv::Mat   &projMask,
                                                  const Camera    &camParameters,
                                                  const Camera    &projParameters)
    {
        logInfo() << "This matcher does not use the epipolar geometry, proceeding "
                  << "as if it was unknown."
        matchWithoutEpipolarGeometry(camGenerator, projGenerator, camMask, projMask);
    }

    MyMatcher::Private::matchWithoutEpipolarGeometry(Generator *const camGenerator,
                                                     Generator *const projGenerator,
                                                     const cv::Mat   &camMask,
                                                     const cv::Mat   &projMask)
    {
        // match that does not use the epipolar geometry
    }

When implementing the matching function, the *protected* function
 
``Matcher::AbstractPrivate::fillInMatchesUsingEpipolarGeometry(``
``cv::Mat &codesX, cv::Mat &codesY,``
``const Camera &camParameters, const Camera &projParameters)``
will come in handy to complete the match map (when the horizontal or vertical codes
have already been computed).

To not depend on the type of images that the generator produces, both functions
should then dispatch their call to *template* member function, where the underlying
type is now apparent as the ``template`` type instantiated automatically
via the **dispatcher** :

.. code-block:: cpp

    #include <core/matcher_p.h>

    MyOtherMatcher::Private::matchWithEpipolarGeometry(Generator *const camGenerator,
                                                       Generator *const projGenerator,
                                                       const cv::Mat   &camMask,
                                                       const cv::Mat   &projMask,
                                                       const Camera    &camParameters,
                                                       const Camera    &projParameters)
    {
        //the dispatcher expects a wrapper class MatcherData that contains
        //everything a matcher needs
        MatcherData data(camGenerator, projGenerator, camMask, projMask, 
                         camParameters, projParameters);

        //dispatch to my template member function matchWithEpipolarGeometry<T>
        //where T is converted automatically from the generator type :
        //camGenerator->type()
        return dispatchWithGeometry(this, data, camGenerator->type());
    }

    template <class T>
    MatTable MyOtherMatcher::matchWithEpipolarGeometry(const MatcherData &data)
    {
        //now you can implement the matching with the template type T of camGenerator
        //T is often uchar, ushort, Vec3b, Vec3w, etc...
    }

Conversely, to dispatch to the matching function that does not use the epipolar
geometry, one may call : ``dispatchWithoutGeometry``.

Finally, if your matcher use both the camera and the projector generators, you
can dispatch on both types :

.. code-block:: cpp

    #include <core/matcher_p.h>

    MyOtherMatcher::Private::matchWithEpipolarGeometry(Generator *const camGenerator,
                                                       Generator *const projGenerator,
                                                       const cv::Mat   &camMask,
                                                       const cv::Mat   &projMask,
                                                       const Camera    &camParameters,
                                                       const Camera    &projParameters)
    {
        //the dispatcher expects a wrapper class MatcherData that contains 
        //everything a matcher needs
        MatcherData data(camGenerator, projGenerator, camMask, projMask,
                         camParameters, projParameters);

        //dispatch to my template member function matchWithEpipolarGeometry<T1, T2> where
        //T1 is converted automatically from the generator type : camGenerator->type()
        //T2 is converted automatically from the generator type : projGenerator->type()
        return dispatchWithGeometry(this, data, camGenerator->type(), 
                                    projGenerator->type());
    }

    template <class T1, class T2>
    MatTable MyOtherMatcher::matchWithEpipolarGeometry(const MatcherData &data)
    {
        //now you can implement the matching with the template types T1 of camGenerator
        //and T2 of projGenerator
    }
