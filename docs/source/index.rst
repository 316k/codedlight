.. CodedLight documentation master file, created by
   sphinx-quickstart on Fri Oct  3 14:21:25 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

======================================================
CodedLight : 3D scanning from images using coded light
======================================================

.. toctree::
   :maxdepth: 2

   installation
   concepts
   tutorial
   reference
   codedlight
   developping
   license

CodedLight is a library that implements several coded (structured and
unstructured) light methods that can be used to scan objects and produces 3D
models of them.

It can :

* Generate sequence of patterns that can later be projected (with or without
  the use of this library)
* Compute the correspondence map between what a projector displays and what a
  camera sees out of it
* Calibrate a camera-projector system
* Reconstruct a 3D mesh based on a correspondence map and calibration data

..
    Getting started
    ---------------

    + Clone the
      Git repository for the latest development version.

      .. code-block:: bash

           git clone https://bitbucket.org/nicolasmartin3d/codedlight

    + Read the :ref:`chapter-tutorial`, browse the chapters on the
      :ref:`chapter-modeling` API and the :ref:`chapter-solving` API.
    + File bugs, feature requests in the `issue tracker
      <https://code.google.com/p/ceres-solver/issues/list>`_.
