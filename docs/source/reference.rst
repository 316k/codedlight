.. default-domain:: cpp

.. _chapter-reference:

=============
API Reference
=============

`CodedLight`'s source code is separated in separate folder containing headers :

+   :ref:`core <section-ref-core>` contains the data structures and algorithms used inside other modules
+   :ref:`patterns <section-ref-patterns>` contains the pattern generators and matchers for many coded
    light methods as well as other classes that can transform patterns
+   :ref:`calib <section-ref-calib>` contains the classes that implement camera-projector system geometric
    or photometric calibration
+   :ref:`devices <section-ref-devices>` contains the classes needed to control cameras, projectors and
    other devices used by the library

.. rubric:: ``cl3ds`` namespace

All the classes and functins are placed into the ``cl3ds`` (**Coded Light 3D Scanning**)
namespace to avoid name clash.

The usual way to use the library is to either prefix with the  ``cl3ds::`` namespace
specifier (in your headers) or to use the ``using namespace cl3ds;`` directive (in your source code).

If an ambiguity arises between ``CodedLight`` names and another namespace, you
might have to explictly use the ``cl3ds::`` specifier, even if you have use the
``using`` directive :

.. code-block:: cpp

    #include <codedlight/core/mattable.h>
    /* ... */
    cl3ds::MatTable table;

or :

.. code-block:: cpp

    #include <codedlight/core/mattable.h>
    using namespace cl3ds;
    /* ... */
    MatTable table;
    


.. _section-ref-core:


--------------------
Core functionalities
--------------------

:class:`Object`
+++++++++++++++

:class:`Object` is the base class of most objects in ``CodedLight``. Objects
can be serialized to a XML file, unserialized to memory, or configured by
querying the user for parameters value. Objects can be created dynamically
thanks to their class factory which is responsible for registering a key
value for each object it can create, and create default objects based on a key
value.

This class has the following API :

.. class:: Object

    .. code-block:: cpp

        class Object
        {
            public:
                virtual bool read(const cv::FileNode &fn);
                virtual bool write(cv::FileStorage &fs) const;
                virtual bool configure(std::istream *is,
                                       std::ostream *os);
                virtual size_t hash() const;
                virtual const std::string & className() const;

            protected:
                Object(cv::Ptr<AbstractPrivate> ptr);
        };

The following functions are responsible for reading an object from 
a ``cv::FileStorage::Node`` or writing to a ``cv::FileStorage`` :

.. function:: void Object::read(const cv::FileNode &fn)
.. function:: void Object::write(cv::FileStorage &fs)

This function dynamically configure an object by asking the user questions about
its parameters value :

.. function:: void Object::configure(std::istream*, std::ostream*)

There is no public constructor for an ``Object``.
This is because the only constructor of an ``Object`` requires a pointer to a private
implementation in order to know how to correctly read, write and configure its
parameters. This constructor is protected and should be overriden in child classes.

Therefore, you will never directly allocate instances of ``Object`` but rather 
instances of classes derived from ``Object`` which provide such private
implementations. ``Generator`` and ``Matcher`` are 
examples of such objects (see :ref:`subsection-ref-generating` and
:ref:`subsection-ref-matching`).

Two other functions are useful :

.. function:: size_t Object::hash() const

    Returns a unique integer representation of the generator taking into accounts its
    current parameters.

.. function:: const std::string& Object::className() const

    Returns the name of the generator that is the same for every instance of this class.

These functions are useful for persistence, as the state of an object can
be permanently saved in a convenient and reusable XML files. They also provide
a way to identify the object so that the correct derived class can be 
instantiated when read from a XML file. When written, the XML file will also
contain comments about the different parameters of the generator which can
complement the documentation.

For example, the following code :

.. code-block:: c++

    #include <codedlight/patterns/seq_reader.h>

    Ptr<SequenceReader> reader = makePtr<SequenceReader>("image_%03d.png", 20);
    FileStorage fs("reader.xml", FileStorage::WRITE);
    fs << "generator" << reader;

would serialize the generator to a XML file whose content is :

.. code-block:: xml

    <?xml version="1.0"?>
    <opencv_storage>
    <generator>
      <name>seqreader</name>
      <param>
        <!--
        Type : string
        Description : String format used to determine the path of images to read
        Default value : ./img_%04d.png
        -->
        <pathFormat>image_%03d.png</pathFormat>
        <!--
        Type : int
        Description : Number of patterns to read
        Default value : 0
        -->
        <length>20</length>
        <!--
        Type : int
        Description : Offset from which to start in the format
        Default value : 0
        -->
        <offset>0</offset>
        <!--
        Type : int
        Description : Steps between name of two images in the format
        Default value : 1
        -->
        <step>1</step></param></generator>
    </opencv_storage>

which can later be read back to unserialize the generator using :

.. code-block:: c++

    #include <codedlight/patterns/seq_reader.h>

    Ptr<SequenceReader> reader;
    FileStorage fs("reader.xml", FileStorage::READ);
    fs["generator"] >> reader;

.. NOTE::
    The code uses OpenCV's ``cv::Ptr`` smart pointer type to store the generator,
    as the line ``fs["generator"] >> reader`` actually allocates the object and
    stores the pointer in this object. This is done by using the 
    ``Generator::classFactory()`` object that can create generator instances when
    it encounters a XML file that needs to unserialize a generator.


:class:`MatTable`
+++++++++++++++++

A :class:`MatTable` is a dictionary of images (represented by a ``cv::Mat``) stored by name.
It is used by the ``Matcher::match`` function to return several results, like the camera-projector
correspondence map. The class has the following (reduced) API :


.. class:: MatTable

   .. code-block:: c++

    class MatTable
    {
        public:
            Mat& camMatchMat();
            const Mat& camMatchMat() const;

            ...

            Mat& operator[](const std::string& name);
            const Mat& operator[](const std::string& name) const;

            vector<string> names() const;
    };

Some standard images can be accessed using public member functions :

.. code-block:: c++

    #include <codedlight/core/mattable.h>

    MatTable table = /* some matcher's match return */;
    Mat camMatch = table.camMatchMat();   //cam-proj correspondence map
    Mat projMatch = table.projMatchMat(); //proj-cam correspondence map

Other images can also be accessed by name :

.. code-block:: c++

    #include <codedlight/core/mattable.h>

    MatTable table = /* some matcher's match return */;
    Mat camConfidence = table["cam confidence"];

A :class:`Matcher` guarantees that it will store either the camera-projector
correspondence map, the projector-camera correspondence map or both in the
returned :class:`MatTable`, depending on the matcher's parameters.

The complete list of images stored in the :class:`MatTable` can be accessed by :

.. function:: vector<std::string> MatTable::names() const

    Returns the list of names that can be used to access the images stored in the table,
    by using the index operator[].

.. WARNING::

    Calling the index operator on a :class:`MatTable` will return a reference
    to the matrix stored in the table for the given key. If it did not exist
    previously, it will be created.

    However, if you call the index operator on a ``const TupleMat``
    object with a matrix that does not exist yet, an exception will be thrown.



.. _section-ref-patterns:

-------------------------------
Pattern generation and matching
-------------------------------

.. _subsection-ref-generating:

Generating patterns
+++++++++++++++++++

The library provides several pattern generators that can be used to construct
a sequence of patterns in memory. Those patterns can either be projected or
saved on the disk for later use.

The abstract base class to generate patterns is :class:`Generator` which
provides the following API :

.. class:: Generator

   .. code-block:: c++

    class Generator {
      public:
         virtual size_t count() const;
         virtual cv::Mat get();
         virtual size_t index() const;
         virtual bool hasNext() const;
         virtual void next();
         virtual void reset();
         virtual cv::Size size() const;
         virtual int type() const;
    };


A pattern generator defines how many patterns it can generate, and gives access
to them sequentially. Generators can be seen as forward iterators through a container
of  ``cv::Mat``. The current pattern that will be generated by the next call of 
``Generator::get()`` is the pattern at position ``Generator::index()`` in the sequence.

    .. NOTE::
       Most functions are marked virtual so that a child class can fully customized
       the default behavior. It is, however, neither required nor recommended to do so, unless
       the child has a very good reason to do it.

.. function:: size_t Generator::count() const

    Returns the number of patterns in the sequence that this generator can generate.
    The default implementation simply forwards the call to the derived class private 
    implementation.
    This function does not have to generate any patterns, and should not take much
    a lot of time to compute as it will be called fairly often to decide if a generator
    can create more patterns.

.. function:: cv::Mat Generator::get()

    Returns the generated pattern at current position in the sequence.
    The default implementation caches the last pattern generated and forwards the call
    to the derived class private implementation if the current index has changed.
    The generator does not have to keep the pattern in memory, it is the responsibility
    of the caller to manage the lifetime of the generated image.

.. function:: size_t Generator::index() const

    Returns the current position in the sequence.
    The default implementation stores and manages the current index in the sequence.

.. function:: bool Generator::hasNext() const

    Returns true when the generator can generate more patterns. 
    The default implementation simply verifies that the current index has not yet
    reached the length of the sequence of the generator.

.. function:: void Generator::next()

    Advances the current index and prepares the generator for the next pattern generation.
    The default implementation also forwards the call to the derived class private 
    implementation.

.. function:: void Generator::reset()

    Resets the generator to its default state and the current index to 0.
    The default implementation also forwards the call to the derived class private 
    implementation.

.. function:: cv::Size Generator::patternSize() const

    Returns the size of a generated pattern.
    The default implementation simply forwards the call to the derived class private 
    implementation.

.. function:: int Generator::type() const

    Returns the (OpenCV) type of the ``cv::Mat`` representing a generated pattern, most of
    the time the value ``CV_8U``.
    The default implementation simply forwards the call to the derived class private 
    implementation.

Additionnaly, like many other objects in ``CodedLight``, a generator can be 
written/read from/to an XML configuration file. It can also be configured live
by asking the users the value of its parameters (see :func:`Object::configure`).

.. _subsection-ref-matching:

Matching patterns
+++++++++++++++++

For each coded light generator, there is an associated matcher that can compute
a correspondence map from a set of captured and projected sequence of patterns.

The anstract base class to compute correspondence maps (referred to as matching),
is :class:`Matcher` which provides the following API :

.. class:: Matcher

   .. code-block:: c++

    class Matcher {
      public:
        virtual MatTable match(Generator *const camGenerator,
                               Generator *const projGenerator,
                               const cv::Mat   &camMask=cv::Mat(),
                               const cv::Mat   &projMask=cv::Mat(),
                               const Camera    &camParameters=Camera(),
                               const Camera    &projParameters=Camera());
    };

Every coded light matcher requires several information to compute a correspondence
map :

.. function:: MatTable Matcher::match(Generator *const camGenerator, Generator *const projGenerator, const cv::Mat &camMask=cv::Mat(), const cv::Mat &projMask=cv::Mat(), const Camera &camParameters=Camera(), const Camera &projParameters=Camera())

   :param camGenerator: the generator that produces the captured images

   :param projGenerator: the generator that produces the projected patterns, which can be an instance of :class:`DummyGenerator` when it is not required by the matcher (refer to the specific doc of the matcher).

   :param camMask: an image of type ``CV_8U`` and same dimension as that of a captured image, with values ``1`` or ``0`` for pixels for which one wish to have or not have its correspondence computed in the camera-projector match map. *(Optional)*

   :param projMask: an image of type ``CV_8U`` and same dimension as that of a projected pattern, with values ``1`` or ``0`` for pixels for which one wish to have or not have its correspondence computed in the projector-camera match map. *(Optional)*

   :param camParameters: the camera geometric calibration which can be used to speed up or enforce geometric constraints during the matching. If provided, ``projParameters`` must also be provided. *(Optional)*

   :param projParameters: the projector geometric calibration which can be used to speed up or enforce geometric constraints during the matching. If provided, ``camParameters`` must also be provided. *(Optional)*


Most matchers can compute camera-projector correspondence map, but some may also compute
the projector-camera correspondences. In the first case, ``camMask`` is used to determine
for which pixels the correspondences are computed, while ``projMask`` is used in the other case.

Matching a subset of all the pixels of an image will speed up the correspondence
computation. It may also help the 3D meshing algorithm if the pixels left out of
the mask correspond to noisy or erroneous 3D points which would eventually be
filtered out.

The mask can correspond to a region of interest to the user. It can also be computed
automatically from some image statistics, like the standard deviation of a pixel
intensities in the sequence of captured images. If no mask is provided, the
matching algorithm will compute a correspondence for every pixel, but might
compute a *confidence* mask that indicates the likelihood of a match for each pixel
(refer to the specific matcher documentation for this information).

The camera and projector geometric calibration may also help the matching algorithm
if provided. Since the **epipolar geometry** of the camera-projector system
constrains the location of the correspondence for a point, the matcher will
produce a correspondence map with fewer information (typically less projected
patterns) which will speed up the capture process. The correspondence computed
should *generally* be comparable to the ones computed wihtout the geometric
information, except for some *difficult* pixels.

.. CAUTION::

    When providing geometric calibration, both camera and projector parameters
    are required. If one of them is empty, then the matcher will procede as if
    none of them was passed. 

.. NOTE::

    The geometric calibration of the system is not always available, despite it
    being mandatory for 3D reconstruction. This is the often the case when
    coded light is used only to compute correspondences for the purpose of
    registration (e.g multi-projection). In this case, the calibration is not
    necessarily available and the match can still be computed. However, if the
    calibration has already been computed, you should pass this information to the
    matcher.

The behavior and parameters of a :class:`Matcher` are documented in detail in
the specific coded light matcher class (see :ref:`chapter-codedlight`).



.. _section-ref-calib:

-----------
Calibration
-----------

Geometric calibration of the setup is a necessary part of building a *3D model*.

A lot of informations regarding camera calibration can be found `here <http://docs.opencv.org/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html>`_ .
In this chapter, we focus on the combined calibration of a single
camera-projector system.
Separate calibration of the camera and the projector can be achieved several ways,
but was proven to be less accurate, due to the specific nature of how a projector
is (separately) calibrated.
The calibration of a multi-camera projector system is not yet documented.


Camera-projector system calibration with correspondence maps
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. TODO::

    Document this section.

Camera-projector system calibration with a translation stage
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This is the preferred method if you have access to a translation stage. It only
needs the positions of some points on a grid moved at regular interval of a
translation axis.
With the help of the library ``TinyTag`` and a printer, it can be done without
human intervention and is very accurate (see :ref:``section-tut-calib``).

The class used to do the calibration is :

.. class:: CameraProjectorLinearStageCalibrator 

    .. code-block:: cpp

        class CameraProjectorLinearStageCalibrator 
        {
            public:
                CameraProjectorLinearStageCalibrator(cv::Size camSize=cv::Size(),
                                                     cv::Size projSize=cv::Size());

                void setGridPoints(const std::map<int, cv::Point2f> &gridPoints);

                void setScale(double scale);
                void setZRange(double zmin,
                               double zmax,
                               double zstep);

                void setResolutions(cv::Size camSize,
                                    cv::Size projSize);

                void addStep(int step,
                             const std::map<int, cv::Point2f> &camPoints,
                             const cv::Mat &camMatch);

                double calibrate(Camera &camParameters,
                                 Camera &projParameters,
                                 CameraProjectorLinearStageProjectionOption
                                     &camOption,
                                 CameraProjectorLinearStageProjectionOption
                                     &projOption,
                                 const std::string &pointsOutputPattern=std::string(),
                                 bool verbose=true);

                CL3DS_PRIV_IMPL
        };

The constructor can be passed two ``cv::Size`` corresponding to the camera and 
projector resolutions, or they can be later passed with
:func:`CameraProjectorCorrespondenceMapCalibrator::setResolutions`.

.. function:: void CameraProjectorLinearStageCalibrator::setScale(double scale)

    This fixes the overall scene scale to convert from pixels to scene unit.
    It should correspond to the ratio between the size of a marker (square, tag, fiducial markers...) in pixels
    and its size in world unit (``scale = 20 px / 1 mm``).

.. function:: void CameraProjectorLinearStageCalibrator::setZRange(double zmin, double zmax, double zstep)

    This fixes the **Z** interval of the translation stage.
    It defines each position the stage will stop at : :math:`z = \{ zmin, zmin + zstep, zmin + 2*zstep, \ldots, zmax \}`

.. function:: void CameraProjectorLinearStageCalibrator::setGridPoints(const std::map<int, cv::Point2f> &gridPoints)

    This is the coordinates of all points of interest on the grid.
    This is a mapping between indices and 2D coordinates of points on the grid image.
    If using ``TinyTag``, the indices are simply the fiducial markers id, and they
    can be detected automatically. Otherwise it is the caller's responsibility
    to ensure that the ids given here are consistent with subsequent calls to
    addStep.

.. function:: void CameraProjectorLinearStageCalibrator::addStep(int step, const std::map<int, cv::Point2f> &camPoints, const cv::Mat &camMatch)

    Adds a set of detected points of the grid in the camera while it was positioned at :math:`z = zmin + step*zstep`
    as well as a correspondence map between the camera and the projector.
    Not all points of the grid need be detected and passed to this function.
    The projector points are automatically computed from the correspondence map.
    The 3D grid points are also automatically computed from the original mapping
    between grid indices and 2D points, the overall scene scale and the **Z** step.

.. function:: double CameraProjectorLinearStageCalibrator::calibrate(Camera &camParameters, Camera &projParameters, CameraProjectorLinearStageProjectionOption &camOption, CameraProjectorLinearStageProjectionOption &projOption, const std::string &pointsOutputPattern=std::string(), bool verbose=true)

    This computes the camera and projector geometric calibration parameters from the
    complete set of computed 2D-3D correspondences.
    The options are instances of ``CameraProjectorLinearStageProjectionOption`` which can
    fix some parameters like the radial distortion, or the **Z** step if its known with 
    good precision.



.. _section-ref-devices:

-------
Devices
-------

.. TODO::

    Document this section.
