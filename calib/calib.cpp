/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ceres/ceres.h>
#include <mvg/mvg.h>
#include <mvg/fundam.h>
#include <mvg/resect.h>
#include <mvg/logger.h>

#include "../core/object_p.h"
#include "calib.h"
#include "calib_p.h"

using namespace std;
using namespace cv;
using namespace ceres;

namespace cl3ds
{
    PinholeProjectionOption::PinholeProjectionOption() :
        m_structureFixed(false),
        m_motionFixed(false),
        m_focalXFixed(false),
        m_aspectRatioFixed(false),
        m_principalPointFixed(false),
        m_skewFixed(false),
        m_distCoeffsFixed(false),
        m_rotFixed(false),
        m_transFixed(false) { }

    PinholeProjectionOption::~PinholeProjectionOption()
    { }

    PinholeProjectionOption & PinholeProjectionOption::skewFixed()
    {
        m_skewFixed = true;

        return *this;
    }

    PinholeProjectionOption & PinholeProjectionOption::distCoeffsFixed()
    {
        m_distCoeffsFixed = true;

        return *this;
    }

    PinholeProjection::PinholeProjection(const cv::Point2d &imagePoint)
    {
        pt2d[0] = imagePoint.x;
        pt2d[1] = imagePoint.y;
    }

    PinholeProjectionParameters::PinholeProjectionParameters(
        const CameraGeometricParameters &cam)
    {

        if (!cam.empty())
        {
            std::fill(distCoeffs, distCoeffs + 4, 0);

            double  _t[3], _K[9], _R[9], _c[3];
            cv::Mat K(3, 3, CV_64F, _K), t(3, 1, CV_64F, _t), R(3, 3, CV_64F, _R);
            cv::Mat c(3, 1, CV_64F, _c);

            cam.K().copyTo(K);
            cam.R().copyTo(R);
            cam.t().copyTo(t);

            transpose(R, R);
            ceres::RotationMatrixToAngleAxis(_R, _c);
            ceres::AngleAxisToQuaternion(_c, rot);

            // normalize the origin of the camera
            /*
             *   c = scale*(-R.t()*t - Mat(med));
             *   t = -R*c;
             */
            std::copy(_t, _t + 3, trans);

            focalX[0]         = _K[0 * 3 + 0];
            aspectRatio[0]    = _K[1 * 3 + 1] / _K[0 * 3 + 0];
            principalPoint[0] = _K[0 * 3 + 2];
            principalPoint[1] = _K[1 * 3 + 2];
            skew[0]           = _K[0 * 3 + 1];

            imageWidth  = cam.imageSize().width;
            imageHeight = cam.imageSize().height;
        }
    }

    PinholeProjectionParameters::operator CameraGeometricParameters() const
    {
        CameraGeometricParameters c;

        double  _R[9];
        cv::Mat R(3, 3, CV_64F, _R);
        ceres::QuaternionToRotation(rot, _R);
        // transpose(R, R);
        c.setRotationMatrix(R);

        /*
         *   //unnormalize ..
         *   double _c[3], _t[3], _m[3] = {med[0], med[1], med[2]};
         *   Mat c(3, 1, CV_64F, _c);
         *   Mat t(3, 1, CV_64F, _t);
         *   Mat m(3, 1, CV_64F, _m);
         *   std::copy(trans, trans+3, _t);
         *   c = -camera.R.t()*t;
         *   c = (c + m) / scale;
         *   t = -R*c;
         *   t.copyTo(camera.t);
         */
        cv::Mat t(3, 1, CV_64F, const_cast<double *>(trans));
        c.setTranslationVector(t);

        double _K[9];
        std::fill(_K, _K + 9, 0);
        _K[2 * 3 + 2] = 1;
        cv::Mat K(3, 3, CV_64F, _K);
        _K[0 * 3 + 0] = focalX[0];
        _K[1 * 3 + 1] = _K[0 * 3 + 0] * aspectRatio[0];
        _K[0 * 3 + 2] = principalPoint[0];
        _K[1 * 3 + 2] = principalPoint[1];
        _K[0 * 3 + 1] = skew[0];
        c.setIntrinsicMatrix(K);

        cv::Mat coeffs(4, 1, CV_64F, const_cast<double *>(distCoeffs));
        c.setDistorsionCoefficients(coeffs);

        c.setImageSize(Size(imageWidth, imageHeight));

        return c;
    }

    CameraProjectorLinearStageProjectionOption::
    CameraProjectorLinearStageProjectionOption
        (const cl3ds::PinholeProjectionOption &opt) :
        PinholeProjectionOption(opt),
        m_zStepFixed(false),
        m_scaleFixed(false) { }

    CameraProjectorLinearStageProjectionOption::~
    CameraProjectorLinearStageProjectionOption()
    { }

    CameraProjectorLinearStageProjection::CameraProjectorLinearStageProjection
        (const Point2d &i,
        const Point2d  &g,
        double          zi,
        double          za,
        int             z) :
        PinholeProjection(i),
        zId(z),
        zmin(zi),
        zmax(za)
    {
        gridPt[0] = g.x;
        gridPt[1] = g.y;
    }

#define PRIV_PTR Private * const priv = \
    static_cast<Private *>(Object::privateImplementation());
#define PRIV_CONST_PTR const Private * const priv = \
    static_cast<const Private *>(Object::privateImplementation());

    struct CameraProjectorCalibrator::Private : public Object::AbstractPrivate
    {
        Private(CameraProjectorCalibrator                  *ptr,
                CameraProjectorCalibrator::AbstractPrivate *pimpl) :
            Object::AbstractPrivate(ptr),
            impl(pimpl)
        { }
        ~Private();

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Object constructor";
            }
        }

        bool read(const FileNode &fn)
        {
            checkImplPtr();

            impl->read(fn);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            checkImplPtr();

            impl->write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            checkImplPtr();

            impl->configure(is, os);

            return true;
        }

        size_t hash() const
        {
            checkImplPtr();

            return impl->hash();
        }

        /* If calibrators have a common interface, virtual functions can be added here. */

        CameraProjectorCalibrator::AbstractPrivate *impl;
        CL3DS_PUB_IMPL(CameraProjectorCalibrator)
    };

    CameraProjectorCalibrator::Private::~Private()
    { }

    static Ptr< Factory<string, CameraProjectorCalibrator> > makeFactory()
    {
        Ptr< Factory<string, CameraProjectorCalibrator> > f =
            makePtr< Factory<string, CameraProjectorCalibrator> >();
        f->reg<CameraProjectorCorrespondenceMapCalibrator>();
        f->reg<CameraProjectorLinearStageCalibrator>();

        return f;
    }

    Factory<string, CameraProjectorCalibrator> *CameraProjectorCalibrator::classFactory()
    {
        static Ptr< Factory<string, CameraProjectorCalibrator> > factory = makeFactory();

        return factory;
    }

    CameraProjectorCalibrator::CameraProjectorCalibrator(AbstractPrivate *ptr) :
        Object(new Private(this, ptr))
    { }

    CameraProjectorCalibrator::~CameraProjectorCalibrator()
    { }

    void CameraProjectorCalibrator::setPrivateImplementation(
        CameraProjectorCalibrator::AbstractPrivate *pimpl)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = pimpl;
    }

    CameraProjectorCalibrator::AbstractPrivate *
    CameraProjectorCalibrator::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const CameraProjectorCalibrator::AbstractPrivate *
    CameraProjectorCalibrator::privateImplementation() const
    {
        PRIV_CONST_PTR;

        return priv->impl;
    }

    CameraProjectorCalibrator::AbstractPrivate::AbstractPrivate(
        CameraProjectorCalibrator *device) :
        Object::AbstractPrivate(device)
    { }

    CameraProjectorCalibrator::AbstractPrivate::~AbstractPrivate()
    { }

    struct CameraProjectorLinearStageCalibrator::Private : public AbstractPrivate
    {
        Private(CameraProjectorLinearStageCalibrator *ptr,
                Size                                  camSize,
                Size                                  projSize) :
            AbstractPrivate(ptr),
            imageSizes(2),
            tagIds(2),
            zIds(2),
            imagePoints(2)
        {
            setResolutions(camSize, projSize);
        }
        ~Private();

        void setResolutions(Size camSize,
                            Size projSize)
        {
            imageSizes[0] = camSize;
            imageSizes[1] = projSize;
        }

        map<int, Point2f>         gridPoints;
        double                    scale; // overall scale for tags from pixels to world units
        double                    zstep; // distance between two stage steps
        double                    zmin, zmax;
        vector<Size>              imageSizes;
        vector< vector<int> >     tagIds;
        vector< vector<int> >     zIds;
        vector< vector<Point2d> > imagePoints;

        bool read(const FileNode &node)
        {
            int numCameras_;
            node["numCameras"] >> numCameras_;
            size_t numCameras = static_cast<size_t>(numCameras_);

            imageSizes.resize(numCameras);
            tagIds.resize(numCameras);
            zIds.resize(numCameras);
            imagePoints.resize(numCameras);

            Mat tmp;
            for (size_t i = 0; i < numCameras; ++i)
            {
                node[format("imageSizes_%d", i)] >> imageSizes[i];
                node[format("points2d_%d", i)] >> tmp;
                imagePoints[i] = tmp;
                node[format("tagIds_%d", i)] >> tmp;
                tagIds[i] = tmp;
                node[format("zIds_%d", i)] >> tmp;
                zIds[i] = tmp;
            }

            FileNode gpNode = node["gridPoints"];
            for (FileNodeIterator it = gpNode.begin(), itE = gpNode.end();
                 it != itE; ++it)
            {
                Point2f p;
                int     id;
                (*it) >> id; ++it;
                (*it) >> p;
                gridPoints[id] = p;
            }

            node["scale"] >> scale;
            node["zstep"] >> zstep;
            node["zmin"] >> zmin;
            node["zmax"] >> zmax;

            return true;
        }

        bool write(FileStorage &fs) const
        {
            const size_t numCameras = imagePoints.size();
            fs << "numCameras" << int(numCameras);
            for (size_t i = 0; i < numCameras; ++i)
            {
                fs << format("imageSizes_%d", i) << imageSizes[i];
                fs << format("points2d_%d", i) << Mat(imagePoints[i]);
                fs << format("tagIds_%d", i) << Mat(tagIds[i]);
                fs << format("zIds_%d", i) << Mat(zIds[i]);
            }
            fs << "gridPoints" << "[";
            for (map<int, Point2f>::const_iterator it = gridPoints.begin(),
                 itE = gridPoints.end(); it != itE; ++it)
            {
                fs << it->first << it->second;
            }
            fs << "]";
            fs << "scale" << scale;
            fs << "zstep" << zstep;
            fs << "zmin" << zmin;
            fs << "zmax" << zmax;

            return true;
        }

        bool configure(istream *,
                       ostream *)
        {
            return true;
        }

        size_t hash() const
        {
            return 0;
        }

        CL3DS_PUB_IMPL(CameraProjectorLinearStageCalibrator)
    };

    CameraProjectorLinearStageCalibrator::CameraProjectorLinearStageCalibrator
        (Size camSize,
        Size  projSize) :
        CameraProjectorCalibrator(new Private(this, camSize, projSize))
    { }

    CameraProjectorLinearStageCalibrator::~CameraProjectorLinearStageCalibrator()
    { }

    CameraProjectorLinearStageCalibrator::Private::~Private()
    { }

    void CameraProjectorLinearStageCalibrator::setGridPoints
        (const map<int, Point2f> &gridPoints)
    {
        CL3DS_PRIV_PTR(CameraProjectorLinearStageCalibrator);

        priv->gridPoints = gridPoints;
    }

    void CameraProjectorLinearStageCalibrator::setScale(double scale)
    {
        CL3DS_PRIV_PTR(CameraProjectorLinearStageCalibrator);

        priv->scale = scale;
    }

    void CameraProjectorLinearStageCalibrator::setZRange(double zmin,
                                                         double zmax,
                                                         double zstep)
    {
        CL3DS_PRIV_PTR(CameraProjectorLinearStageCalibrator);

        priv->zstep = zstep;
        priv->zmin  = zmin;
        priv->zmax  = zmax;
    }

    void CameraProjectorLinearStageCalibrator::setResolutions(Size camSize,
                                                              Size projSize)
    {
        CL3DS_PRIV_PTR(CameraProjectorLinearStageCalibrator);

        priv->setResolutions(camSize, projSize);
    }

    void CameraProjectorLinearStageCalibrator::addStep(int step,
                                                       const map<int, Point2f> &camPoints,
                                                       const Mat &camMatch,
                                                       const Mat &camMask)
    {
        CL3DS_PRIV_PTR(CameraProjectorLinearStageCalibrator);

        for (map<int, Point2f>::const_iterator it = camPoints.begin(),
             itE = camPoints.end(); it != itE; ++it)
        {
            // should we have detected this tag ?
            if (priv->gridPoints.find(it->first) != priv->gridPoints.end())
            {
                int id = it->first;
                priv->tagIds[0].push_back(id);
                priv->zIds[0].push_back(step);

                Point2f p = it->second;
                priv->imagePoints[0].push_back(p);

                // si les points ne sont pas visibles du proj, meme pas la peine !
                if (!camMask.empty())
                {
                    int x0 = int(p.x), x1 = x0 + 1;
                    int y0 = int(p.y), y1 = y0 + 1;
                    if ((camMask.ptr<uchar>(y0)[x0] == 0) ||
                        (camMask.ptr<uchar>(y0)[x1] == 0) ||
                        (camMask.ptr<uchar>(y1)[x0] == 0) ||
                        (camMask.ptr<uchar>(y1)[x1] == 0))
                    {
                        continue;
                    }
                }

                priv->tagIds[1].push_back(id);
                priv->zIds[1].push_back(step);

                // ajoute les points transferes dans le proj
                Mat patch;
                remap(camMatch, patch, Mat(1, 1, CV_32FC2, &p),
                      noArray(), INTER_LINEAR, BORDER_REFLECT_101);
                Vec3w val = patch.at<Vec3w>(0, 0);

                double x, y;
                pointFromMatch(val, priv->imageSizes[1], x, y);
                Point2f q(static_cast<float>(x), static_cast<float>(y));
                priv->imagePoints[1].push_back(q);
            }
        }
    }

    void CameraProjectorLinearStageCalibrator::setData(
        const std::vector<std::vector<Point2d> > &imagePoints,
        const std::vector<std::vector<int> >     &tagIds,
        const std::vector<std::vector<int> >     &zIds)
    {
        CL3DS_PRIV_PTR(CameraProjectorLinearStageCalibrator);

        priv->imagePoints = imagePoints;
        priv->zIds        = zIds;
        priv->tagIds      = tagIds;
    }

    double CameraProjectorLinearStageCalibrator::calibrate(
        CameraGeometricParameters                  &camParameters,
        CameraGeometricParameters                  &projParameters,
        CameraProjectorLinearStageProjectionOption &camOption,
        CameraProjectorLinearStageProjectionOption &projOption,
        const string                               &pointsOutputPattern,
        bool                                        verbose)
    {
        CL3DS_PRIV_PTR(CameraProjectorLinearStageCalibrator);

        vector<CameraGeometricParameters> parameters(2);
        double                            _K[9];
        Mat                               K(3, 3, CV_64F, _K), R, t, M, d(4, 1, CV_64F);
        vector<bool>                      mask;

        for (size_t i = 0; i < 2; ++i)
        {
            size_t          count = priv->imagePoints[i].size();
            vector<Point3d> objectPoints(count);
            for (size_t j = 0; j < count; ++j)
            {
                int tagId = priv->tagIds[i][j];
                int zId   = priv->zIds[i][j];
                objectPoints[j] = Point3d(priv->gridPoints[tagId].x * priv->scale,
                                          priv->gridPoints[tagId].y * priv->scale,
                                          priv->zmin + zId * priv->zstep);
            }

            Size imageSize = priv->imageSizes[i];

            findResectionMatrix(Mat(objectPoints),
                                Mat(priv->imagePoints[i]),
                                M, &mask, RM_RANSAC, 3, 0.99);

            decompositionKRT(M, K, R, t);

            // no skew !
            _K[0 * 3 + 1] = 0;
            // no pp outside image !
            if (_K[0 * 3 + 2] >= imageSize.width)
            {
                _K[0 * 3 + 2] = imageSize.width - 1;
            }
            if (_K[1 * 3 + 2] >= imageSize.height)
            {
                _K[1 * 3 + 2] = imageSize.height - 1;
            }

            if (verbose)
            {
                logInfo() << "K initialized to\n" << K << endl;
            }

            parameters[i] = CameraGeometricParameters(K.clone(), R.clone(),
                                                      t.clone(), d.clone(), imageSize);

            if (!pointsOutputPattern.empty())
            {
                FileStorage fs;
                string      path = format(pointsOutputPattern.c_str(), i);
                if (!fs.open(path, FileStorage::WRITE))
                {
                    logError() << "Could not open" << path;
                }
                fs << "points2D" << Mat(priv->imagePoints[i]);
                fs << "tagIds" << Mat(priv->tagIds[i]);
                fs << "zIds" << Mat(priv->zIds[i]);
                fs << "scale" << priv->scale;
                fs << "zstep" << priv->zstep;
            }
        }

        Problem problem;

        double zstep = priv->zstep;
        double scale = priv->scale;

        vector<CameraProjectorLinearStageProjectionOption> options(2);
        options[0] = camOption;
        options[1] = projOption;

        vector<PinholeProjectionParameters> params(2);
        for (size_t i = 0; i < 2; ++i)
        {
            params[i] = parameters[i];

            size_t count = priv->imagePoints[i].size();
            for (size_t j = 0; j < count; ++j)
            {
                CameraProjectorLinearStageProjection *p =
                    new CameraProjectorLinearStageProjection(
                        priv->imagePoints[i][j],
                        priv->gridPoints[priv->tagIds[i][j]],
                        priv->zmin,
                        priv->zmax,
                        priv->zIds[i][j]);
                typedef AutoDiffCostFunction<CameraProjectorLinearStageProjection, 2, 4,
                                             3, 1, 1, 2, 1, 4, 1, 1> Function;
                Function *f = new Function(p);
                problem.AddResidualBlock(f,
                                         new HuberLoss(1.),
                                         params[i].rot,
                                         params[i].trans,
                                         params[i].focalX,
                                         params[i].aspectRatio,
                                         params[i].principalPoint,
                                         params[i].skew,
                                         params[i].distCoeffs,
                                         &scale,
                                         &zstep);
            }

            problem.SetParameterization(params[i].rot,
                                        new ceres::QuaternionParameterization);

            // test camera options ...
            if (options[i].m_motionFixed || options[i].m_rotFixed)
            {
                problem.SetParameterBlockConstant(params[i].rot);
            }
            if (options[i].m_motionFixed || options[i].m_transFixed)
            {
                problem.SetParameterBlockConstant(params[i].trans);
            }
            if (options[i].m_motionFixed || options[i].m_focalXFixed)
            {
                problem.SetParameterBlockConstant(params[i].focalX);
            }
            if (options[i].m_motionFixed || options[i].m_aspectRatioFixed)
            {
                problem.SetParameterBlockConstant(params[i].aspectRatio);
            }
            if (options[i].m_motionFixed || options[i].m_principalPointFixed)
            {
                problem.SetParameterBlockConstant(params[i].principalPoint);
            }
            if (options[i].m_motionFixed || options[i].m_skewFixed)
            {
                problem.SetParameterBlockConstant(params[i].skew);
            }
            if (options[i].m_motionFixed || options[i].m_distCoeffsFixed)
            {
                problem.SetParameterBlockConstant(params[i].distCoeffs);
            }

            // test scale and zstep options
            if (options[i].m_structureFixed || options[i].m_zStepFixed)
            {
                problem.SetParameterBlockConstant(&zstep);
            }
            if (options[i].m_scaleFixed || options[i].m_scaleFixed)
            {
                problem.SetParameterBlockConstant(&scale);
            }
        }

        // fix one Rt
        problem.SetParameterBlockConstant(params[0].rot);
        problem.SetParameterBlockConstant(params[0].trans);

        Solver::Options _options;
        _options.max_num_iterations = 5000;
        // options.gradient_tolerance = 1e-16;
        // options.function_tolerance = 1e-16;
        // options.linear_solver_type = ceres::DENSE_QR;
        // options.minimizer_progress_to_stdout = true;

        Solver::Summary summary;
        Solve(_options, &problem, &summary);
        if (verbose)
        {
            logInfo() << summary.FullReport();
        }

        camParameters = params[0];
        if (verbose)
        {
            logInfo() << "K refined to\n" << camParameters.K() << endl;
        }

        projParameters = params[1];
        if (verbose)
        {
            logInfo() << "K refined to\n" << projParameters.K() << endl;
        }

        return summary.final_cost;
    }

    struct CameraProjectorCorrespondenceMapCalibrator::Private : public AbstractPrivate
    {
        Private(CameraProjectorCorrespondenceMapCalibrator *ptr,
                const Mat                                  &camK,
                const Mat                                  &projK,
                Size                                        camSize,
                Size                                        projSize) :
            AbstractPrivate(ptr),
            imageSizes(2),
            initialCameras(2),
            fundamImagePoints(2),
            imagePoints(2)
        {
            setInitialParameters(camK, projK, camSize, projSize);
        }
        ~Private();

        void setInitialParameters(const Mat &camK,
                                  const Mat &projK,
                                  Size       camSize,
                                  Size       projSize)
        {
            imageSizes[0]     = camSize;
            imageSizes[1]     = projSize;
            initialCameras[0] = camK;
            initialCameras[1] = projK;
        }

        bool read(const FileNode &) { return true; }
        bool write(FileStorage &) const { return true; }
        bool configure(istream *,
                       ostream *) { return true; }
        size_t hash() const { return 0; }

        vector<Size>              imageSizes;
        vector<Mat>               initialCameras;
        vector< vector<Point2d> > fundamImagePoints;
        vector< vector<Point2d> > imagePoints;

        CL3DS_PUB_IMPL(CameraProjectorCorrespondenceMapCalibrator)
    };

    CameraProjectorCorrespondenceMapCalibrator::CameraProjectorCorrespondenceMapCalibrator
        (const Mat &camK,
        const Mat  &projK,
        Size        camSize,
        Size        projSize) :
        CameraProjectorCalibrator(new Private(this, camK, projK, camSize, projSize))
    { }

    CameraProjectorCorrespondenceMapCalibrator::~
    CameraProjectorCorrespondenceMapCalibrator()
    { }

    CameraProjectorCorrespondenceMapCalibrator::Private::~Private()
    { }

    void CameraProjectorCorrespondenceMapCalibrator::setInitialParameters(const Mat &camK,
                                                                          const Mat &projK,
                                                                          Size       camSize,
                                                                          Size       projSize)
    {
        CL3DS_PRIV_PTR(CameraProjectorCorrespondenceMapCalibrator);

        priv->setInitialParameters(camK, projK, camSize, projSize);
    }

    void CameraProjectorCorrespondenceMapCalibrator::addCorrespondenceMap(
        const Mat &match,
        const Mat &mask,
        int        maxPoints)
    {
        CL3DS_PRIV_PTR(CameraProjectorCorrespondenceMapCalibrator);

        Size camSize  = priv->imageSizes[0];
        Size projSize = priv->imageSizes[1];

#if 0
        int ycmin = camSize.height * 0.2;
        int ycmax = camSize.height * 0.8;
        int xcmin = camSize.width * 0.2;
        int xcmax = camSize.width * 0.8;
        int ypmin = projSize.height * 0.2;
        int ypmax = projSize.height * 0.8;
        int xpmin = projSize.width * 0.2;
        int xpmax = projSize.width * 0.8;
#else
        int ycmin = 0;
        int ycmax = camSize.height;
        int xcmin = 0;
        int xcmax = camSize.width;
        int ypmin = 0;
        int ypmax = projSize.height;
        int xpmin = 0;
        int xpmax = projSize.width;
#endif

        vector<Point2d> p1s, p2s;
        for (int yc = 0; yc < camSize.height; ++yc)
        {
            const uchar *maskPtr  = (mask.empty() ? 0 : mask.ptr<uchar>(yc));
            const Vec3w *matchPtr = match.ptr<Vec3w>(yc);

            for (int xc = 0; xc < camSize.width; ++xc)
            {
                if (!maskPtr || (maskPtr[xc] == 0)) { continue; }

                // trouve la correpondance dans le proj
                double xp, yp;
                pointFromMatch(matchPtr[xc], projSize, xp, yp);

                if (!((xc < xcmin) || (xc > xcmax) || (yc < ycmin) || (yc > ycmax) ||
                      (xp < xpmin) || (xp > xpmax) || (yp < ypmin) || (yp > ypmax)))
                {
                    priv->fundamImagePoints[0].push_back(Point2d(xc + 0.5, yc + 0.5));
                    priv->fundamImagePoints[1].push_back(Point2d(xp + 0.5, yp + 0.5));
                }

                p1s.push_back(Point2d(xc + 0.5, yc + 0.5));
                p2s.push_back(Point2d(xp + 0.5, yp + 0.5));
            }
        }

        size_t         size = p1s.size();
        vector<size_t> index(size);
        for (size_t i = 0; i < size; ++i)
        {
            index[i] = i;
        }
        random_shuffle(index.begin(), index.end());

        size = std::min(size, static_cast<size_t>(maxPoints));
        for (size_t i = 0; i < size; ++i)
        {
            priv->imagePoints[0].push_back(p1s[index[i]]);
            priv->imagePoints[1].push_back(p2s[index[i]]);
        }
    }

    double CameraProjectorCorrespondenceMapCalibrator::calibrate(
        CameraGeometricParameters &camParameters,
        CameraGeometricParameters &projParameters,
        PinholeProjectionOption   &camOption,
        PinholeProjectionOption   &projOption,
        const string              &pointsOutputPattern,
        bool                       verbose)
    {
        CL3DS_PRIV_PTR(CameraProjectorCorrespondenceMapCalibrator);
        CL3DS_UNUSED(pointsOutputPattern);

        Size camSize  = priv->imageSizes[0];
        Size projSize = priv->imageSizes[1];

        vector<Point2d> p1s  = priv->fundamImagePoints[0];
        vector<Point2d> p2s  = priv->fundamImagePoints[1];
        size_t          size = p1s.size();

        vector<bool> mask(size);
        Mat          F;
        int          ret = findFundamentalMatrix(Mat(p1s), Mat(
                                                     p2s), F, &mask, FM_RANSAC, 3.0, 0.99, camSize,
                                                 projSize);
        if (ret == 0)
        {
            logWarning() <<
                "Could not calibrate based on the correspondence map : no fundamental matrix found.";

            return -1;
        }

        size_t k = 0;
        for (size_t i = 0; i < size; ++i)
        {
            if (mask[i])
            {
                p1s[k] = p1s[i];
                p2s[k] = p2s[i];
                // logInfo() << p1s[j] << p2s[j] << squaredDistanceToEpipolarLine(F, p1s[i],
                // p2s[i]);
                ++k;
            }
        }
        p1s.resize(k);
        p2s.resize(k);

        Mat E;
        essentialFromFundamentalMatrix(F, priv->initialCameras[0],
                                       priv->initialCameras[1], E);

        Mat R, t;
        poseFromEssentialMatrix(E, priv->initialCameras[0], priv->initialCameras[1],
                                Mat(p1s), Mat(p2s),
                                camSize, projSize, R, t);

        vector<CameraGeometricParameters> parameters(2);
        parameters[0].setIntrinsicMatrix(priv->initialCameras[0]);
        parameters[0].setRotationMatrix(Mat::eye(3, 3, CV_64F));
        parameters[0].setTranslationVector(Mat::zeros(3, 1, CV_64F));
        parameters[0].setImageSize(priv->imageSizes[0]);

        parameters[1].setIntrinsicMatrix(priv->initialCameras[1]);
        parameters[1].setRotationMatrix(R);
        parameters[1].setTranslationVector(t);
        parameters[1].setImageSize(priv->imageSizes[1]);

        Mat         pts3d;
        vector<Mat> imagePoints(2);
        for (size_t i = 0; i < 2; ++i)
        {
            imagePoints[i] = Mat(priv->imagePoints[i]);
        }

        size = priv->imagePoints[0].size();
        Mat vmask(2, static_cast<int>(size), CV_8U, Scalar::all(1));
        triangulatePoints(imagePoints, parameters, vmask, camSize, pts3d);

        double  *objectPoints = new double[size * 3];
        Point3d *ptr          = pts3d.ptr<Point3d>();
        for (size_t i = 0; i < size; ++i)
        {
            objectPoints[i * 3 + 0] = ptr[i].x;
            objectPoints[i * 3 + 1] = ptr[i].y;
            objectPoints[i * 3 + 2] = ptr[i].z;
        }

        Problem                         problem;
        vector<PinholeProjectionOption> options(2);
        options[0] = camOption;
        options[1] = projOption;

        vector<PinholeProjectionParameters> params(2);

        for (size_t i = 0; i < 2; ++i)
        {
            params[i] = parameters[i];

            for (size_t j = 0; j < size; ++j)
            {
                PinholeProjection *p = new PinholeProjection(priv->imagePoints[i][j]);
                typedef AutoDiffCostFunction<PinholeProjection, 2, 4, 3, 1, 1, 2, 1, 4, 3>
                    Function;
                Function *f = new Function(p);
                problem.AddResidualBlock(f,
                                         new HuberLoss(1.),
                                         params[i].rot,
                                         params[i].trans,
                                         params[i].focalX,
                                         params[i].aspectRatio,
                                         params[i].principalPoint,
                                         params[i].skew,
                                         params[i].distCoeffs,
                                         objectPoints + j * 3);
            }

            problem.SetParameterization(params[i].rot,
                                        new ceres::QuaternionParameterization);

            // test camera options ...
            if (options[i].m_motionFixed || options[i].m_rotFixed)
            {
                problem.SetParameterBlockConstant(params[i].rot);
            }
            if (options[i].m_motionFixed || options[i].m_transFixed)
            {
                problem.SetParameterBlockConstant(params[i].trans);
            }
            if (options[i].m_motionFixed || options[i].m_focalXFixed)
            {
                problem.SetParameterBlockConstant(params[i].focalX);
            }
            if (options[i].m_motionFixed || options[i].m_aspectRatioFixed)
            {
                problem.SetParameterBlockConstant(params[i].aspectRatio);
            }
            if (options[i].m_motionFixed || options[i].m_principalPointFixed)
            {
                problem.SetParameterBlockConstant(params[i].principalPoint);
            }
            if (options[i].m_motionFixed || options[i].m_skewFixed)
            {
                problem.SetParameterBlockConstant(params[i].skew);
            }
            if (options[i].m_motionFixed || options[i].m_distCoeffsFixed)
            {
                problem.SetParameterBlockConstant(params[i].distCoeffs);
            }
        }

        // fix one Rt
        problem.SetParameterBlockConstant(params[0].rot);
        problem.SetParameterBlockConstant(params[0].trans);

        // fix 3D points
        for (size_t j = 0; j < size; ++j)
        {
            problem.SetParameterBlockConstant(objectPoints + j * 3);
        }

        Solver::Options _options;
        _options.max_num_iterations = 5000;
        // options.gradient_tolerance = 1e-16;
        // options.function_tolerance = 1e-16;
        // options.linear_solver_type = ceres::DENSE_QR;
        if (verbose) { _options.minimizer_progress_to_stdout = true; }

        Solver::Summary summary;
        Solve(_options, &problem, &summary);
        if (verbose)
        {
            std::cout << summary.FullReport() << "\n";
        }

        camParameters = params[0];
        if (verbose)
        {
            logInfo() << "K refined to\n" << camParameters.K() << endl;
        }

        projParameters = params[1];
        if (verbose)
        {
            logInfo() << "K refined to\n" << projParameters.K() << endl;
        }

        delete[] objectPoints;

        return summary.final_cost;
    }
}

#if CV_MAJOR_VERSION < 3
namespace cv
{
    template <>
    void Ptr<cl3ds::CameraProjectorCalibrator::AbstractPrivate>::delete_obj()
    {
        delete obj;
    }
}
#endif
