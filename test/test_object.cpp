/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <mvg/logger.h>
#include "../core/object.h"
#include "../core/object_p.h"

using namespace cv;
using namespace cl3ds;
using namespace std;

struct MyObject : public Object
{
    struct Private : public AbstractPrivate
    {
        Private(MyObject *ptr) : AbstractPrivate(ptr) { }
        ~Private();
        bool read(const FileNode &) { return true; }
        bool write(FileStorage &) const { return true; }
        bool configure(istream *,
                       ostream *) { return true; }
        size_t hash() const { return 0; }
    };

    MyObject() : Object(new Private(this)) { }
    ~MyObject();

    CL3DS_CLASS_NAME("myobj")
};

struct MyObject2 : public Object
{
    struct Private : public AbstractPrivate
    {
        Private(Object *ptr) : AbstractPrivate(ptr) { }
        ~Private();
        bool read(const FileNode &) { return true; }
        bool write(FileStorage &) const { return true; }
        bool configure(istream *,
                       ostream *) { return true; }
        size_t hash() const { return 0; }
    };

    MyObject2() : Object(new Private(this)) { }
    ~MyObject2();

    CL3DS_CLASS_NAME("myobj2")
};

int main()
{
    MyObject o;
    string   name = o.objectName();

    MyObject2 o2;
    if (o2.objectName() != "myobj2")
    {
        cerr << "Wrong class name for o2 : " << o2.objectName() << endl;

        return 1;
    }

    return 0;
}

// to avoid -Wweak-pointers
MyObject::~MyObject() { }
MyObject::Private::~Private() { }
MyObject2::~MyObject2() { }
MyObject2::Private::~Private() { }

