/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <typeinfo>
#include <map>
#include <stdexcept>
#include <sys/time.h>
#include "../core/matdispatcher.h"
#include "../core/typelist.h"

using namespace std;
using namespace cv;
using namespace cl3ds;

struct Doer1D
{
    template <class T>
    void operator()(const Mat &m_)
    {
        Mat m = m_;
        for (int j = 0; j < m.rows; ++j)
        {
            T *ptr = m.ptr<T>(j);
            for (int i = 0; i < m.cols; ++i)
            {
                ptr[i] = saturate_cast<T>(
                    rand() * 2000 / double(RAND_MAX) - 1000);
            }
        }
    }
};

struct Doer2D
{
    template <class T1, class T2>
    string operator()(const string &s)
    {
        ostringstream oss;
        oss << s << " " << typeid(T1).name() << " " << typeid(T2).name();

        return oss.str();
    }
};

struct Test
{
    template <class T>
    Ptr<int> operator()(const Ptr<string> &ptr)
    {
        Ptr<string> s = ptr;
        s->append(typeid(T).name());
        logInfo() << *s;

        return makePtr<int>(1);
    }
};

class Test2
{
    public:
        template <class T>
        bool operator()(const int &)
        // bool operator()(const int&) const
        {
            return true;
        }
};

int main()
{
    Doer1D d1;
    Mat    m;
    m.create(3, 3, CV_8U);
    dispatch<void>(d1, m, CV_8U);
    logInfo() << m;

    m.create(4, 6, CV_64F);
    MatDispatcher1D<Doer1D, Mat, void, TYPELIST_2(uchar, double)> disp(d1);
    disp(CV_64F, m);
    logInfo() << m;

    Doer2D d2;
    string s = dispatch<string>(d2, string("bonjour"), CV_8U, CV_16U);
    logInfo() << s;

    Test               t;
    const Ptr<string> &s2 = makePtr<string>("bonjour");
    Ptr<int>           p  = dispatch< Ptr<int> >(t, s2, CV_8U);

    Test2 t2;
    dispatch<bool>(t2, 0, CV_16U);
}
