/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "../core/common.h"
#include "../core/parameter.h"

using namespace std;
using namespace cv;
using namespace cl3ds;

int main()
{
    TypeParameter<int>    pa, pb, pc;
    TypeParameter<double> pd;
    TypeParameter<string> pe;

    int    a, b, c;
    double d;
    string e;

    string buf;
    {
        FileStorage fs(".xml", FileStorage::WRITE + FileStorage::MEMORY);
        fs << "a" << 12;
        fs << "b" << 33;
        fs << "c" << 32;
        buf = fs.releaseAndGetString();
    }

    pa = TypeParameter<int>("a", "un a", 0,
                            makePtr<BoundConstraint<int> >(-3, 25));
    pc = TypeParameter<int>("c", "un c", 1,
                            makePtr<PositiveConstraint<int> >(20));
    pb = TypeParameter<int>("b", "un b");
    pd = TypeParameter<double>("d", "un d", 0,
                               makePtr<BoundConstraint<double> >(-4, 4));

    string         tab[]  = { "yo1", "yo2", "yo3" };
    vector<string> values = makeVector(tab);
    pe = TypeParameter<string>("e", "un e", "yo1",
                               makePtr<EnumerationConstraint<string> >(values));

    {
        FileStorage fs(buf, FileStorage::READ + FileStorage::MEMORY);
        a = pa.read(fs.root());
        b = pb.read(fs.root());
        c = pc.read(fs.root());
        d = pd.read(fs.root());
        e = pe.read(fs.root());
        logInfo() << "a is" << a << "b is" << b << "c is" << c << "d is" << d <<
            "e is" << e;
    }

    pa = 3;
    pb = 9;
    pc = -1; // invalid, warning and def value
    pd = -2.123;
    pe = "yo2";

    {
        FileStorage fs(".xml", FileStorage::WRITE + FileStorage::MEMORY);
        pa.write(fs);
        pb.write(fs);
        pc.write(fs);
        pd.write(fs);
        pe.write(fs);
        buf = fs.releaseAndGetString();
    }
    {
        FileStorage fs(buf, FileStorage::READ + FileStorage::MEMORY);
        a = pa.read(fs.root());
        b = pb.read(fs.root());
        c = pc.read(fs.root());
        d = pd.read(fs.root());
        e = pe.read(fs.root());
        logInfo() << "a is" << a << "b is" << b << "c is" << c << "d is" << d <<
            "e is" << e;
    }

    {
        a = pa.configure();
        b = pb.configure();
        c = pc.configure();
        d = pd.configure(0);
        e = pe.configure();

        FileStorage fs(".xml", FileStorage::WRITE + FileStorage::MEMORY);
        pa.write(fs);
        pb.write(fs);
        pc.write(fs);
        pd.write(fs);
        pe.write(fs);
        buf = fs.releaseAndGetString();
        logInfo() << buf;
    }

    {
        FileStorage fs(".xml", FileStorage::WRITE);
        pa.write(fs);
        pb.write(fs);
        pc.write(fs);
        pd.write(fs);
        pe.write(fs);
        buf = fs.releaseAndGetString();
        logInfo() << buf;
    }

    return 0;
}
