/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMBINER_HPP
#define COMBINER_HPP

#include "generator.h"

namespace cl3ds
{
    class CL3DS_DECLSPEC Combiner : public Generator
    {
        public:
            Combiner();
            ~Combiner();

            size_t numGenerators() const;
            Generator * generator(size_t index);
            void setGenerator(size_t             index,
                              cv::Ptr<Generator> generator);
            void removeGenerator(size_t index);
            void removeAllGenerators();
            void addGenerator(cv::Ptr<Generator> generator);

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("combiner")
    };
}

#endif /* #ifndef COMBINER_HPP */
