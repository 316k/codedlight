/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XOR_BINARY_H
#define XOR_BINARY_H

#include "generator.h"
#include "graycode.h"
#include "matcher.h"

namespace cl3ds
{
    /** Implements robust binary xor codes generation. */
    class CL3DS_DECLSPEC XorBinaryGenerator : public Generator
    {
        public:
            typedef GrayCodeGenerator::BinarizationMethod BinarizationMethod;

            enum XorOperation { Xor2, Xor4, Xor8 };

            XorBinaryGenerator(cv::Size           size=defaultSize(),
                               XorOperation       operation=defaultXorOperation(),
                               Direction          direction=defaultDirection(),
                               BinarizationMethod method=defaultBinarizationMethod());
            ~XorBinaryGenerator();

            Direction direction() const;
            void setDirection(const Direction &dir);
            static Direction defaultDirection();

            /** Pattern size of this binary code generator. */
            cv::Size size() const;
            void setSize(cv::Size size);
            static cv::Size defaultSize();

            XorOperation xorOperation() const;
            void setXorOperation(XorOperation operation);
            static XorOperation defaultXorOperation();

            BinarizationMethod binarizationMethod() const;
            void setBinarizationMethod(BinarizationMethod method);
            static BinarizationMethod defaultBinarizationMethod();

            /** The number of bits used to encode a binary code is basically log2(dimension)
             *  where dimension corresponds to the pattern width or height. */
            size_t nbBitsX() const;
            size_t nbBitsY() const;
            size_t nbBits() const;

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("xor-binary")
    };

    /** Implements robust binary xor codes matching. */
    class CL3DS_DECLSPEC XorBinaryMatcher : public Matcher
    {
        public:
            XorBinaryMatcher(const cv::Ptr<XorBinaryGenerator> & =
                                 nonOwningPtr(defaultGenerator()),
                             int=defaultMedianFilterSize());
            ~XorBinaryMatcher();

            /** Getter/setter of XorBinary generator parameters of this matcher. */
            XorBinaryGenerator * generator() const;
            void setGenerator(const cv::Ptr<XorBinaryGenerator> &);
            /** Typical XorBinary code default generation parameters, mostly used for
             *  creating default matcher. */
            static XorBinaryGenerator * defaultGenerator();

            int medianFilterSize() const;
            void setMedianFilterSize(int medianFilterSize);
            static int defaultMedianFilterSize();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("xor-binary")
    };
}

#endif /* #ifndef XOR_BINARY_H */

