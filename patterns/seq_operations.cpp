/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/core/core.hpp>
#include "../core/common.h"
#include "../core/matdispatcher.h"
#include "seq_operations.h"

using namespace cv;

namespace cl3ds
{
    ContinuousImageTransposer::~ContinuousImageTransposer() { }

    enum Operation
    {
        STATS_MIN  = 1,
        STATS_MAX  = 2,
        STATS_AMPL = STATS_MIN | STATS_MAX | 4,
        STATS_MEAN = 8,
        STATS_DEV  = STATS_MEAN | 16,
        STATS_MED  = 32
    };

    SequenceStats::SequenceStats() : m_operations(0) { }

    bool SequenceStats::minAsked() const
    {
        return op(STATS_MIN);
    }

    SequenceStats & SequenceStats::min()
    {
        m_operations |= STATS_MIN;

        return *this;
    }

    bool SequenceStats::maxAsked() const
    {
        return op(STATS_MAX);
    }

    SequenceStats & SequenceStats::max()
    {
        m_operations |= STATS_MAX;

        return *this;
    }

    bool SequenceStats::amplAsked() const
    {
        return op(STATS_AMPL);
    }

    SequenceStats & SequenceStats::ampl()
    {
        m_operations |= STATS_AMPL;

        return *this;
    }

    bool SequenceStats::meanAsked() const
    {
        return op(STATS_MEAN);
    }

    SequenceStats & SequenceStats::mean()
    {
        m_operations |= STATS_AMPL;

        return *this;
    }

    bool SequenceStats::devAsked() const
    {
        return op(STATS_DEV);
    }

    SequenceStats & SequenceStats::dev()
    {
        m_operations |= STATS_DEV;

        return *this;
    }

    bool SequenceStats::medAsked() const
    {
        return op(STATS_MED);
    }

    SequenceStats & SequenceStats::med()
    {
        m_operations |= STATS_MED;

        return *this;
    }

    bool SequenceStats::op(int oper) const
    {
        return (m_operations & oper) == oper;
    }

    struct SequenceStatsComputer
    {
        SequenceStatsComputer(Generator *const g,
                              SequenceStats    s) :
            gen(g),
            stats(s)
        { }

        template <typename T>
        MatTable operator()(const int &)
        {
            Mat minOut;
            Mat maxOut;
            Mat amplOut;
            Mat meanOut;
            Mat devOut;
            Mat medOut;

            int    type  = gen->type();
            Size   size  = gen->size();
            size_t count = gen->count();

            if (stats.minAsked())
            {
                minOut = Mat(size, type, Scalar::all(std::numeric_limits<T>::max()));
            }
            if (stats.maxAsked())
            {
                maxOut = Mat(size, type, Scalar::all(0));
            }
            if (stats.amplAsked())
            {
                amplOut = Mat(size, type, Scalar::all(0));
            }
            if (stats.meanAsked())
            {
                meanOut = Mat(size, CV_MAKETYPE(CV_64F, CV_MAT_CN(type)));
                meanOut = Scalar::all(0);
            }
            if (stats.devAsked())
            {
                devOut = Mat(size, CV_MAKETYPE(CV_64F, CV_MAT_CN(type)));
                devOut = Scalar::all(0);
            }
            if (stats.medAsked())
            {
                medOut = Mat(size, type, Scalar::all(0));
            }

            if (stats.devAsked() || stats.medAsked())
            {
                work = Mat(size, CV_MAKETYPE(CV_64F, CV_MAT_CN(type)));
                work = Scalar::all(0);
            }

            if (stats.medAsked())
            {
                transposeImages<T>(gen, work);
                MatIterator_<T> it = medOut.begin<T>();
                for (int j = 0; j < size.height; ++j)
                {
                    for (int i = 0; i < size.width; ++i, ++it)
                    {
                        T *ptr = work.ptr<T>(j * size.width + i);
                          *it  = median(ptr, ptr + count);
                    }
                }
            }
            else
            {
                int i = 0;
                while (gen->hasNext())
                {
                    Mat m = gen->get();
                    if (stats.minAsked()) { cv::min(minOut, m, minOut); }
                    if (stats.maxAsked()) { cv::max(maxOut, m, maxOut); }
                    if (stats.meanAsked()) { meanOut += Mat_<double>(m); }
                    if (stats.devAsked())
                    {
                        double di  = static_cast<double>(i);
                        Mat    tmp = Mat_<double>(m) - work;
                        devOut = devOut + di * (tmp.mul(tmp)) / (di + 1);
                        work   = work + (Mat_<double>(m) - work) / (di + 1);
                    }
                    ++i;
                }
                if (stats.meanAsked())
                {
                    meanOut /= static_cast<double>(count);
                }
                if (stats.amplAsked())
                {
                    amplOut = Mat_<T>(Mat_<double>(maxOut) - Mat_<double>(minOut));
                }
                if (stats.devAsked())
                {
                    sqrt(devOut / (count - 1.0), devOut);
                }
            }

            MatTable result;
            if (stats.minAsked())
            {
                result["min"] = minOut;
            }
            if (stats.maxAsked())
            {
                result["max"] = maxOut;
            }
            if (stats.amplAsked())
            {
                result["ampl"] = amplOut;
            }
            if (stats.meanAsked())
            {
                result["mean"] = meanOut;
            }
            if (stats.devAsked())
            {
                result["dev"] = devOut;
            }
            if (stats.medAsked())
            {
                result["med"] = medOut;
            }

            return result;
        }

        Generator *const gen;
        SequenceStats    stats;
        Mat              work;
    };

    MatTable computeSequenceStats(Generator *const gen,
                                  SequenceStats    stats)
    {
        SequenceStatsComputer                                 comp(gen, stats);
        MatDispatcher1D<SequenceStatsComputer, int, MatTable> dispatcher(comp);

        return dispatcher(gen->type(), 0);
    }
}
