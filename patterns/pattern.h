/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PATTERN_HPP
#define PATTERN_HPP

#include "generator.h"

namespace cl3ds
{
    class CL3DS_DECLSPEC Pattern : public Generator
    {
        public:
            virtual ~Pattern();

            /** Pattern size of this pattern. */
            cv::Size size() const;
            void setSize(cv::Size size);
            static cv::Size defaultSize();

        protected:
            struct AbstractPrivate;

            Pattern(AbstractPrivate *ptr,
                    cv::Size         size);

        protected:
            void setPrivateImplementation(AbstractPrivate *pimpl);
            AbstractPrivate * privateImplementation();
            const AbstractPrivate * privateImplementation() const;

        private:
            struct Private;
    };
}
#endif /* #ifndef PATTERN_HPP */
