/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../core/parameter.h"
#include "dummy.h"
#include "combiner.h"
#include "generator_p.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct Combiner::Private : public AbstractPrivate
    {
        Private(Combiner *ptr) :
            AbstractPrivate(ptr),
            currentGenIndex(0)
        {
            paramCount =
                TypeParameter<int>("count",
                                   "Number of generators to combine",
                                   0,
                                   makePtr< BoundConstraint<int> >(-1,
                                                                   numeric_limits<int>::
                                                                   max()));
        }
        ~Private();

        void addGenerator(cv::Ptr<Generator> generator)
        {
            generators.push_back(generator);
        }

        void removeGenerator(size_t index)
        {
            vector<Ptr<Generator> >::iterator it = generators.begin();
            advance(it, static_cast<ptrdiff_t>(index));
            generators.erase(it);
        }

        void removeAllGenerators()
        {
            generators.clear();
        }

        bool read(const FileNode &node)
        {
            paramCount.read(node);

            size_t count = static_cast<size_t>(paramCount);
            generators.resize(count);
            for (size_t i = 0; i < count; ++i)
            {
                string name = format("generator_%d", i);
                node[name] >> generators[i];
            }

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramCount = static_cast<int>(generators.size());
            paramCount.write(fs);

            size_t count = static_cast<size_t>(paramCount);
            for (size_t i = 0; i < count; ++i)
            {
                string name = format("generator_%d", i);
                fs << name << generators[i];
            }

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            paramCount.configure(is, os);

            size_t count = static_cast<size_t>(paramCount);
            generators.resize(count);
            for (size_t i = 0; i < count; ++i)
            {
                cl3ds::configure(generators[i], is, os,
                                 string(), makePtr<DummyGenerator>());
            }

            return true;
        }

        size_t hash() const
        {

            Hasher h;
            size_t count = generators.size();
            for (size_t i = 0; i < count; ++i)
            {
                h << generators[i]->hash();
            }

            return h;
        }

        size_t count() const
        {
            size_t totalCount = 0;
            size_t count      = generators.size();
            for (size_t i = 0; i < count; ++i)
            {
                totalCount += generators[i]->count();
            }

            return totalCount;
        }

        void reset()
        {
            currentGenIndex = 0;
            size_t count = generators.size();
            for (size_t i = 0; i < count; ++i)
            {
                generators[i]->reset();
            }
        }

        void next()
        {
            generators[currentGenIndex]->next();
            if (!generators[currentGenIndex]->hasNext())
            {
                currentGenIndex++;
            }
        }

        Mat get()
        {
            return generators[currentGenIndex]->get(false);
        }

        cv::Size size() const
        {
            if (generators[currentGenIndex].empty())
            {
                logError() << "No generators to combine";
            }

            return generators[currentGenIndex]->size();
        }

        int type() const
        {
            if (generators[currentGenIndex].empty())
            {
                logError() << "No generators to combine";
            }

            return generators[currentGenIndex]->type();
        }

        mutable TypeParameter<int> paramCount;
        size_t                     currentGenIndex;
        vector< Ptr<Generator> >   generators;

        CL3DS_PUB_IMPL(Combiner)
    };

    Combiner::Private::~Private()
    { }

    Combiner::Combiner() :
        Generator(new Private(this))
    { }

    Combiner::~Combiner()
    { }

    size_t Combiner::numGenerators() const
    {
        CL3DS_PRIV_CONST_PTR(Combiner);

        return priv->generators.size();
    }

    Generator * Combiner::generator(size_t index)
    {
        CL3DS_PRIV_PTR(Combiner);

        if (index >= priv->generators.size())
        {
            logError() << "Invalid index :" << index << ">=" << priv->generators.size();
        }

        return priv->generators[index];

    }

    void Combiner::setGenerator(size_t             index,
                                cv::Ptr<Generator> generator)
    {
        CL3DS_PRIV_PTR(Combiner);
        if (index >= priv->generators.size())
        {
            logError() << "Invalid index :" << index << ">=" << priv->generators.size();
        }

        priv->generators[index] = generator;
    }

    void Combiner::removeGenerator(size_t index)
    {
        CL3DS_PRIV_PTR(Combiner);
        if (index >= priv->generators.size())
        {
            logError() << "Invalid index :" << index << ">=" << priv->generators.size();
        }

        priv->removeGenerator(index);
    }

    void Combiner::removeAllGenerators()
    {
        CL3DS_PRIV_PTR(Combiner);

        priv->removeAllGenerators();
    }

    void Combiner::addGenerator(cv::Ptr<Generator> generator)
    {
        CL3DS_PRIV_PTR(Combiner);

        priv->addGenerator(generator);
    }
}
