/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENERATOR_P_H
#define GENERATOR_P_H

#include "../core/object_p.h"
#include "generator.h"

namespace cl3ds
{

    /** Base class for an opaque implementation of a Generator. */
    struct CL3DS_DECLSPEC Generator::AbstractPrivate : public Object::AbstractPrivate
    {
        AbstractPrivate(Generator *generator);
        ~AbstractPrivate();

        virtual void reset()          = 0;
        virtual void next()           = 0;
        virtual cv::Mat get()         = 0;
        virtual size_t count() const  = 0;
        virtual cv::Size size() const = 0;
        virtual int type() const      = 0;
    };
}

#endif // GENERATOR_P_H

