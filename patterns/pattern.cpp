/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../core/parameter.h"
#include "generator_p.h"
#include "pattern_p.h"

using namespace std;
using namespace cv;

namespace cl3ds
{

#define CONST_PRIV_PTR const Private * priv = \
    static_cast<const Private *>(Generator::privateImplementation());
#define PRIV_PTR Private * priv = \
    static_cast<Private *>(Generator::privateImplementation());

    struct Pattern::Private : public Generator::AbstractPrivate
    {
        Private(Pattern                  *p,
                Pattern::AbstractPrivate *i,
                Size                      s) :
            Generator::AbstractPrivate(p),
            impl(i)
        {
            paramSize = TypeParameter<Size>(
                "size",
                "Size of the pattern to generate",
                Pattern::defaultSize(),
                makePtr< BoundConstraint<Size> >(Size(0, 0), Size(10000, 10000)));
            setSize(s);
        }
        ~Private();

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Pattern constructor";
            }
        }

        bool read(const FileNode &node)
        {
            paramSize.read(node);
            setSize(paramSize);

            checkImplPtr();

            return impl->read(node);
        }

        bool write(cv::FileStorage &fs) const
        {
            paramSize.write(fs);

            checkImplPtr();

            return impl->write(fs);
        }

        bool configure(std::istream *is,
                       std::ostream *os)
        {
            paramSize.configure(is, os);
            setSize(paramSize);

            checkImplPtr();

            return impl->configure(is, os);
        }

        size_t hash() const
        {
            checkImplPtr();

            return Hasher() << paramSize << impl->hash();
        }

        void reset()
        {
            // nothing to do ...
        }

        void next()
        {
            // nothing to do ...
        }

        cv::Mat get()
        {
            checkImplPtr();

            return impl->get();
        }

        size_t count() const
        {
            return 1;
        }

        void setSize(Size s)
        {
            paramSize = s;
        }

        Size size() const
        {
            return paramSize;
        }

        int type() const
        {
            checkImplPtr();

            return impl->type();
        }

        TypeParameter<Size>       paramSize;
        Pattern::AbstractPrivate *impl;
    };

    Pattern::Private::~Private()
    {
        delete impl;
    }

    Pattern::~Pattern()
    { }

    Pattern::Pattern(AbstractPrivate *ptr,
                     Size             size) :
        Generator(new Private(this, ptr, size))
    { }

    Size Pattern::size() const
    {
        CONST_PRIV_PTR;

        return priv->size();
    }

    void Pattern::setSize(Size size)
    {
        PRIV_PTR;

        priv->setSize(size);
    }

    Size Pattern::defaultSize()
    {
        return Size(800, 600);
    }

    void Pattern::setPrivateImplementation(AbstractPrivate *pimpl)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = pimpl;
    }

    Pattern::AbstractPrivate * Pattern::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const Pattern::AbstractPrivate * Pattern::privateImplementation() const
    {
        CONST_PRIV_PTR;

        return priv->impl;
    }

    Pattern::AbstractPrivate::AbstractPrivate(Pattern *filter) :
        Object::AbstractPrivate(filter)
    { }

    Pattern::AbstractPrivate::~AbstractPrivate()
    { }
}
