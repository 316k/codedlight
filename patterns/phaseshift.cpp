/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <mvg/mvg.h>
#include <mvg/concurrency.h>
#include "../core/parameter.h"
#include "generator_p.h"
#include "matcher_p.h"
#include "phaseshift.h"
#include "seq_operations.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    typedef PhaseShiftGenerator::Generation Generation;
    typedef PhaseShiftGenerator::Direction  Direction;
    typedef PhaseShiftMatcher::Unwrapping   Unwrapping;

    struct GenerationMethodWrapper : public Object
    {
        struct Private : public AbstractPrivate
        {
            Private(GenerationMethodWrapper *ptr) : AbstractPrivate(ptr) { }
            ~Private();

            virtual Mat get(const PhaseShiftGenerator *gen,
                            size_t                     index) const = 0;
        };

        GenerationMethodWrapper(Private *ptr) : Object(ptr) { }
        ~GenerationMethodWrapper();

        Mat get(const PhaseShiftGenerator *gen,
                size_t                     index) const
        {
            CL3DS_PRIV_CONST_PTR(GenerationMethodWrapper);

            return priv->get(gen, index);
        }

        static Ptr< Factory<string, GenerationMethodWrapper> > makeFactory();
        static Factory<string, GenerationMethodWrapper> * classFactory()
        {
            static Ptr< Factory<string, GenerationMethodWrapper> > factory =
                makeFactory();

            return factory;
        }

        CL3DS_PRIV_PTR_GETTERS
    };

    struct DefaultGenerationMethodWrapper : public GenerationMethodWrapper
    {
        struct Private : public GenerationMethodWrapper::Private
        {
            Private(GenerationMethodWrapper *ptr) : GenerationMethodWrapper::Private(ptr)
            { }
            ~Private();

            bool read(const FileNode &) { return true; }
            bool write(FileStorage &) const { return true; }
            bool configure(istream *,
                           ostream *) { return true; }
            size_t hash() const { return 0; }

            Mat get(const PhaseShiftGenerator *gen,
                    size_t                     index) const;
        };

        DefaultGenerationMethodWrapper() : GenerationMethodWrapper(new Private(this))
        { }
        ~DefaultGenerationMethodWrapper();

        CL3DS_PRIV_PTR_GETTERS
        CL3DS_CLASS_NAME("default")
    };

    struct BlockModulationGenerationMethodWrapper : public GenerationMethodWrapper
    {
        struct Private : public GenerationMethodWrapper::Private
        {
            Private(GenerationMethodWrapper *ptr,
                    double                   modPeriod) :
                GenerationMethodWrapper::Private(ptr)
            {
                paramModPeriod = TypeParameter<double>("modPeriod",
                                                       "Modulation period",
                                                       Generation::BlockModulation::defaultModPeriod());
                paramModPeriod = modPeriod;
            }
            ~Private();

            Mat get(const PhaseShiftGenerator *gen,
                    size_t                     index) const;

            /* *INDENT-OFF* */
            bool read(const FileNode &fn) { paramModPeriod.read(fn); return true; }
            bool write(FileStorage &fs) const { paramModPeriod.write(fs); return true; }
            bool configure(std::istream *is, std::ostream *os) {
                if (os) {
                    *os << "Configuring Phase Shift generator, parameters " <<
                                 "for the block modulation generation type." << endl;
                }
                paramModPeriod.configure(is, os);
                return true;
            }
            size_t hash() const {
                return Hasher() << paramModPeriod;
            }
            /* *INDENT-ON* */

            TypeParameter<double> paramModPeriod;
        };

        BlockModulationGenerationMethodWrapper(
            double modPeriod=Generation::BlockModulation::defaultModPeriod()) :
            GenerationMethodWrapper(new Private(this, modPeriod)) { }
        ~BlockModulationGenerationMethodWrapper();

        CL3DS_PRIV_PTR_GETTERS
        CL3DS_CLASS_NAME("block")
    };

    struct SinusoidalModulationGenerationMethodWrapper : public GenerationMethodWrapper
    {
        struct Private : public GenerationMethodWrapper::Private
        {
            Private(GenerationMethodWrapper *ptr,
                    double                   modPeriod) :
                GenerationMethodWrapper::Private(ptr)
            {
                paramModPeriod = TypeParameter<double>("modPeriod",
                                                       "Modulation period",
                                                       Generation::SinusoidalModulation::defaultModPeriod());
                paramModPeriod = modPeriod;
            }
            ~Private();

            Mat get(const PhaseShiftGenerator *gen,
                    size_t                     index) const;

            /* *INDENT-OFF* */
            bool read(const FileNode &fn) { paramModPeriod.read(fn); return true; }
            bool write(FileStorage &fs) const { paramModPeriod.write(fs); return true; }
            bool configure(std::istream *is, std::ostream *os) {
                if (os) {
                    *os << "Configuring Phase Shift generator, parameters " <<
                                 "for the sinusoidal modulation generation type." << endl;
                }
                paramModPeriod.configure(is, os);
                return true;
            }
            size_t hash() const {
                return Hasher() << paramModPeriod;
            }
            /* *INDENT-ON* */

            TypeParameter<double> paramModPeriod;
        };

        SinusoidalModulationGenerationMethodWrapper(
            double modPeriod=Generation::SinusoidalModulation::defaultModPeriod()) :
            GenerationMethodWrapper(new Private(this, modPeriod)) { }
        ~SinusoidalModulationGenerationMethodWrapper();

        CL3DS_PRIV_PTR_GETTERS
        CL3DS_CLASS_NAME("sinusoidal")
    };

    Ptr< Factory<string, GenerationMethodWrapper> > GenerationMethodWrapper::makeFactory()
    {
        Ptr< Factory<string, GenerationMethodWrapper> > f =
            makePtr< Factory<string, GenerationMethodWrapper> >();
        f->reg<DefaultGenerationMethodWrapper>();
        f->reg<BlockModulationGenerationMethodWrapper>();
        f->reg<SinusoidalModulationGenerationMethodWrapper>();

        return f;
    }

    GenerationMethodWrapper::~GenerationMethodWrapper() { }
    GenerationMethodWrapper::Private::~Private() { }

    static Ptr<GenerationMethodWrapper> makeGenerationMethodWrapper(
        const Generation::Method &method)
    {
        switch (method.type)
        {
            case Generation::Method::Default:
            {
                return makePtr<DefaultGenerationMethodWrapper>();
            }
            case Generation::Method::Block:
            {
                const Generation::BlockModulation &type =
                    static_cast<const Generation::BlockModulation &>(method);

                return makePtr<BlockModulationGenerationMethodWrapper>(type.modPeriod);
            }
            case Generation::Method::Sinusoidal:
            {
                const Generation::SinusoidalModulation &type =
                    static_cast<const Generation::SinusoidalModulation &>(method);

                return makePtr<SinusoidalModulationGenerationMethodWrapper>(
                    type.modPeriod);
            }
        }

        return Ptr<GenerationMethodWrapper>();
    }

    static Ptr<Generation::Method> makeGenerationMethod(
        const Ptr<GenerationMethodWrapper> &type)
    {
        const GenerationMethodWrapper *ptr =
            static_cast<const GenerationMethodWrapper *>(type);
        if (dynamic_cast<const DefaultGenerationMethodWrapper *>(ptr))
        {
            return makePtr<Generation::Default>();
        }
        else if (const BlockModulationGenerationMethodWrapper *bwrapper =
                     dynamic_cast<const BlockModulationGenerationMethodWrapper *>(ptr))
        {
            return makePtr<Generation::BlockModulation>(
                bwrapper->privPtr()->paramModPeriod);
        }
        else if (const SinusoidalModulationGenerationMethodWrapper *swrapper =
                     dynamic_cast<const SinusoidalModulationGenerationMethodWrapper *>(ptr))
        {
            return makePtr<Generation::SinusoidalModulation>(
                swrapper->privPtr()->paramModPeriod);
        }

        return Ptr<Generation::Method>();
    }

    // PhaseShiftGenerator PIMPL
    struct PhaseShiftGenerator::Private : public AbstractPrivate
    {
        Private(PhaseShiftGenerator      *ptr,
                Size                      size,
                Direction                 dir,
                const vector<int>        &numShifts,
                const vector<double>     &periods,
                const Generation::Method &type);

        bool hasNext() const;
        void next();
        void reset();
        Mat get();
        size_t count() const;

        bool read(const FileNode &node);
        bool write(FileStorage &fs) const;
        bool configure(istream *is,
                       ostream *os);
        size_t hash() const;

        Size size() const;
        int type() const;

        TypeParameter<Size>             paramSize;
        TypeParameter<int>              paramDirection;
        TypeParameter< vector<int> >    paramNumShifts;
        TypeParameter< vector<double> > paramPeriods;
        Ptr<GenerationMethodWrapper>    generationMethodWrapper;

        CL3DS_PUB_IMPL(PhaseShiftGenerator)
    };

    DefaultGenerationMethodWrapper::~DefaultGenerationMethodWrapper() { }
    DefaultGenerationMethodWrapper::Private::~Private() { }

    Mat DefaultGenerationMethodWrapper::Private::get(const PhaseShiftGenerator *gen,
                                                     size_t                     index)
    const
    {
        Mat pattern;

        size_t nbFreqs = gen->numShifts().size();
        int    nbPatts = 0;
        for (size_t i = 0; i < nbFreqs; ++i)
        {
            nbPatts += gen->numShifts()[i];
        }

        size_t ifreq  = 0;
        size_t ishift = 0;
        for (size_t i = 0; i < index % static_cast<size_t>(nbPatts); ++i)
        {
            if (ishift + 1 == static_cast<size_t>(gen->numShifts()[ifreq]))
            {
                ishift = 0;
                ifreq++;
            }
            else { ishift++; }
        }

        const double phaseShift = 2 * CV_PI / gen->numShifts()[ifreq];
        const double shift      = ishift * phaseShift;
        const double ampl       = 127.5;
        const double mean       = 127.5;
        const double freq       = 1 / gen->periods()[ifreq];
        const double scale      = freq * 2 * CV_PI;

        bool doingHorizontal = (gen->direction() == Generator::HORIZONTAL ||
                                (gen->direction() == Generator::BOTH && index <
                                 static_cast<size_t>(nbPatts)));

        Size s = Size(doingHorizontal ? gen->size().height : gen->size().width,
                      doingHorizontal ? gen->size().width : gen->size().height);

        pattern.create(s, CV_8U);

        MatIterator_<uchar> it = pattern.begin<uchar>();
        for (int j = 0; j < pattern.rows; ++j)
        {
            uchar val = saturate_cast<uchar>(mean + ampl * cos(scale * j - shift));
            fill(it, it + pattern.cols, val);
            it += pattern.cols;
        }

        if (doingHorizontal) { pattern = pattern.t(); }

        return pattern;
    }

    BlockModulationGenerationMethodWrapper::~BlockModulationGenerationMethodWrapper() { }
    BlockModulationGenerationMethodWrapper::Private::~Private() { }

    Mat BlockModulationGenerationMethodWrapper::Private::get(
        const PhaseShiftGenerator *gen,
        size_t                     index)
    const
    {
        Mat pattern;

        size_t nbFreqs = gen->numShifts().size();
        int    nbPatts = 0;
        for (size_t i = 0; i < nbFreqs; ++i)
        {
            nbPatts += gen->numShifts()[i];
        }

        size_t ifreq  = 0;
        size_t ishift = 0;
        for (size_t i = 0; i < index % static_cast<size_t>(nbPatts); ++i)
        {
            if (ishift + 1 == static_cast<size_t>(gen->numShifts()[ifreq]))
            {
                ishift = 0;
                ifreq++;
            }
            else { ishift++; }
        }

        const double phaseShift = 2 * CV_PI / gen->numShifts()[ifreq];
        const double shift      = ishift * phaseShift;
        const double ampl       = 127.5;
        const double mean       = 127.5;
        double       period     = gen->periods()[ifreq];
        const double freq       = 1 / period;
        const double scale      = freq * 2 * CV_PI;

        bool doingHorizontal = (gen->direction() == Generator::HORIZONTAL ||
                                (gen->direction() == Generator::BOTH && index <
                                 static_cast<size_t>(nbPatts)));

        Size s = Size(doingHorizontal ? gen->size().height : gen->size().width,
                      doingHorizontal ? gen->size().width : gen->size().height);

        pattern.create(s, CV_8U);

        int modPeriod  = static_cast<int>(paramModPeriod);
        int halfPeriod = modPeriod / 2;
        for (int j = 0; j < pattern.rows; j++)
        {
            uchar *ptr = pattern.ptr<uchar>(j);
            for (int i = 0; i < pattern.cols; i += modPeriod)
            {
                uchar val = saturate_cast<uchar>(mean + ampl * cos(scale * (j) - shift));
                for (int k = 0; k < halfPeriod; ++k)
                {
                    ptr[i + k] = val;
                }
            }
            for (int i = halfPeriod; i < pattern.cols; i += modPeriod)
            {
                uchar val =
                    saturate_cast<uchar>(mean + ampl * cos(scale * (j) - shift + CV_PI));
                for (int k = 0; k < halfPeriod; ++k)
                {
                    ptr[i + k] = val;
                }
            }
        }

        if (doingHorizontal) { pattern = pattern.t(); }

        return pattern;
    }

    SinusoidalModulationGenerationMethodWrapper::~
    SinusoidalModulationGenerationMethodWrapper()
    { }
    SinusoidalModulationGenerationMethodWrapper::Private::~Private() { }

    Mat SinusoidalModulationGenerationMethodWrapper::Private::get(
        const PhaseShiftGenerator *gen,
        size_t                     index)
    const
    {
        Mat pattern;

        size_t nbFreqs = gen->numShifts().size();
        int    nbPatts = 0;
        for (size_t i = 0; i < nbFreqs; ++i)
        {
            nbPatts += gen->numShifts()[i];
        }

        size_t ifreq  = 0;
        size_t ishift = 0;
        for (size_t i = 0; i < index % static_cast<size_t>(nbPatts); ++i)
        {
            if (ishift + 1 == static_cast<size_t>(gen->numShifts()[ifreq]))
            {
                ishift = 0;
                ifreq++;
            }
            else { ishift++; }
        }

        const double phaseShift = 2 * CV_PI / gen->numShifts()[ifreq];
        const double shift      = ishift * phaseShift;
        const double ampl       = 127.5;
        const double mean       = 127.5;
        const double freq       = 1 / gen->periods()[ifreq];
        const double scale      = freq * 2 * CV_PI;
        const double modscale   = 2 * CV_PI / paramModPeriod;

        bool doingHorizontal = (gen->direction() == Generator::HORIZONTAL ||
                                (gen->direction() == Generator::BOTH && index <
                                 static_cast<size_t>(nbPatts)));

        Size s = Size(doingHorizontal ? gen->size().height : gen->size().width,
                      doingHorizontal ? gen->size().width : gen->size().height);

        pattern.create(s, CV_8U);

        for (int j = 0; j < pattern.rows; ++j)
        {
            uchar *ptr = pattern.ptr<uchar>(j);
            for (int i = 0; i < pattern.cols; i++)
            {
                uchar val =
                    saturate_cast<uchar>(mean + ampl *
                                         cos(scale * j + cos(modscale * i) - shift));
                ptr[i] = val;
            }
        }

        if (doingHorizontal) { pattern = pattern.t(); }

        return pattern;
    }

    PhaseShiftGenerator::Private::Private(PhaseShiftGenerator      *ptr,
                                          Size                      size,
                                          Direction                 dir,
                                          const vector<int>        &numShifts,
                                          const vector<double>     &periods,
                                          const Generation::Method &type) :
        AbstractPrivate(ptr),
        generationMethodWrapper(makeGenerationMethodWrapper(type))
    {
        paramSize =
            TypeParameter<Size>(
                "size",
                "Size of the pattern to generate",
                PhaseShiftGenerator::defaultSize(),
                makePtr< BoundConstraint<Size> >(Size(0, 0), Size(10000, 10000)));
        paramSize = size;

        vector<int> valuesDirection;
        valuesDirection.push_back(Generator::HORIZONTAL);
        valuesDirection.push_back(Generator::VERTICAL);
        valuesDirection.push_back(Generator::BOTH);
        paramDirection =
            TypeParameter<int>("direction",
                               "Direction for the pattern",
                               PhaseShiftGenerator::defaultDirection(),
                               makePtr< EnumerationConstraint<int> >(valuesDirection));
        paramDirection = dir;

        paramNumShifts =
            TypeParameter< vector<int> >(
                "numShifts",
                "Number of shifts each sine wave is shifted",
                PhaseShiftGenerator::defaultNumShifts(),
                makePtr< BoundConstraint< vector<int> > >(3, numeric_limits<int>::max()));
        paramNumShifts = numShifts;

        paramPeriods = TypeParameter< vector<double> >("periods",
                                                       "Periods of each sine wave",
                                                       PhaseShiftGenerator::defaultPeriods());
        paramPeriods = periods;
    }

    void PhaseShiftGenerator::Private::next()
    { }

    void PhaseShiftGenerator::Private::reset()
    { }

    Mat PhaseShiftGenerator::Private::get()
    {
        return generationMethodWrapper->get(pubPtr(), pubPtr()->index());
    }

    size_t PhaseShiftGenerator::Private::count() const
    {
        size_t             size     = 0;
        const vector<int> &nbShifts = paramNumShifts;
        size_t             nbFreqs  = nbShifts.size();

        for (size_t i = 0; i < nbFreqs; ++i)
        {
            size += static_cast<size_t>(nbShifts[i]);
        }

        if (paramDirection == Generator::BOTH)
        {
            size *= 2;
        }

        return size;
    }

    bool PhaseShiftGenerator::Private::read(const FileNode &node)
    {
        paramSize.read(node);
        paramDirection.read(node);
        paramNumShifts.read(node);
        paramPeriods.read(node);

        // the global read function will allocate the pointer for us
        node["generationMethod"] >> generationMethodWrapper;

        return true;
    }

    bool PhaseShiftGenerator::Private::write(FileStorage &fs) const
    {
        paramSize.write(fs);
        paramDirection.write(fs);
        paramNumShifts.write(fs);
        paramPeriods.write(fs);

        // the global write function will properly serialize polymorphic base pointer
        fs << "generationMethod" << generationMethodWrapper;

        return true;
    }

    bool PhaseShiftGenerator::Private::configure(istream *is,
                                                 ostream *os)
    {
        if (os) { *os << "Configuring Phase Shift generator parameters" << endl; }
        paramSize.configure(is, os);
        paramDirection.configure(is, os);
        paramNumShifts.configure(is, os);
        paramPeriods.configure(is, os);

        if (os) { *os << "Which Phase Shift generation type should be used ?" << endl; }
        // the global configure function will allocate the pointer for us
        cl3ds::configure(generationMethodWrapper, is, os,
                         string(),
                         makePtr<DefaultGenerationMethodWrapper>(), false);

        return true;
    }

    size_t PhaseShiftGenerator::Private::hash() const
    {
        return Hasher() << paramSize
                        << paramDirection
                        << paramNumShifts
                        << paramPeriods
                        << generationMethodWrapper->objectName();

    }

    Size PhaseShiftGenerator::Private::size() const
    {
        return paramSize;
    }

    int PhaseShiftGenerator::Private::type() const
    {
        return CV_8U;
    }

    PhaseShiftGenerator::PhaseShiftGenerator(Size                      size,
                                             Direction                 dir,
                                             const vector<int>        &numShifts,
                                             const vector<double>     &periods,
                                             const Generation::Method &generationMethod) :
        Generator(new Private(this, size, dir, numShifts, periods, generationMethod))
    { }

    PhaseShiftGenerator::~PhaseShiftGenerator() { }

    Generation::Default::Default()
    {
        type = Generation::Method::Default;
    }

    Generation::BlockModulation::BlockModulation(double m) :
        modPeriod(m)
    {
        type = Generation::Method::Block;
    }

    double Generation::BlockModulation::defaultModPeriod()
    {
        return 16;
    }

    Generation::SinusoidalModulation::SinusoidalModulation(double m) :
        modPeriod(m)
    {
        type = Generation::Method::Sinusoidal;
    }

    double Generation::SinusoidalModulation::defaultModPeriod()
    {
        return 16;
    }

    Direction PhaseShiftGenerator::direction() const
    {
        CL3DS_PRIV_CONST_PTR(PhaseShiftGenerator);

        return static_cast<Direction>(int(priv->paramDirection));
    }

    void PhaseShiftGenerator::setDirection(const Direction &dir)
    {
        CL3DS_PRIV_PTR(PhaseShiftGenerator);

        priv->paramDirection = dir;
    }

    Direction PhaseShiftGenerator::defaultDirection()
    {
        return HORIZONTAL;
    }

    Size PhaseShiftGenerator::size() const
    {
        CL3DS_PRIV_CONST_PTR(PhaseShiftGenerator);

        return priv->size();
    }

    void PhaseShiftGenerator::setSize(Size size)
    {
        CL3DS_PRIV_PTR(PhaseShiftGenerator);

        priv->paramSize = size;
    }

    Size PhaseShiftGenerator::defaultSize()
    {
        return Size(800, 600);
    }

    const vector<double> &PhaseShiftGenerator::periods() const
    {
        CL3DS_PRIV_CONST_PTR(PhaseShiftGenerator);

        return priv->paramPeriods;
    }

    void PhaseShiftGenerator::setPeriods(const vector<double> &periods)
    {
        CL3DS_PRIV_PTR(PhaseShiftGenerator);
        priv->paramPeriods = periods;
    }

    std::vector<double> PhaseShiftGenerator::defaultPeriods()
    {
        double periods[8];
        periods[0] = 16;
        for (int i = 1; i < 8; ++i)
        {
            periods[i] = periods[i - 1] * 2;
        }

        return makeVector(periods);
    }

    const vector<int> &PhaseShiftGenerator::numShifts() const
    {
        CL3DS_PRIV_CONST_PTR(PhaseShiftGenerator);

        return priv->paramNumShifts;
    }

    void PhaseShiftGenerator::setNumShifts(const vector<int> &numShifts)
    {
        CL3DS_PRIV_PTR(PhaseShiftGenerator);

        priv->paramNumShifts = numShifts;
    }

    std::vector<int> PhaseShiftGenerator::defaultNumShifts()
    {
        int numShifts[8];
        for (int i = 1; i < 8; ++i)
        {
            numShifts[i] = 3;
        }
        numShifts[0] = 10;

        return makeVector(numShifts);
    }

    const PhaseShiftGenerator::Generation::Method & PhaseShiftGenerator::generationMethod()
    const
    {
        CL3DS_PRIV_CONST_PTR(PhaseShiftGenerator);

        static Ptr<Generation::Method> genMethod;
        genMethod = makeGenerationMethod(priv->generationMethodWrapper);

        return *genMethod;
    }

    void PhaseShiftGenerator::setGenerationMethod(
        const PhaseShiftGenerator::Generation::Method &genMethod)
    {
        CL3DS_PRIV_PTR(PhaseShiftGenerator);

        Ptr<GenerationMethodWrapper> genMethodWrapper =
            makeGenerationMethodWrapper(genMethod);
        priv->generationMethodWrapper = genMethodWrapper;
    }

    Generation::Default PhaseShiftGenerator::defaultGenerationMethod()
    {
        return Generation::Default();
    }

    struct PhaseShiftMatcherData : public MatcherData
    {
        PhaseShiftMatcherData(PhaseShiftMatcher               *m,
                              Generator *const                 camGenerator,
                              Generator *const                 projGenerator,
                              const cv::Mat                   &camMask,
                              const cv::Mat                   &projMask,
                              const CameraGeometricParameters &camParameters=CameraGeometricParameters(),
                              const CameraGeometricParameters &projParameters=CameraGeometricParameters(),
                              bool                             useHorizontalPatterns=
                                  true) :
            MatcherData(camGenerator, projGenerator,
                        camMask, projMask,
                        camParameters, projParameters,
                        useHorizontalPatterns),
            matcher(m) { }
        PhaseShiftMatcher *matcher;
    };

    static vector<double> makeCosVals(size_t nbShifts)
    {
        vector<double> vals(nbShifts);
        for (size_t i = 0; i < nbShifts; ++i)
        {
            vals[i] = cos((CV_PI * 2 * i) / nbShifts);
        }

        return vals;
    }

    static vector<double> makeSinVals(size_t nbShifts)
    {
        vector<double> vals(nbShifts);
        for (size_t i = 0; i < nbShifts; ++i)
        {
            vals[i] = sin((CV_PI * 2 * i) / nbShifts);
        }

        return vals;
    }

    template <typename T>
    inline double phaseFromIntensities(const T              *ptr,
                                       size_t                nbVals,
                                       const vector<double> &cosVals,
                                       const vector<double> &sinVals)
    {
        double sumCos = 0, sumSin = 0;
        for (size_t i = 0; i < nbVals; ++i)
        {
            sumCos += cosVals[i] * ptr[i];
            sumSin += sinVals[i] * ptr[i];
        }
        double phase = atan2(sumSin, sumCos);

        return phase < 0 ? phase + 2 * CV_PI : phase;
    }

    struct PhaseShiftData
    {
        PhaseShiftData(PhaseShiftGenerator *gen) :
            numShifts(gen->numShifts()),
            periods(gen->periods())
        {
            numFreqs = numShifts.size();

            cumShifts.resize(numFreqs);
            cumShifts[0] = 0;
            for (size_t i = 1; i < numFreqs; ++i)
            {
                cumShifts[i] = numShifts[i - 1] + cumShifts[i - 1];
            }

            numPatts = 0;
            for (size_t i = 0; i < numFreqs; ++i)
            {
                numPatts += numShifts[i];
            }

            cosVals.resize(numFreqs);
            sinVals.resize(numFreqs);
            for (size_t i = 0; i < numFreqs; ++i)
            {
                cosVals[i] = makeCosVals(static_cast<size_t>(numShifts[i]));
                sinVals[i] = makeSinVals(static_cast<size_t>(numShifts[i]));
            }
        }

        const vector<int>       &numShifts;
        const vector<double>    &periods;
        size_t                   numFreqs;
        vector< vector<double> > cosVals;
        vector< vector<double> > sinVals;
        vector<int>              cumShifts;
        int                      numPatts;
    };

    struct UnwrappingMethodWrapper : public Object
    {
        struct Private : public AbstractPrivate
        {
            Private(UnwrappingMethodWrapper *ptr) : AbstractPrivate(ptr) { }
            ~Private();

            virtual MatTable matchWithEpipolarGeometry(const MatcherData &)    = 0;
            virtual MatTable matchWithoutEpipolarGeometry(const MatcherData &) = 0;
        };

        UnwrappingMethodWrapper(Private *ptr) : Object(ptr) { }
        ~UnwrappingMethodWrapper();

        MatTable matchWithEpipolarGeometry(const MatcherData &data)
        {
            CL3DS_PRIV_PTR(UnwrappingMethodWrapper);

            return priv->matchWithEpipolarGeometry(data);
        }

        MatTable matchWithoutEpipolarGeometry(const MatcherData &data)
        {
            CL3DS_PRIV_PTR(UnwrappingMethodWrapper);

            return priv->matchWithoutEpipolarGeometry(data);
        }

        static Ptr< Factory<string, UnwrappingMethodWrapper> > makeFactory();

        static Factory<string, UnwrappingMethodWrapper> * classFactory()
        {
            static Ptr< Factory<string, UnwrappingMethodWrapper> > factory =
                makeFactory();

            return factory;
        }

        CL3DS_PRIV_PTR_GETTERS
    };

    struct AllFrequenciesUnwrappingMethodWrapper : public UnwrappingMethodWrapper
    {
        struct Private : public UnwrappingMethodWrapper::Private
        {
            Private(UnwrappingMethodWrapper *ptr) : UnwrappingMethodWrapper::Private(ptr)
            { }
            ~Private();

            bool read(const FileNode &) { return true; }
            bool write(FileStorage &) const { return true; }
            bool configure(istream *,
                           ostream *) { return true; }
            size_t hash() const { return 0; }

            template <class T> MatTable matchWithEpipolarGeometry(const MatcherData &);
            MatTable matchWithEpipolarGeometry(const MatcherData &data)
            {
                return dispatchWithGeometry(this, data, data.camGenerator->type());
            }

            template <class T> MatTable matchWithoutEpipolarGeometry(const MatcherData &);
            virtual MatTable matchWithoutEpipolarGeometry(const MatcherData &data)
            {
                return dispatchWithoutGeometry(this, data, data.camGenerator->type());
            }

            Mat hDataT, vDataT;
        };

        AllFrequenciesUnwrappingMethodWrapper() :
            UnwrappingMethodWrapper(new Private(this)) { }
        ~AllFrequenciesUnwrappingMethodWrapper();

        CL3DS_PRIV_PTR_GETTERS
        CL3DS_CLASS_NAME("allFrequencies")
    };

    struct ExternalMapUnwrappingMethodWrapper : public UnwrappingMethodWrapper
    {
        struct Private : public UnwrappingMethodWrapper::Private
        {
            Private(UnwrappingMethodWrapper *ptr,
                    const string            &path,
                    int                      phaseFreqId) :
                UnwrappingMethodWrapper::Private(ptr)
            {
                paramPath = TypeParameter<string>("path",
                                                  "Path to the external unwrapped map",
                                                  PhaseShiftMatcher::Unwrapping::ExternalMap::defaultPath());
                paramPath = path;

                paramPhaseFreqId = TypeParameter<int>("phaseFreqId",
                                                      "Which frequency will be used to compute the phase",
                                                      PhaseShiftMatcher::Unwrapping::ExternalMap::defaultPhaseFrequencyId());
                paramPhaseFreqId = phaseFreqId;
            }
            ~Private();

            bool read(const FileNode &fn)
            {
                paramPath.read(fn);
                paramPhaseFreqId.read(fn);

                return true;
            }

            bool write(FileStorage &fs) const
            {
                paramPath.write(fs);
                paramPhaseFreqId.write(fs);

                return true;
            }

            bool configure(std::istream *is,
                           std::ostream *os)
            {
                if (os)
                {
                    *os << "Configuring Phase Shift matcher, parameters " <<
                        "for the unwrapping type with an external map." << endl;
                }
                paramPath.configure(is, os);
                paramPhaseFreqId.configure(is, os);

                return true;
            }

            size_t hash() const
            {
                return Hasher() << paramPath
                                << paramPhaseFreqId;
            }

            template <class T> MatTable matchWithEpipolarGeometry(const MatcherData &);
            virtual MatTable matchWithEpipolarGeometry(const MatcherData &data)
            {
                return dispatchWithGeometry(this, data, data.camGenerator->type());
            }

            template <class T> MatTable matchWithoutEpipolarGeometry(const MatcherData &);
            virtual MatTable matchWithoutEpipolarGeometry(const MatcherData &data)
            {
                return dispatchWithoutGeometry(this, data, data.camGenerator->type());
            }

            TypeParameter<string> paramPath;
            TypeParameter<int>    paramPhaseFreqId;

            Mat hDataT, vDataT;
            Mat unwrapMap;
        };

        ExternalMapUnwrappingMethodWrapper
            (const string &path=Unwrapping::ExternalMap::defaultPath(),
            int            phaseFreqId=Unwrapping::ExternalMap::
                                        defaultPhaseFrequencyId()) :
            UnwrappingMethodWrapper(new Private(this, path, phaseFreqId)) { }
        ~ExternalMapUnwrappingMethodWrapper();

        CL3DS_PRIV_PTR_GETTERS
        CL3DS_CLASS_NAME("externalMap")
    };

    static Ptr<UnwrappingMethodWrapper> makeUnwrappingMethodWrapper(
        const Unwrapping::Method &method)
    {
        switch (method.type)
        {
            case Unwrapping::Method::AllFrequencies:
            {
                return makePtr<AllFrequenciesUnwrappingMethodWrapper>();
            }
            case Unwrapping::Method::ExternalMap:
            {
                const Unwrapping::ExternalMap &type =
                    static_cast<const Unwrapping::ExternalMap &>(method);

                return makePtr<ExternalMapUnwrappingMethodWrapper>(type.path,
                                                                   type.phaseFrequencyId);
            }
        }

        return Ptr<UnwrappingMethodWrapper>();
    }

    static Ptr<Unwrapping::Method> makeUnwrappingMethod(
        const Ptr<UnwrappingMethodWrapper> &type)
    {
        const UnwrappingMethodWrapper *ptr =
            static_cast<const UnwrappingMethodWrapper *>(type);
        if (dynamic_cast<const AllFrequenciesUnwrappingMethodWrapper *>(ptr))
        {
            return makePtr<Unwrapping::AllFrequencies>();
        }
        else if (const ExternalMapUnwrappingMethodWrapper *ewrapper =
                     dynamic_cast<const ExternalMapUnwrappingMethodWrapper *>(ptr))
        {
            return makePtr<Unwrapping::ExternalMap>(ewrapper->privPtr()->paramPath,
                                                    ewrapper->privPtr()->paramPhaseFreqId);
        }

        return Ptr<Unwrapping::Method>();
    }

    Ptr< Factory<string, UnwrappingMethodWrapper> > UnwrappingMethodWrapper::makeFactory()
    {
        Ptr< Factory<string, UnwrappingMethodWrapper> > f =
            makePtr< Factory<string, UnwrappingMethodWrapper> >();
        f->reg<AllFrequenciesUnwrappingMethodWrapper>();
        f->reg<ExternalMapUnwrappingMethodWrapper>();

        return f;
    }

    UnwrappingMethodWrapper::~UnwrappingMethodWrapper() { }
    UnwrappingMethodWrapper::Private::~Private() { }

    struct PhaseShiftMatcher::Private : public AbstractPrivate
    {
        Private(PhaseShiftMatcher              *ptr,
                const Ptr<PhaseShiftGenerator> &gen,
                const Unwrapping::Method       &type);

        MatTable matchWithEpipolarGeometry(Generator *const                 camGenerator,
                                           Generator *const                 projGenerator,
                                           const Mat                       &camMask,
                                           const Mat                       &projMask,
                                           const CameraGeometricParameters &camParameters,
                                           const CameraGeometricParameters &projParameters,
                                           bool                             useHorizontalPatterns);
        MatTable matchWithoutEpipolarGeometry(Generator *const camGenerator,
                                              Generator *const projGenerator,
                                              const Mat       &camMask,
                                              const Mat       &projMask);

        bool read(const FileNode &node);
        bool write(FileStorage &fs) const;
        bool configure(istream *is,
                       ostream *os);
        size_t hash() const;

        Ptr<PhaseShiftGenerator>     gen;
        Ptr<UnwrappingMethodWrapper> unwrappingMethodWrapper;

        CL3DS_PUB_IMPL(PhaseShiftMatcher)
    };

    template <class T>
    struct AllFrequenciesUnwrapper : public ParallelForBody
    {
        AllFrequenciesUnwrapper(const Mat            &m,
                                Mat                  &c,
                                Size                  s,
                                const Mat            &i,
                                const PhaseShiftData &d) :
            mask(m),
            codes(c),
            camSize(s),
            intensities(i),
            data(d)
        { }

        virtual void operator()(size_t _begin,
                                size_t _end) const
        {
            int begin = static_cast<int>(_begin);
            int end   = static_cast<int>(_end);

            for (int j = begin; j < end; ++j)
            {
                const uchar *mptr = (mask.empty() ? 0 : mask.ptr<uchar>(j));
                float       *ptr  = codes.ptr<float>(j);

                for (int i = 0; i < camSize.width; ++i)
                {
                    if (mptr && !mptr[i]) { continue; }

                    int      camId = j * camSize.width + i;
                    const T *vptr  = intensities.ptr<T>(camId);

                    double p0 =
                        phaseFromIntensities(
                            vptr + data.cumShifts[data.numFreqs - 1],
                            static_cast<size_t>(data.numShifts[data.numFreqs - 1]),
                            data.cosVals[data.numFreqs - 1],
                            data.sinVals[data.numFreqs - 1]);

                    for (int k = static_cast<int>(data.numFreqs) - 2; k >= 0; --k)
                    {
                        double p =
                            phaseFromIntensities(
                                vptr + data.cumShifts[static_cast<size_t>(k)],
                                static_cast<size_t>(data.numShifts[static_cast<size_t>(k)]),
                                data.cosVals[static_cast<size_t>(k)],
                                data.sinVals[static_cast<size_t>(k)]);

                        double mult = data.periods[static_cast<size_t>(k) + 1]
                                      / data.periods[static_cast<size_t>(k)];
                        p0 = p - 2 * CV_PI * cvRound((p - mult * p0) / (2 * CV_PI));

                    }
                    ptr[i] = static_cast<float>(p0 * data.periods[0] / (2 * CV_PI));
                }
            }
        }

        const Mat            &mask;
        Mat                  &codes;
        Size                  camSize;
        const Mat            &intensities;
        const PhaseShiftData &data;
    };

    AllFrequenciesUnwrappingMethodWrapper::~AllFrequenciesUnwrappingMethodWrapper() { }
    AllFrequenciesUnwrappingMethodWrapper::Private::~Private() { }

    template <class T>
    MatTable AllFrequenciesUnwrappingMethodWrapper::Private::matchWithEpipolarGeometry(
        const MatcherData &matcherData_)
    {
        MatTable result;

        const PhaseShiftMatcherData &matcherData =
            static_cast<const PhaseShiftMatcherData &>(matcherData_);
        PhaseShiftGenerator *gen = matcherData.matcher->generator();
        PhaseShiftData       data(gen);

        Size camSize, projSize;
        camSize  = matcherData.camGenerator->size();
        projSize = gen->size();
        const Mat &mask = matcherData.camMask;

        bool useHorizontalPatterns = matcherData.useHorizontalPatterns;
        transposeImages<T>(matcherData.camGenerator, hDataT, data.numPatts, 0);

        Mat                        codes(camSize, CV_32F, Scalar::all(0));
        AllFrequenciesUnwrapper<T> unwrapper(mask, codes, camSize, hDataT, data);
        parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);

        if (useHorizontalPatterns)
        {
            result.camHorizontalCodes() = codes;
        }
        else
        {
            result.camVerticalCodes() = codes;
        }

        return result;
    }

    template <class T>
    MatTable AllFrequenciesUnwrappingMethodWrapper::Private::matchWithoutEpipolarGeometry(
        const MatcherData &matcherData_)
    {
        MatTable result;

        const PhaseShiftMatcherData &matcherData =
            static_cast<const PhaseShiftMatcherData &>(matcherData_);
        PhaseShiftGenerator *gen = matcherData.matcher->generator();
        // we need the epipolar geometry known when using block or sinusoidal modulation and
        // multiple frequency unwrapping
        Ptr<GenerationMethodWrapper> genMethodWrapper = makeGenerationMethodWrapper(
            gen->generationMethod());
        if (genMethodWrapper->objectName() !=
            DefaultGenerationMethodWrapper::className())
        {
            logError() <<
                "It's impossible to unwrap phases without knowledge of absolute (integral) matches "
                "for this kind of phaseshift generation type. Please use the ExternalMap unwrapping.";
        }

        PhaseShiftData data(gen);
        Size           camSize, projSize;
        camSize  = matcherData.camGenerator->size();
        projSize = gen->size();
        const Mat &mask = matcherData.camMask;

        bool both = (gen->direction() == Generator::BOTH);
        for (int r = 0; r < (both ? 2 : 1); ++r)
        {
            bool horiz = (both && r == 0) ||
                         (gen->direction() == Generator::HORIZONTAL);

            transposeImages<T>(matcherData.camGenerator,
                               (horiz ? hDataT : vDataT),
                               data.numPatts,
                               (r == 0 ? 0 : data.numPatts));

            Mat codes(camSize, CV_32F, Scalar::all(0));

            AllFrequenciesUnwrapper<T> unwrapper(mask,
                                                 codes,
                                                 camSize,
                                                 (horiz ? hDataT : vDataT),
                                                 data);
            parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);

            if (horiz) { result.camHorizontalCodes() = codes; }
            else { result.camVerticalCodes() = codes; }
        }

        return result;
    }

    struct ExternalMapUnwrapperData
    {
        ExternalMapUnwrapperData(const Mat            &m,
                                 Mat                  &c,
                                 Size                  cs,
                                 Size                  ps,
                                 const Mat            &i,
                                 const PhaseShiftData &d,
                                 bool                  h,
                                 const Mat            &u,
                                 int                   f,
                                 PhaseShiftGenerator  *g) :
            mask(m),
            codes(c),
            camSize(cs),
            projSize(ps),
            intensities(i),
            data(d),
            unwrapMap(u),
            horiz(h),
            phaseFreqId(f),
            gen(g) { }

        // helper functions

        const Mat            &mask;
        Mat                  &codes;
        Size                  camSize;
        Size                  projSize;
        const Mat            &intensities;
        const PhaseShiftData &data;
        const Mat            &unwrapMap;
        bool                  horiz;
        int                   phaseFreqId;
        PhaseShiftGenerator  *gen;
    };

    inline double mod(double a,
                      double b)
    {
        return fmod(fmod(a, b) + b, b);
    }

    inline double diffAngles(double a1,
                             double a2)
    {

        /*
         *   double a = mod(a2,2*M_PI)-mod(a1,2*M_PI);
         *   return mod(a+M_PI,2*M_PI)-M_PI;
         */
        return fabs(CV_PI - fabs(fabs(a1 - a2) - CV_PI));
    }

    template <class Derived, class T>
    struct ExternalMapUnwrapper : public ParallelForBody
    {
        ExternalMapUnwrapper(const ExternalMapUnwrapperData &data) : d(data) { }

        virtual void operator()(size_t _begin,
                                size_t _end) const
        {
            int begin = static_cast<int>(_begin);
            int end   = static_cast<int>(_end);

            for (int j = begin; j < end; ++j)
            {
                const uchar *mptr = (d.mask.empty() ? 0 : d.mask.ptr<uchar>(j));
                float       *ptr  = d.codes.ptr<float>(j);
                const Vec3w *ptrW = d.unwrapMap.ptr<Vec3w>(j);

                for (int i = 0; i < d.camSize.width; ++i)
                {
                    if (mptr && !mptr[i]) { continue; }

                    int      camId  = j * d.camSize.width + i;
                    const T *vptr   = d.intensities.ptr<T>(camId);
                    size_t   freqId = static_cast<size_t>(d.phaseFreqId);
                    double   period = d.data.periods[freqId];

                    // when using external map, only one frequency is used to compute the phase
                    double pW =
                        phaseFromIntensities(vptr + d.data.cumShifts[freqId],
                                             static_cast<size_t>(d.data.numShifts[freqId]),
                                             d.data.cosVals[freqId],
                                             d.data.sinVals[freqId]);
                    double ppW = pW;

                    double xp, yp;
                    pointFromMatch(ptrW[i], d.projSize, xp, yp);

                    // unwrap map is the same as a low frequency map computed using a period
                    // as wide as the width (resp. height) of the image
                    double pU  = (d.horiz ? ptrW[i][2] : ptrW[i][1]) * 2 * CV_PI / 65535.;
                    double pUx = fmod(xp, period) * 2 * CV_PI / period;
                    double pUy = fmod(yp, period) * 2 * CV_PI / period;

                    // we may need to correct the phase if it has been modulated
                    pW = demodulate(pW, (d.horiz ? yp : xp), (d.horiz ? pUx : pUy));

                    double mult = (d.horiz ? d.projSize.width : d.projSize.height) /
                                  period;
                    double p0 = pW - 2 * CV_PI * cvRound((pW - mult * pU) / (2 * CV_PI));

                    ptr[i] = static_cast<float>(p0 * period / (2 * CV_PI));

                    if ((i >= 302) && (i <= 394) && (j >= 354) && (j <= 459))
                    {
                        if (d.horiz && (fabs(ptr[i] - xp) > 5))
                        {
                            logWarning() << j << i << ptrW[i][2] << ptrW[i][1] << xp <<
                                pUx <<  fmod(yp, 16)  << ppW << pW << ptr[i]
                                         << (fmod(yp, 16) > 16 / 2. + 0.5)
                                         << (fmod(yp, 16) < 16 - 0.5)
                                         << diffAngles(pUx, ppW + CV_PI)
                                         << (diffAngles(pUx, ppW + CV_PI) < CV_PI / 4);
                        }
                    }
                }
            }
        }
        double demodulate(double phase,
                          double unwrapPosO,
                          double unwrapPosD) const
        {
            return static_cast<const Derived *>(this)->demodulate(phase, unwrapPosO,
                                                                  unwrapPosD);
        }
        const ExternalMapUnwrapperData &d;
    };

    template <class T>
    struct DefaultGenerationExternalMapUnwrapper :
        public ExternalMapUnwrapper<DefaultGenerationExternalMapUnwrapper<T>, T>
    {
        DefaultGenerationExternalMapUnwrapper(const ExternalMapUnwrapperData &d) :
            ExternalMapUnwrapper<DefaultGenerationExternalMapUnwrapper<T>, T>(d) { }
        double demodulate(double phase,
                          double /*unwrapPosO*/,
                          double /*unwrapPosD*/) const
        {
            return phase;
        }
    };

    template <class T>
    struct BlockModulationExternalMapUnwrapper :
        public ExternalMapUnwrapper<BlockModulationExternalMapUnwrapper<T>, T>
    {
        BlockModulationExternalMapUnwrapper(const ExternalMapUnwrapperData &d) :
            ExternalMapUnwrapper<BlockModulationExternalMapUnwrapper<T>, T>(d)
        {
            modPeriod = static_cast<const Generation::BlockModulation &>(
                d.gen->generationMethod()).modPeriod;
        }

        double demodulate(double phase,
                          double unwrapPosO,
                          double wrappedPhaseD) const
        {
            double p = phase;

            bool demod = ((fmod(unwrapPosO, modPeriod) > modPeriod / 2 + 0.5) &&
                          (fmod(unwrapPosO, modPeriod) < modPeriod - 0.5)) ||
                         (diffAngles(wrappedPhaseD, p +
                                     CV_PI) < CV_PI / (modPeriod / 4));

            if (demod)
            {
                p += CV_PI;
            }
            if (p > 2 * CV_PI) { p -= 2 * CV_PI; }

            /*
             *   //une erreur dans le unwrapPos de la dimension orthogonale .. on demodule pas
             *   if (fabs(wrappedPhaseD-p) > M_PI/4 && fabs(wrappedPhaseD-p) < 2*M_PI-M_PI/4)
             *   p = phase;
             */

            return p;
        }
        double modPeriod;
    };

    template <class T>
    struct SinusoidalModulationExternalMapUnwrapper :
        public ExternalMapUnwrapper<SinusoidalModulationExternalMapUnwrapper<T>, T>
    {
        SinusoidalModulationExternalMapUnwrapper(const ExternalMapUnwrapperData &d) :
            ExternalMapUnwrapper<SinusoidalModulationExternalMapUnwrapper<T>, T>(d)
        {
            modPeriod = static_cast<const Generation::SinusoidalModulation &>(
                d.gen->generationMethod()).modPeriod;
        }

        double demodulate(double phase,
                          double unwrapPosO,
                          double /*unwrapPosD*/) const
        {
            double p = phase - cos(unwrapPosO * 2 * CV_PI / modPeriod);
            if (p < 0) { p += 2 * CV_PI; }

            return p;
        }
        double modPeriod;
    };

    template <class T>
    MatTable ExternalMapUnwrappingMethodWrapper::Private::matchWithEpipolarGeometry(
        const MatcherData &matcherData_)
    {
        MatTable result;

        const PhaseShiftMatcherData &matcherData =
            static_cast<const PhaseShiftMatcherData &>(matcherData_);
        PhaseShiftGenerator *gen = matcherData.matcher->generator();
        PhaseShiftData       data(gen);

        Size camSize, projSize;
        camSize  = matcherData.camGenerator->size();
        projSize = gen->size();
        const Mat &mask = matcherData.camMask;

        int phaseFreqId = paramPhaseFreqId;
        unwrapMap = imread(string(paramPath), -1);
        if (unwrapMap.empty())
        {
            logError() << "Unable to read unwrap map";
        }
        if (unwrapMap.type() != CV_16UC3)
        {
            logError() << "Unwrap map is not a RGB 16bit image";
        }
        if (unwrapMap.size() != camSize)
        {
            logError() << "Unwrap map must be the same size as camera image";
        }

        bool useHorizontalPatterns = matcherData.useHorizontalPatterns;
        transposeImages<T>(matcherData.camGenerator, hDataT, data.numPatts, 0);

        // if (verbose) std::cout << std::endl;
        // logInfo() << "Transposing" << nbPatts << "images";

        Mat                      codes(camSize, CV_32F, Scalar::all(0));
        ExternalMapUnwrapperData uData(mask, codes,
                                       camSize, projSize,
                                       hDataT, data,
                                       useHorizontalPatterns,
                                       unwrapMap, phaseFreqId,
                                       gen);

        switch (gen->generationMethod().type)
        {
            case Generation::Method::Default:
            {
                DefaultGenerationExternalMapUnwrapper<T> unwrapper(uData);
                parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);
                break;
            }
            case Generation::Method::Block:
            {
                BlockModulationExternalMapUnwrapper<T> unwrapper(uData);
                parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);
                break;
            }
            case Generation::Method::Sinusoidal:
            {
                SinusoidalModulationExternalMapUnwrapper<T> unwrapper(uData);
                parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);
                break;
            }
        }

        // because we only use two frequencies to unwrap .. we can expect some errors at boundaries
        medianBlur(codes, codes, 5);

        if (useHorizontalPatterns)
        {
            result.camHorizontalCodes() = codes;
        }
        else
        {
            result.camVerticalCodes() = codes;
        }

        return result;
    }

    ExternalMapUnwrappingMethodWrapper::~ExternalMapUnwrappingMethodWrapper() { }
    ExternalMapUnwrappingMethodWrapper::Private::~Private() { }

    template <class T>
    MatTable ExternalMapUnwrappingMethodWrapper::Private::matchWithoutEpipolarGeometry(
        const MatcherData &matcherData_)
    {
        MatTable result;

        const PhaseShiftMatcherData &matcherData =
            static_cast<const PhaseShiftMatcherData &>(matcherData_);
        PhaseShiftGenerator *gen = matcherData.matcher->generator();
        PhaseShiftData       data(gen);

        Size camSize, projSize;
        camSize  = matcherData.camGenerator->size();
        projSize = gen->size();
        const Mat &mask = matcherData.camMask;

        int phaseFreqId = paramPhaseFreqId;
        unwrapMap = imread(string(paramPath), -1);
        if (unwrapMap.empty())
        {
            logError() << "Unable to read unwrap map";
        }
        if (unwrapMap.type() != CV_16UC3)
        {
            logError() << "Unwrap map is not a RGB 16bit image";
        }
        if (unwrapMap.size() != camSize)
        {
            logError() << "Unwrap map must be the same size as camera image";
        }

        bool both = (gen->direction() == Generator::BOTH);
        for (int r = 0; r < (both ? 2 : 1); ++r)
        {
            bool horiz = (both && r == 0) ||
                         (gen->direction() == Generator::HORIZONTAL);

            // if (verbose) { std::cout << "Transposing high frequency images "; std::cout.flush();
            // }
            transposeImages<T>(matcherData.camGenerator,
                               (horiz ? hDataT : vDataT),
                               data.numPatts,
                               (r == 0 ? 0 : data.numPatts));

            // if (verbose) std::cout << std::endl;
            // logInfo() << "Transposing" << nbPatts << "images";

            Mat codes(camSize, CV_32F, Scalar::all(0));

            ExternalMapUnwrapperData uData(mask,
                                           codes,
                                           camSize,
                                           projSize,
                                           horiz ? hDataT : vDataT,
                                           data,
                                           horiz,
                                           unwrapMap,
                                           phaseFreqId,
                                           gen);

            switch (gen->generationMethod().type)
            {
                case Generation::Method::Default:
                {
                    DefaultGenerationExternalMapUnwrapper<T> unwrapper(uData);
                    parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);
                    break;
                }
                case Generation::Method::Block:
                {
                    BlockModulationExternalMapUnwrapper<T> unwrapper(uData);
                    parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);
                    break;
                }
                case Generation::Method::Sinusoidal:
                {
                    SinusoidalModulationExternalMapUnwrapper<T> unwrapper(uData);
                    parallel_for(0, static_cast<size_t>(camSize.height), unwrapper);
                    break;
                }
            }

            // because we only use two frequencies to unwrap .. we can expect some errors at
            // boundaries
            medianBlur(codes, codes, 5);

            if (horiz) { result.camHorizontalCodes() = codes; }
            else { result.camVerticalCodes() = codes; }
        }

        return result;
    }

    PhaseShiftMatcher::Private::Private(PhaseShiftMatcher              *ptr,
                                        const Ptr<PhaseShiftGenerator> &g,
                                        const Unwrapping::Method       &type) :
        AbstractPrivate(ptr),
        gen(g),
        unwrappingMethodWrapper(makeUnwrappingMethodWrapper(type))
    { }

    MatTable PhaseShiftMatcher::Private::matchWithEpipolarGeometry(
        Generator *const                 camGenerator,
        Generator *const                 projGenerator,
        const Mat                       &camMask,
        const Mat                       &projMask,
        const CameraGeometricParameters &camParameters,
        const CameraGeometricParameters &projParameters,
        bool                             useHorizontalPatterns)
    {
        PhaseShiftMatcherData matcherData(pubPtr(),
                                          camGenerator, projGenerator,
                                          camMask, projMask,
                                          camParameters, projParameters,
                                          useHorizontalPatterns);

        MatTable result = unwrappingMethodWrapper->matchWithEpipolarGeometry(matcherData);

        Size camSize, projSize;
        camSize  = matcherData.camGenerator->size();
        projSize = gen->size();

        Mat camMatch(camSize, CV_16UC3, Scalar::all(0));
        Mat codes = (useHorizontalPatterns ? result.camHorizontalCodes() :
                     result.camVerticalCodes());

        for (int j = 0; j < camSize.height; ++j)
        {
            const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(j));
            Vec3w       *ptr  = camMatch.ptr<Vec3w>(j);
            float       *ptrC = codes.ptr<float>(j);

            for (int i = 0; i < camSize.width; ++i)
            {
                if (mptr && !mptr[i]) { continue; }
                pointToMatch(useHorizontalPatterns ? ptrC[i] : 0,
                             useHorizontalPatterns ? 0 : ptrC[i],
                             projSize, ptr[i]);
            }
        }

        // if neeeded by the caller ...
        if (useHorizontalPatterns)
        {
            codes.convertTo(codes, CV_16U, 65535. / projSize.width, 0);
            result.camHorizontalCodes() = codes;
        }
        else
        {
            codes.convertTo(codes, CV_16U, 65535. / projSize.height, 0);
            result.camVerticalCodes() = codes;
        }
        result.camMatchMat() = camMatch;

        return result;
    }

    MatTable PhaseShiftMatcher::Private::matchWithoutEpipolarGeometry(
        Generator *const camGenerator,
        Generator *const projGenerator,
        const Mat       &camMask,
        const Mat       &projMask)
    {
        PhaseShiftMatcherData matcherData(
            pubPtr(), camGenerator, projGenerator, camMask, projMask);

        MatTable result =
            unwrappingMethodWrapper->matchWithoutEpipolarGeometry(matcherData);

        Size camSize, projSize;
        camSize  = matcherData.camGenerator->size();
        projSize = gen->size();

        Mat camMatch(camSize, CV_16UC3, Scalar::all(0));
        Mat codesX = result.camHorizontalCodes();
        Mat codesY = result.camVerticalCodes();

        for (int j = 0; j < camSize.height; ++j)
        {
            const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(j));
            Vec3w       *ptr  = camMatch.ptr<Vec3w>(j);
            float       *ptrX = codesX.ptr<float>(j);
            float       *ptrY = codesY.ptr<float>(j);

            for (int i = 0; i < camSize.width; ++i)
            {
                if (mptr && !mptr[i]) { continue; }
                pointToMatch(ptrX[i], ptrY[i], projSize, ptr[i]);
            }
        }

        // if neeeded by the caller ...
        codesX.convertTo(codesX, CV_16U, 65535. / projSize.width, 0);
        codesY.convertTo(codesY, CV_16U, 65535. / projSize.height, 0);

        result.camMatchMat() = camMatch;

        return result;
    }

    bool PhaseShiftMatcher::Private::read(const FileNode &node)
    {
        // the global read function will possibly read embedded parameters
        // or read from another file if a configFilePath and configNodeName
        // tags are available
        node["generator"] >> gen;

        // the global read function will allocate the pointer for us
        node["unwrappingMethod"] >> unwrappingMethodWrapper;

        return true;
    }

    bool PhaseShiftMatcher::Private::write(FileStorage &fs) const
    {
        // the global write function will possibly embed the parameters
        // or link to another file if a configFilePath() and configNodeName()
        // are available
        fs << "generator" << gen;

        // the global write function will properly serialize polymorphic base pointer
        fs << "unwrappingMethod" << unwrappingMethodWrapper;

        return true;
    }

    bool PhaseShiftMatcher::Private::configure(istream *is,
                                               ostream *os)
    {
        if (os) { *os << "Configuring Phase Shift matcher parameters." << endl; }
        // the global configure function will allocate the pointer for us
        // and possibly read the parameters from another file if they are not embedded
        // in this one;
        cl3ds::configure(gen, is, os,
                         string(),
                         makePtr<PhaseShiftGenerator>(),
                         true, false);

        if (os) { *os << "Which Phase Shift unwrapping type should be used ?" << endl; }
        // the global configure function will allocate the pointer for us
        cl3ds::configure(unwrappingMethodWrapper, is, os,
                         string(),
                         makePtr<AllFrequenciesUnwrappingMethodWrapper>(),
                         false); // embedding is mandatory here

        return true;
    }

    size_t PhaseShiftMatcher::Private::hash() const
    {
        return Hasher() << gen->hash()
                        << unwrappingMethodWrapper->hash();
    }

    PhaseShiftMatcher::PhaseShiftMatcher(const Ptr<PhaseShiftGenerator> &gen,
                                         const Unwrapping::Method       &type) :
        Matcher(new Private(this, gen, type))
    { }

    PhaseShiftMatcher::~PhaseShiftMatcher() { }

    Unwrapping::AllFrequencies::AllFrequencies()
    {
        type = Unwrapping::Method::AllFrequencies;
    }

    Unwrapping::ExternalMap::ExternalMap(const string &p,
                                         int           f) :
        path(p),
        phaseFrequencyId(f)
    {
        type = Unwrapping::Method::ExternalMap;
    }

    const std::string & PhaseShiftMatcher::Unwrapping::ExternalMap::defaultPath()
    {
        static string path = "unwrap.png";

        return path;
    }

    int PhaseShiftMatcher::Unwrapping::ExternalMap::defaultPhaseFrequencyId()
    {
        return 0;
    }

    PhaseShiftGenerator * PhaseShiftMatcher::generator() const
    {
        CL3DS_PRIV_CONST_PTR(PhaseShiftMatcher);

        return priv->gen;
    }

    void PhaseShiftMatcher::setGenerator(const cv::Ptr<PhaseShiftGenerator> &gen)
    {
        CL3DS_PRIV_PTR(PhaseShiftMatcher);

        priv->gen = gen;
    }

    PhaseShiftGenerator * PhaseShiftMatcher::defaultGenerator()
    {
        static Ptr<PhaseShiftGenerator> gen = makePtr<PhaseShiftGenerator>();

        return gen;
    }

    const PhaseShiftMatcher::Unwrapping::Method & PhaseShiftMatcher::unwrappingMethod()
    const
    {
        CL3DS_PRIV_CONST_PTR(PhaseShiftMatcher);

        static Ptr<Unwrapping::Method> unwrapMethod;
        unwrapMethod = makeUnwrappingMethod(priv->unwrappingMethodWrapper);

        return *unwrapMethod;
    }

    void PhaseShiftMatcher::setUnwrappingMethod(
        const PhaseShiftMatcher::Unwrapping::Method &type)
    {
        CL3DS_PRIV_PTR(PhaseShiftMatcher);

        Ptr<UnwrappingMethodWrapper> unwrapMethodWrapper =
            makeUnwrappingMethodWrapper(type);
        priv->unwrappingMethodWrapper = unwrapMethodWrapper;
    }

    Unwrapping::AllFrequencies PhaseShiftMatcher::defaultUnwrappingMethod()
    {
        return Unwrapping::AllFrequencies();
    }
}
