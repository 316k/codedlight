/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include "generator_p.h"
#include "seq_buffer.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct SequenceBuffer::Private : public AbstractPrivate
    {
        Private(SequenceBuffer *ptr) :
            AbstractPrivate(ptr)
        { }
        ~Private();

        bool read(const FileNode &node)
        {
            CL3DS_UNUSED(node);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            CL3DS_UNUSED(fs);

            return true;
        }

        bool configure(istream *,
                       ostream *)
        {
            return true;
        }

        size_t hash() const
        {
            return Hasher() << images;
        }

        void next()
        { }

        Mat get()
        {
            return images[pubPtr()->index()];
        }

        void reset()
        { }

        size_t count() const
        {
            return images.size();
        }

        Mat & operator[](size_t index)
        {
            if (index >= images.size())
            {
                logError() << "Index is out of range : " << index << " >= " <<
                    images.size();
            }

            return images[index];
        }

        const Mat & operator[](size_t index) const
        {
            if (index >= images.size())
            {
                logError() << "Index is out of range : " << index << " >= " <<
                    images.size();
            }

            return images[index];
        }

        void resize(size_t size)
        {
            images.resize(size);
        }

        Size size() const
        {
            if (images.empty())
            {
                logError() << "No image in the buffer, impossible " <<
                    "to determine the pattern size.";
            }

            return images[0].size();
        }

        int type() const
        {
            if (images.empty())
            {
                logError() << "No image in the buffer, impossible " <<
                    "to determine the pattern type.";
            }

            return images[0].type();
        }

        vector<Mat> images;
        Size        imageSize;
        int         imageType;

        CL3DS_PUB_IMPL(SequenceBuffer)
    };

    SequenceBuffer::Private::~Private() { }

    SequenceBuffer::SequenceBuffer() :
        Generator(0)
    {
        init();
    }

    SequenceBuffer::SequenceBuffer(const Mat &image) :
        Generator(0)
    {
        init();

        // cannot be called before init !
        CL3DS_PRIV_PTR(SequenceBuffer);
        priv->images.push_back(image);
    }

    SequenceBuffer::SequenceBuffer(Generator *const gen,
                                   size_t           count) :
        Generator(0)
    {
        init();
        if (!gen) { return; }
        if (count == 0) { count = gen->count(); }

        // cannot be called before init !
        CL3DS_PRIV_PTR(SequenceBuffer);

        gen->reset();
        priv->images.resize(count);
        for (size_t i = 0; i < count; ++i)
        {
            priv->images[i] = gen->get();
        }
    }

    void SequenceBuffer::init()
    {
        setPrivateImplementation(new Private(this));
    }

    SequenceBuffer::~SequenceBuffer() { }

    Mat & SequenceBuffer::operator[](size_t index)
    {
        CL3DS_PRIV_PTR(SequenceBuffer);

        return priv->operator[](index);
    }

    const Mat & SequenceBuffer::operator[](size_t index) const
    {
        CL3DS_PRIV_CONST_PTR(SequenceBuffer);

        return priv->operator[](index);
    }

    void SequenceBuffer::resize(size_t size)
    {
        CL3DS_PRIV_PTR(SequenceBuffer);

        priv->resize(size);
    }
}
