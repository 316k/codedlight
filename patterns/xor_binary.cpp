/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include "../core/parameter.h"
#include "seq_buffer.h"
#include "generator_p.h"
#include "matcher_p.h"
#include "xor_binary.h"
#include "graycode_p.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    typedef XorBinaryGenerator::BinarizationMethod BinarizationMethod;
    typedef XorBinaryGenerator::XorOperation       XorOperation;
    typedef XorBinaryGenerator::Direction          Direction;

    struct XorBinaryGenerator::Private : public AbstractPrivate
    {
        Private(XorBinaryGenerator *ptr,
                XorOperation        operation,
                Direction           direction,
                BinarizationMethod  method,
                Size                size) :
            AbstractPrivate(ptr)
        {
            paramSize = TypeParameter<Size>(
                "size",
                "Size of the pattern to generate",
                XorBinaryGenerator::defaultSize(),
                makePtr< BoundConstraint<Size> >(Size(0, 0), Size(10000, 10000)));

            setSize(size);

            paramBinarizationMethod = TypeParameter<int>(
                "bin-method",
                "Type of binarization :\n"
                "\t0 -> projects black and white pattern to estimate per pixel threshold\n"
                "\t1 -> estimates best threshold from min and max values of captured images\n"
                "\t2 -> projects inverted patterns and binarize based on sign of differences\n",
                GrayCodeGenerator::defaultBinarizationMethod(),
                makePtr< BoundConstraint<int> >(-1, 3));
            paramBinarizationMethod = method;

            paramXorOperation = TypeParameter<int>(
                "xor-op",
                "Type of Xor operation : 0->XOR-2, 1->XOR-4, 2->XOR-8",
                XorBinaryGenerator::defaultXorOperation(),
                makePtr< BoundConstraint<int> >(-1, 3));
            paramXorOperation = operation;

            vector<int> valuesDirection;
            valuesDirection.push_back(Generator::HORIZONTAL);
            valuesDirection.push_back(Generator::VERTICAL);
            valuesDirection.push_back(Generator::BOTH);
            paramDirection =
                TypeParameter<int>("direction",
                                   "Direction for the pattern",
                                   XorBinaryGenerator::defaultDirection(),
                                   makePtr< EnumerationConstraint<int> >(valuesDirection));
            paramDirection = direction;

        }
        ~Private();

        void next()
        {
            switch (paramBinarizationMethod)
            {
                case GrayCodeGenerator::BLACK_AND_WHITE:
                {
                    if (!black) { black = true; }
                    else if (!white) { white = true; }
                    else { bit++; }
                    break;
                }
                case GrayCodeGenerator::INVERSE_PATTERNS:
                {
                    if (inv) { bit++; }
                    inv = !inv;
                    break;
                }
                case GrayCodeGenerator::PER_PIXEL_THRESHOLD:
                {
                    bit++;
                    break;
                }
            }
            if (((paramDirection == Generator::VERTICAL) && (bit >= nbBitsY)) ||
                ((paramDirection != Generator::VERTICAL) && (bit >= nbBitsX)))
            {
                bit = 0;
                inv = false;
                dir = Generator::VERTICAL;
                enc.setNbBits(nbBitsY);
            }
        }

        // should we try to shift the sequence, to avoid only 0 bits (for pos=0) ?
        Mat get()
        {
            if (paramBinarizationMethod == GrayCodeGenerator::BLACK_AND_WHITE)
            {
                if (!black) { return Mat(paramSize, CV_8U, Scalar::all(0)); }
                else if (!white) { return Mat(paramSize, CV_8U, Scalar::all(255)); }
            }

            Mat  pattern;
            Size size = static_cast<Size>(paramSize);
            bool vert = (dir == Generator::VERTICAL);
            pattern.create(Size(vert ? size.width : size.height,
                                vert ? size.height : size.width), CV_8U);

            size_t rows = static_cast<size_t>(pattern.rows);
            pattBits.resize(rows);
            for (size_t j = 0; j < rows; ++j)
            {
                pattBits[j] = enc.bit(j, bit);
            }

            maskBits.resize(rows);
            size_t op = static_cast<size_t>(static_cast<int>(paramXorOperation));
            for (size_t j = 0; j < rows; ++j)
            {
                maskBits[j] = (bit <= op ? 0 : enc.bit(j, op));
            }

            MatIterator_<uchar> it = pattern.begin<uchar>();
            for (size_t j = 0; j < rows; ++j)
            {
                uchar b   = pattBits[j] ^ maskBits[j];
                uchar val = 255 * (inv ? 1 - b : b);
                fill(it, it + pattern.cols, val);
                it += pattern.cols;
            }

            if (!vert) { pattern = pattern.t(); }

            return pattern;
        }

        void reset()
        {
            black = false;
            white = false;
            bit   = 0;
            inv   = false;
            dir   = static_cast<Direction>(static_cast<int>(paramDirection));
            enc.setNbBits(dir == Generator::VERTICAL ? nbBitsY : nbBitsX);
        }

        void setSize(Size size)
        {
            paramSize = size;
            nbBitsX   = static_cast<size_t>(ceil(log2(size.width)));
            nbBitsY   = static_cast<size_t>(ceil(log2(size.height)));
            nbBits    = nbBitsX + nbBitsY;
        }

        Size size() const
        {
            return paramSize;
        }

        int type() const
        {
            return CV_8U;
        }

        bool read(const FileNode &node)
        {
            paramSize.read(node);
            setSize(paramSize);
            paramXorOperation.read(node);
            paramDirection.read(node);
            paramBinarizationMethod.read(node);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramSize.write(fs);
            paramXorOperation.write(fs);
            paramDirection.write(fs);
            paramBinarizationMethod.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            if (os) { *os << "Configuring Gray code generator parameters" << endl; }
            paramSize.configure(is, os);
            setSize(paramSize);
            paramXorOperation.configure(is, os);
            paramDirection.configure(is, os);
            paramBinarizationMethod.configure(is, os);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramSize
                            << paramXorOperation
                            << paramDirection
                            << paramBinarizationMethod;
        }

        size_t count() const
        {
            size_t c = 0;

            switch (paramDirection)
            {
                case XorBinaryGenerator::HORIZONTAL:
                {
                    c = nbBitsX;
                    break;
                }

                case XorBinaryGenerator::VERTICAL:
                {
                    c = nbBitsY;
                    break;
                }
                case XorBinaryGenerator::BOTH:
                {
                    c = nbBitsX + nbBitsY;
                    break;
                }

            }

            if (paramBinarizationMethod == GrayCodeGenerator::BLACK_AND_WHITE)
            {
                c += 2;
            }
            else if (paramBinarizationMethod == GrayCodeGenerator::INVERSE_PATTERNS)
            {
                c *= 2;
            }

            return c;
        }

        bool            inv;
        bool            black;
        bool            white;
        size_t          bit;
        Direction       dir;
        GrayCodeEncoder enc;

        size_t        nbBitsX;
        size_t        nbBitsY;
        size_t        nbBits;
        vector<uchar> pattBits;
        vector<uchar> maskBits;

        TypeParameter<Size> paramSize;
        TypeParameter<int>  paramXorOperation;
        TypeParameter<int>  paramDirection;
        TypeParameter<int>  paramBinarizationMethod;

        CL3DS_PUB_IMPL(XorBinaryGenerator)
    };

    XorBinaryGenerator::Private::~Private()
    { }

    XorBinaryGenerator::XorBinaryGenerator(Size               size,
                                           XorOperation       operation,
                                           Direction          direction,
                                           BinarizationMethod method) :
        Generator(new Private(this, operation, direction, method, size))
    { }

    XorBinaryGenerator::~XorBinaryGenerator()
    { }

    Direction XorBinaryGenerator::direction() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryGenerator);

        return static_cast<Direction>(int(priv->paramDirection));
    }

    void XorBinaryGenerator::setDirection(const Direction &dir)
    {
        CL3DS_PRIV_PTR(XorBinaryGenerator);

        priv->paramDirection = dir;
    }

    Direction XorBinaryGenerator::defaultDirection()
    {
        return HORIZONTAL;
    }

    Size XorBinaryGenerator::size() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryGenerator);

        return priv->size();
    }

    void XorBinaryGenerator::setSize(Size size)
    {
        CL3DS_PRIV_PTR(XorBinaryGenerator);

        priv->setSize(size);
    }

    Size XorBinaryGenerator::defaultSize()
    {
        return Size(800, 600);
    }

    XorOperation XorBinaryGenerator::xorOperation() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryGenerator);

        return static_cast<XorOperation>(static_cast<int>(priv->paramXorOperation));
    }

    void XorBinaryGenerator::setXorOperation(XorOperation operation)
    {
        CL3DS_PRIV_PTR(XorBinaryGenerator);

        priv->paramXorOperation = operation;
    }

    XorOperation XorBinaryGenerator::defaultXorOperation()
    {
        return Xor8;
    }

    BinarizationMethod XorBinaryGenerator::binarizationMethod() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryGenerator);

        return static_cast<BinarizationMethod>(
            static_cast<int>(priv->paramBinarizationMethod));
    }

    void XorBinaryGenerator::setBinarizationMethod(BinarizationMethod method)
    {
        CL3DS_PRIV_PTR(XorBinaryGenerator);

        priv->paramBinarizationMethod = method;
    }

    BinarizationMethod XorBinaryGenerator::defaultBinarizationMethod()
    {
        return GrayCodeGenerator::PER_PIXEL_THRESHOLD;
    }

    size_t XorBinaryGenerator::nbBitsX() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryGenerator);

        return priv->nbBitsX;
    }

    size_t XorBinaryGenerator::nbBitsY() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryGenerator);

        return priv->nbBitsY;
    }

    size_t XorBinaryGenerator::nbBits() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryGenerator);

        return priv->nbBits;
    }

    struct XorBinaryMatcher::Private : public AbstractPrivate
    {
        Private(XorBinaryMatcher              *ptr,
                const Ptr<XorBinaryGenerator> &generator,
                int                            medianFilterSize) :
            AbstractPrivate(ptr),
            gen(generator)
        {
            paramMedianFilterSize = TypeParameter<int>(
                "med-size",
                "Size of the median filter neighborhood : if 0, none, otherwise n should be odd (nxn kernel)",
                XorBinaryMatcher::defaultMedianFilterSize(),
                makePtr< BoundConstraint<int> >(-1, 8));
            paramMedianFilterSize = medianFilterSize;
        }
        ~Private();

        template <class T> MatTable matchWithEpipolarGeometry(const MatcherData &);
        MatTable matchWithEpipolarGeometry(Generator *const                 camGenerator,
                                           Generator *const                 projGenerator,
                                           const Mat                       &camMask,
                                           const Mat                       &projMask,
                                           const CameraGeometricParameters &camParameters,
                                           const CameraGeometricParameters &projParameters,
                                           bool                             useHorizontalPatterns)
        {
            MatcherData data(camGenerator, projGenerator,
                             camMask, projMask,
                             camParameters, projParameters,
                             useHorizontalPatterns);

            return dispatchWithGeometry(this, data, camGenerator->type());
        }

        template <class T> MatTable matchWithoutEpipolarGeometry(const MatcherData &);
        MatTable matchWithoutEpipolarGeometry(Generator *const camGenerator,
                                              Generator *const projGenerator,
                                              const Mat       &camMask,
                                              const Mat       &projMask)
        {
            MatcherData data(camGenerator,
                             projGenerator, camMask,
                             projMask);

            return dispatchWithoutGeometry(this, data, camGenerator->type());
        }

        template <class T>
        void decode(const Mat         &camMask,
                    BinarizationMethod method,
                    SequenceBuffer    &buf,
                    size_t             pos,
                    size_t             xorIndex,
                    const MatTable    &result,
                    size_t             nbBits,
                    Mat               &codes);

        bool read(const FileNode &node)
        {
            // the global read function will possibly read embedded parameters
            // or read from another file if a configFilePath and configNodeName
            // tags are available
            node["generator"] >> gen;
            paramMedianFilterSize.read(node);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            // the global write function will possibly embed the parameters
            // or link to another file if a configFilePath() and configNodeName()
            // are available
            fs << "generator" << gen;
            paramMedianFilterSize.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            if (os) { *os << "Configuring Gray code matcher parameters" << endl; }
            // the global configure function will allocate the pointer for us
            // and possibly read the parameters from another file if they are not embedded
            // in this one;
            cl3ds::configure(gen, is, os,
                             string(),
                             makePtr<XorBinaryGenerator>());
            paramMedianFilterSize.configure(is, os);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << gen->hash()
                            << paramMedianFilterSize;
        }

        Ptr<XorBinaryGenerator> gen;
        TypeParameter<int>      paramMedianFilterSize;
        vector<Mat>             tmpIms;
        GrayCodeDecoder         dec;

        CL3DS_PUB_IMPL(XorBinaryMatcher)
    };

    XorBinaryMatcher::Private::~Private()
    { }

    template <class T>
    void XorBinaryMatcher::Private::decode(const Mat         &camMask,
                                           BinarizationMethod method,
                                           SequenceBuffer    &buf,
                                           size_t             pos,
                                           size_t             xorIndex,
                                           const MatTable    &result,
                                           size_t             nbBits,
                                           Mat               &codes)
    {
        Size   camSize = camMask.size();
        RNGBit rng;
        dec.setNbBits(nbBits);

        tmpIms.resize(nbBits);
        for (size_t i = 0; i < nbBits; ++i)
        {
            Mat m  = buf[pos++];
            Mat tm = (method == GrayCodeGenerator::INVERSE_PATTERNS ?
                      buf[pos++] : result["thresh"]);
            tmpIms[i].create(m.size(), m.type());
            tmpIms[i] = Scalar::all(0);

            for (int y = 0; y < camSize.height; ++y)
            {
                const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(y));
                T           *cptr = tmpIms[i].ptr<T>(y);
                const T     *xptr = m.ptr<T>(y);
                const T     *tptr = tm.ptr<T>(y);

                for (size_t x = 0; x < static_cast<size_t>(camSize.width); ++x)
                {
                    if (mptr && !mptr[x]) { continue; }
                    if (xptr[x] > tptr[x])
                    {
                        cptr[x] = 1;
                    }
                    else if (std::abs<double>(xptr[x] - tptr[x]) <= 1.0)
                    {
                        // random si les valeurs sont égales
                        if (rng()) { cptr[x] = 1; }
                    }
                    // else nothing to do
                }
            }
        }

        for (size_t i = xorIndex + 1; i < nbBits; ++i)
        {
            // now unxor !
            bitwise_xor(tmpIms[i], tmpIms[xorIndex], tmpIms[i], camMask);
        }

        for (size_t i = 0; i < nbBits; ++i)
        {
            if (paramMedianFilterSize > 0)
            {
                medianBlur(tmpIms[i], tmpIms[i], paramMedianFilterSize);
            }

            for (int y = 0; y < camSize.height; ++y)
            {
                const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(y));
                ushort      *cptr = codes.ptr<ushort>(y);
                const T     *xptr = tmpIms[i].ptr<T>(y);

                for (int x = 0; x < camSize.width; ++x)
                {
                    if (mptr && !mptr[x]) { continue; }
                    cptr[x] |= (xptr[x] << i);
                }
            }
        }

        MatIterator_<ushort> it;
        for (it = codes.begin<ushort>(); it != codes.end<ushort>(); ++it)
        {
            *it = static_cast<ushort>(dec(*it));
        }
    }

    template <class T>
    MatTable XorBinaryMatcher::Private::matchWithEpipolarGeometry(const MatcherData &data)
    {
        MatTable result;

        Generator *const camGen = data.camGenerator;
        Size             camSize, projSize;
        camSize  = camGen->size();
        projSize = gen->size();

        BinarizationMethod method = gen->binarizationMethod();
        SequenceBuffer     buf(camGen, gen->count());
        threshold<T>(method, buf, result);

        size_t     xorIndex = gen->xorOperation();
        const Mat &camMask  = data.camMask;
        size_t     pos      = 0;
        if (method == GrayCodeGenerator::BLACK_AND_WHITE)
        {
            // skip black and white
            pos = 2;
        }

        // only do one direction !
        bool   useHorizontalPatterns = data.useHorizontalPatterns;
        size_t nbBits                =
            (useHorizontalPatterns ? gen->nbBitsX() : gen->nbBitsY());
        Mat codes(camSize, CV_16U, Scalar::all(0));
        decode<T>(camMask, method, buf, pos, xorIndex, result, nbBits, codes);

        Mat camMatch(camSize, CV_16UC3);
        for (int y = 0; y < camSize.height; ++y)
        {
            const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(y));
            Vec3w       *ptr  = camMatch.ptr<Vec3w>(y);
            ushort      *ptrC = codes.ptr<ushort>(y);

            for (int x = 0; x < camSize.width; ++x)
            {
                if (mptr && !mptr[x]) { continue; }
                pointToMatch(useHorizontalPatterns ? ptrC[x] : 0,
                             useHorizontalPatterns ? 0 : ptrC[x],
                             projSize, ptr[x]);
            }
        }

        // if neeeded by the caller ...
        if (useHorizontalPatterns)
        {
            codes.convertTo(codes, CV_16U, 65535. / projSize.width, 0);
            result.camHorizontalCodes() = codes;
        }
        else
        {
            codes.convertTo(codes, CV_16U, 65535. / projSize.height, 0);
            result.camVerticalCodes() = codes;
        }
        result.camMatchMat() = camMatch;

        return result;
    }

    template <class T>
    MatTable XorBinaryMatcher::Private::matchWithoutEpipolarGeometry(
        const MatcherData &data)
    {
        MatTable result;

        Generator *const camGen = data.camGenerator;
        Size             camSize, projSize;
        camSize  = camGen->size();
        projSize = gen->size();

        BinarizationMethod method = gen->binarizationMethod();
        SequenceBuffer     buf(camGen, gen->count());
        threshold<T>(method, buf, result);

        size_t     xorIndex = gen->xorOperation();
        const Mat &camMask  = data.camMask;
        size_t     pos      = 0;
        if (method == GrayCodeGenerator::BLACK_AND_WHITE)
        {
            // skip black and white
            pos = 2;
        }

        size_t nbBitsX = gen->nbBitsX();
        size_t nbBitsY = gen->nbBitsY();
        Mat    codesX(camSize, CV_16U, Scalar::all(0));
        Mat    codesY(camSize, CV_16U, Scalar::all(0));
        decode<T>(camMask, method, buf, pos, xorIndex, result, nbBitsX, codesX);
        decode<T>(camMask, method, buf, pos + nbBitsX, xorIndex, result, nbBitsY, codesY);

        Mat camMatch(camSize, CV_16UC3);
        for (int y = 0; y < camSize.height; ++y)
        {
            const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(y));
            Vec3w       *ptr  = camMatch.ptr<Vec3w>(y);
            ushort      *ptrX = codesX.ptr<ushort>(y);
            ushort      *ptrY = codesY.ptr<ushort>(y);

            for (int x = 0; x < camSize.width; ++x)
            {
                if (mptr && !mptr[x]) { continue; }
                pointToMatch(ptrX[x], ptrY[x], projSize, ptr[x]);
            }
        }

        // if neeeded by the caller ...
        codesX.convertTo(codesX, CV_16U, 65535. / projSize.width, 0);
        codesY.convertTo(codesY, CV_16U, 65535. / projSize.height, 0);

        result.camHorizontalCodes() = codesX;
        result.camVerticalCodes()   = codesY;
        result.camMatchMat()        = camMatch;

        return result;
    }

    XorBinaryMatcher::XorBinaryMatcher(const Ptr<XorBinaryGenerator> &generator,
                                       int                            medianFilterSize) :
        Matcher(new Private(this, generator, medianFilterSize))
    { }

    XorBinaryMatcher::~XorBinaryMatcher()
    { }

    XorBinaryGenerator * XorBinaryMatcher::generator() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryMatcher);

        return priv->gen;
    }

    void XorBinaryMatcher::setGenerator(const Ptr<XorBinaryGenerator> &gen)
    {
        CL3DS_PRIV_PTR(XorBinaryMatcher);

        priv->gen = gen;
    }

    XorBinaryGenerator * XorBinaryMatcher::defaultGenerator()
    {
        static Ptr<XorBinaryGenerator> gen = makePtr<XorBinaryGenerator>();

        return gen;
    }

    int XorBinaryMatcher::medianFilterSize() const
    {
        CL3DS_PRIV_CONST_PTR(XorBinaryMatcher);

        return priv->paramMedianFilterSize;
    }

    void XorBinaryMatcher::setMedianFilterSize(int medianFilterSize)
    {
        CL3DS_PRIV_PTR(XorBinaryMatcher);

        priv->paramMedianFilterSize = medianFilterSize;
    }

    int XorBinaryMatcher::defaultMedianFilterSize()
    {
        return 3;
    }
}
