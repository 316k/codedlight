/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEQ_BUFFER_H
#define SEQ_BUFFER_H

#include "generator.h"

namespace cl3ds
{

    /** A generator that stores sequence of image in memory. */
    class CL3DS_DECLSPEC SequenceBuffer : public Generator
    {
        public:
            /** Empty buffer. */
            SequenceBuffer();

            /** Buffer constructed from one image. */
            SequenceBuffer(const cv::Mat &image);

            /** Buffer constructed from a range of iterators of
             *  cv::Mat compatible objects. */
            template <typename Iterator>
            SequenceBuffer(Iterator begin,
                           Iterator end) :
                Generator(0)
            {
                init();
                resize(static_cast<size_t>(std::distance(begin, end)));
                size_t i = 0;
                for (Iterator it = begin; it != end; ++it, ++i)
                {
                    this->operator[](i) = *it;
                }
            }

            /** Stores all the pattern in memory for fast and persistent
             *  access. */
            SequenceBuffer(Generator *const gen,
                           size_t           count=0);

            ~SequenceBuffer();

            /** SequenceBuffer is the only generator that offers the possibility
             *  of storing patterns, rather than generating them on demand. */
            cv::Mat & operator[](size_t index);
            const cv::Mat & operator[](size_t index) const;

            /** Reserves more space in the buffer. */
            void resize(size_t size);

        private:
            void init();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("seqbuffer")
    };
}

#endif /* #ifndef SEQ_BUFFER_H */

