/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHASESHIFT_HPP
#define PHASESHIFT_HPP

#include "generator.h"
#include "matcher.h"

namespace cl3ds
{
    /** Implements phase shifted structured light patterns. */
    class CL3DS_DECLSPEC PhaseShiftGenerator : public Generator
    {
        public:
            struct CL3DS_DECLSPEC Generation
            {
                struct CL3DS_DECLSPEC Method
                {
                    enum Type { Default, Block, Sinusoidal };
                    Type type;
                };
                struct CL3DS_DECLSPEC Default : public Method
                {
                    Default();
                };
                struct CL3DS_DECLSPEC BlockModulation : public Method
                {
                    BlockModulation(double modPeriod=defaultModPeriod());
                    double modPeriod;
                    static double defaultModPeriod();
                };
                struct CL3DS_DECLSPEC SinusoidalModulation : public Method
                {
                    SinusoidalModulation(double modPeriod=defaultModPeriod());
                    double modPeriod;
                    static double defaultModPeriod();
                };
            };

            PhaseShiftGenerator(cv::Size                   size=defaultSize(),
                                Direction                  dir=defaultDirection(),
                                const std::vector<int>    &numShifts=defaultNumShifts(),
                                const std::vector<double> &periods=defaultPeriods(),
                                const Generation::Method  &type=
                                    defaultGenerationMethod());
            ~PhaseShiftGenerator();

            Direction direction() const;
            void setDirection(const Direction &dir);
            static Direction defaultDirection();

            cv::Size size() const;
            void setSize(cv::Size size);
            static cv::Size defaultSize();

            const std::vector<double> & periods() const;
            void setPeriods(const std::vector<double> &periods);
            static std::vector<double> defaultPeriods();

            const std::vector<int> & numShifts() const;
            void setNumShifts(const std::vector<int> &numShifts);
            static std::vector<int> defaultNumShifts();

            const Generation::Method & generationMethod() const;
            void setGenerationMethod(const Generation::Method &mode);
            static Generation::Default defaultGenerationMethod();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("phaseshift")
    };

    class CL3DS_DECLSPEC PhaseShiftMatcher : public Matcher
    {
        public:
            // unwrapping strategies :
            //
            // internal uses several (at least two) frequencies to unwrap
            // the lowest frequency should be low enough to cover all projector
            // pixel position
            //
            // external uses a map that provides absolute positions
            // the map should identify all projector pixel positions
            struct CL3DS_DECLSPEC Unwrapping
            {
                struct CL3DS_DECLSPEC Method
                {
                    enum Type { AllFrequencies, ExternalMap };
                    Type type;
                };
                struct CL3DS_DECLSPEC AllFrequencies : public Method
                {
                    AllFrequencies();
                };
                struct CL3DS_DECLSPEC ExternalMap : public Method
                {
                    ExternalMap(const std::string &path=defaultPath(),
                                int                phaseFreqId=
                                    defaultPhaseFrequencyId());

                    const std::string &path;
                    static const std::string & defaultPath();

                    int phaseFrequencyId;
                    static int defaultPhaseFrequencyId();
                };
            };

            PhaseShiftMatcher(const cv::Ptr<PhaseShiftGenerator> &gen=
                                  nonOwningPtr(defaultGenerator()),
                              const Unwrapping::Method           &type=
                                  defaultUnwrappingMethod());
            ~PhaseShiftMatcher();

            PhaseShiftGenerator * generator() const;
            void setGenerator(const cv::Ptr<PhaseShiftGenerator> &);
            static PhaseShiftGenerator * defaultGenerator();

            const Unwrapping::Method & unwrappingMethod() const;
            void setUnwrappingMethod(const Unwrapping::Method &type);
            static Unwrapping::AllFrequencies defaultUnwrappingMethod();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("phaseshift")
    };
}

#endif /* #ifndef PHASESHIFT_HPP */
