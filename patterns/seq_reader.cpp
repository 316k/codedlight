/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include "../core/parameter.h"
#include "generator_p.h"
#include "seq_reader.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct SequenceReader::Private : public AbstractPrivate
    {
        Private(SequenceReader *ptr,
                const string   &pathFormat,
                int             length,
                int             offset,
                int             step) :
            AbstractPrivate(ptr),
            gotImage(false)
        {
            paramPathFormat = TypeParameter<string>("pathFormat",
                                                    "String format used to determine the path of images to read",
                                                    SequenceReader::defaultPathFormat());
            paramPathFormat = pathFormat;

            paramLength = TypeParameter<int>("length",
                                             "Number of patterns to read",
                                             SequenceReader::defaultLength());
            paramLength = length;

            paramOffset = TypeParameter<int>("offset",
                                             "Offset from which to start in the format",
                                             SequenceReader::defaultOffset());
            paramOffset = offset;

            paramStep = TypeParameter<int>("step",
                                           "Steps between name of two images in the format",
                                           SequenceReader::defaultStep());
            paramStep = step;
        }
        ~Private();

        bool read(const FileNode &node)
        {
            paramPathFormat.read(node);
            paramLength.read(node);
            paramOffset.read(node);
            paramStep.read(node);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramPathFormat.write(fs);
            paramLength.write(fs);
            paramOffset.write(fs);
            paramStep.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            if (os)
            {
                *os << "Configuring Sequence Reader generator parameters." << endl;
            }
            paramPathFormat.configure(is, os);
            paramLength.configure(is, os);
            paramOffset.configure(is, os);
            paramStep.configure(is, os);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramPathFormat
                            << paramLength
                            << paramOffset
                            << paramStep;
        }

        void next()
        { }

        // reads first image and extract size and type
        void getTypeAndSize() const // this is needed to be called from size or type
        {
            size_t offset   = static_cast<size_t>(paramOffset);
            string form     = paramPathFormat;
            string filePath = format(form.c_str(), offset);
            Mat    pattern  = imread(filePath, -1);

            if (pattern.empty())
            {
                logError() << "File not found :" << filePath;
            }
            else
            {
                logDebug() << "Read file : " << filePath;
                if (!gotImage)
                {
                    imageSize = pattern.size();
                    imageType = pattern.type();
                    gotImage  = true;
                }
            }
        }

        Mat get()
        {
            size_t step     = static_cast<size_t>(paramStep);
            size_t offset   = static_cast<size_t>(paramOffset);
            string form     = paramPathFormat;
            string filePath = format(form.c_str(), pubPtr()->index() * step + offset);
            Mat    pattern  = imread(filePath, -1);

            if (pattern.empty())
            {
                logError() << "File not found :" << filePath;
            }
            else
            {
                logDebug() << "Read file : " << filePath;
                if (!gotImage)
                {
                    imageSize = pattern.size();
                    imageType = pattern.type();
                    gotImage  = true;
                }
            }

            return pattern;
        }

        void reset()
        {
            gotImage = false;
        }

        size_t count() const
        {
            return static_cast<size_t>(paramLength);
        }

        Size size() const
        {
            if (!gotImage) { getTypeAndSize(); }
            if (!gotImage)
            {
                logError() << "No image in the buffer, impossible " <<
                    "to determine the pattern size.";
            }

            return imageSize;
        }

        int type() const
        {
            if (!gotImage) { getTypeAndSize(); }
            if (!gotImage)
            {
                logError() << "No image in the buffer, impossible " <<
                    "to determine the pattern type.";
            }

            return imageType;
        }

        TypeParameter<string> paramPathFormat;
        TypeParameter<int>    paramLength;
        TypeParameter<int>    paramOffset;
        TypeParameter<int>    paramStep;

        mutable bool gotImage;
        mutable Size imageSize;
        mutable int  imageType;

        CL3DS_PUB_IMPL(SequenceReader)
    };

    SequenceReader::Private::~Private() { }

    SequenceReader::SequenceReader(const std::string &pathFormat,
                                   int                length,
                                   int                offset,
                                   int                step) :
        Generator(new Private(this, pathFormat, length, offset, step))
    { }

    SequenceReader::~SequenceReader() { }

    const string & SequenceReader::pathFormat() const
    {
        CL3DS_PRIV_CONST_PTR(SequenceReader);

        return priv->paramPathFormat;
    }

    void SequenceReader::setPathFormat(const string &pathFormat)
    {
        CL3DS_PRIV_PTR(SequenceReader);

        priv->paramPathFormat = pathFormat;
    }

    string SequenceReader::defaultPathFormat()
    {
        return "./img_%04d.png";
    }

    int SequenceReader::length() const
    {
        CL3DS_PRIV_CONST_PTR(SequenceReader);

        return priv->paramLength;
    }

    void SequenceReader::setLength(int length)
    {
        CL3DS_PRIV_PTR(SequenceReader);

        priv->paramLength = length;
    }

    int SequenceReader::defaultLength()
    {
        return 0;
    }

    int SequenceReader::offset() const
    {
        CL3DS_PRIV_CONST_PTR(SequenceReader);

        return priv->paramOffset;
    }

    void SequenceReader::setOffset(int offset)
    {
        CL3DS_PRIV_PTR(SequenceReader);

        priv->paramOffset = offset;
    }

    int SequenceReader::defaultOffset()
    {
        return 0;
    }

    int SequenceReader::step() const
    {
        CL3DS_PRIV_CONST_PTR(SequenceReader);

        return priv->paramStep;
    }

    void SequenceReader::setStep(int step)
    {
        CL3DS_PRIV_PTR(SequenceReader);

        priv->paramStep = step;
    }

    int SequenceReader::defaultStep()
    {
        return 1;
    }

}
