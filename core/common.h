/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This header file defines several functions that are used in the rest of the
 * code. It also declares some useful classes like RNGBit or ElapsedTimer.
 */

#ifndef COMMON_HPP
#define COMMON_HPP

#include <cctype>
#include <limits>
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif

#ifdef _MSC_VER
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#include <time.h>
#endif

#include "config.h"
#include "export.h"
#include "foreach.h"
#include "typelist.h"

namespace cl3ds
{
    /** From qobject.h : silences -Wunused-variables. */
#define CL3DS_UNUSED(v) (void)v

    CL3DS_DECLSPEC double log2(double n);

    /** Convert from and to 16 bit color coded match to double
     *  precision floating point pixel coordinates. */
    CL3DS_DECLSPEC void pointFromMatch(const cv::Vec3w &color,
                                       cv::Size         size,
                                       double          &x,
                                       double          &y);
    CL3DS_DECLSPEC void pointToMatch(double     x,
                                     double     y,
                                     cv::Size   size,
                                     cv::Vec3w &color);

    /** Convert a 16 bit color coded match map to two regular grids
     *  of correspondences of same size as match size. camPoints
     *  is basically a grid of (x+0.5, y+0.5) for each x,y and
     *  projPoints is the grid of corresponding points in the projector. */
    CL3DS_DECLSPEC void pointsFromMatchMap(const cv::Mat  &match,
                                           const cv::Size &projSize,
                                           cv::Mat        &camPoints,
                                           cv::Mat        &projPoints);

    // Convenience algorithms
    template <typename T> inline int sign(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    template <class T> const T & max3(const T &a,
                                      const T &b,
                                      const T &c)
    {
        return std::max(a, std::max(b, c));
    }

    template <class T> const T & min3(const T &a,
                                      const T &b,
                                      const T &c)
    {
        return std::min(a, std::min(b, c));
    }

    // Computes the median of a sequence of values between two iterators
    // use with median(v.begin(), v.end());
    template <class RanIter>
    typename std::iterator_traits<RanIter>::value_type median(RanIter begin,
                                                              RanIter end)
    {
        typedef typename std::iterator_traits<RanIter>::value_type      val_t;
        typedef typename std::iterator_traits<RanIter>::difference_type diff_t;

        diff_t count = std::distance(begin, end);

        RanIter mid = begin + count / 2;
        std::nth_element(begin, mid, end);

        val_t ret;
        if (count % 2 != 0)   // Odd number of elements
        {
            ret = *mid;
        }
        else                  // Even number of elements
        {
            RanIter midNeighbor = std::max_element(begin, mid);
            ret = static_cast<val_t>((*mid + *midNeighbor) / 2.0);
        }

        return ret;
    }

    // Computes the MAD of a sequence of values between two iterators
    // use with median(v.begin(), v.end());
    template <class RanIter>
    typename std::iterator_traits<RanIter>::value_type median_abs_dev(
        RanIter                                            begin,
        RanIter                                            end,
        typename std::iterator_traits<RanIter>::value_type med)
    {
        for (RanIter it = begin; it != end; ++it)
        {
            *it = std::abs(*it - med);
        }

        return median(begin, end);
    }

    // These are input and ouput stream operators for a few useful types
    template <class T>
    std::ostream & operator<<(std::ostream         &os,
                              const std::vector<T> &v)
    {
        os << "[ ";
        for (size_t i = 0; i < v.size() - 1; ++i)
        {
            os << v[i] << ", ";
        }
        os << v[v.size() - 1];
        os << " ]";

        return os;
    }

    template <class T>
    std::istream & operator>>(std::istream   &is,
                              std::vector<T> &v)
    {
        int count;
        is >> count;
        v.resize(static_cast<size_t>(count));
        for (int i = 0; i < count; ++i)
        {
            is >> v[static_cast<size_t>(i)];
        }

        return is;
    }

    inline std::ostream & operator<<(std::ostream   &os,
                                     const cv::Size &s)
    {
        os << s.width << "x" << s.height;

        return os;
    }

    inline std::istream & operator>>(std::istream &is,
                                     cv::Size     &s)
    {
        is >> s.width;
        int c = is.peek();
        while (!std::isdigit(c))
        {
            is.get();
            c = is.peek();
        }
        is >> s.height;

        return is;
    }

    inline std::istream & operator>>(std::istream &is,
                                     cv::Scalar   &s)
    {
        for (int i = 0; i < 4; ++i)
        {
            is >> s[i];
        }

        return is;
    }

    inline std::ostream & operator<<(std::ostream   &os,
                                     const cv::Rect &s)
    {
        os << s.width << "x" << s.height << "+" << s.x << "," << s.y;

        return os;
    }

    inline std::istream & operator>>(std::istream &is,
                                     cv::Rect     &s)
    {
        int vals[4];
        for (int i = 0; i < 4; ++i)
        {
            int c = is.peek();
            while (!std::isdigit(c))
            {
                is.get();
                c = is.peek();
            }
            is >> vals[i];
        }
        s.width  = vals[0];
        s.height = vals[1];
        s.x      = vals[2];
        s.y      = vals[3];

        return is;
    }

    inline std::istream & operator>>(std::istream &is,
                                     cv::Mat      &m)
    {
        CL3DS_UNUSED(m);
        std::cerr << "operator>>(istream, Mat) not implemented yet" <<
            std::endl;

        return is;
    }

    // Creates a Ptr<T> to an instance the smart pointer does not manage.
    // It can be useful when passing a pointer to a stack allocated object.
    template <class T>
    cv::Ptr<T> nonOwningPtr(T *ptr)
    {
#if CV_MAJOR_VERSION < 3
        // non owning pointer is just a smart pointer with a counter that never
        // reaches 0..
        cv::Ptr<T> p(ptr);
        p.addref();
#else
        // non owning pointer introduced in 3.0
        cv::Ptr<T> p(cv::Ptr<T>(), ptr);
#endif

        return p;
    }

    // Convenience wrapper around std::ostringstream
    template <class T>
    std::string toString(const T &t)
    {
        std::ostringstream oss;
        oss << t;

        return oss.str();
    }

    // Convenience wrapper around std::istringstream
    template <class T>
    T fromString(const std::string &s)
    {
        T                  t;
        std::istringstream iss(s);
        iss >> t;

        return t;
    }

    // Convenience wrapper for creating a vector from a static array
    // must take argument by reference, to avoid parameter decay
    template <class T, int N>
    std::vector<T> makeVector(const T(&data)[N])
    {
        return std::vector<T>(data, data + N);
    }

    // Puts the current thread to sleep for at least milliseconds
    void CL3DS_DECLSPEC sleep_for(unsigned int milliseconds);

    /** Random Number Generation object that generates bits. */
    class CL3DS_DECLSPEC RNGBit
    {
        public:
            /** Constructs a new random bit generator with given seed. */
            RNGBit(uint64 seed=0);
            RNGBit(const RNGBit &rng);
            RNGBit & operator=(const RNGBit &rng);
            ~RNGBit();

            /** Generates a random bit. */
            bool operator()();

        private:
            struct Private;
            Private *m_privImpl;
    };

    /** Coarse implementation of a timer that computes delta times. */
    struct CL3DS_DECLSPEC ElapsedTimer
    {
        public:
            ElapsedTimer();

            /** Restarts the timer, all delta times will now be computed from
             *  this time. */
            void restart();

            /** Computes a delta time since last time the timer was restarted. */
            double elapsed() const;

        private:
            double m_start;
    };

    /** Convenient dynamic_cast for cv::Ptr which was not available in OpenCV 2.0 */
    template <class T, class U>
    cv::Ptr<T> dynamicPtrCast(cv::Ptr<U> u)
    {
        cv::Ptr<T> ptr;
#if CV_MAJOR_VERSION < 3
        ptr = cv::Ptr<T>(u);
#else
        ptr = u.template dynamicCast<T>();
#endif

        return ptr;
    }

    /** Convenient static_cast for cv::Ptr which was not available in OpenCV 2.0 */
    template <class T, class U>
    cv::Ptr<T> staticPtrCast(cv::Ptr<U> u)
    {
        cv::Ptr<T> ptr;
#if CV_MAJOR_VERSION < 3
        ptr = cv::Ptr<T>(u);
#else
        ptr = u.template staticCast<T>();
#endif

        return ptr;
    }
}

/** Provides functions that are not available in OpenCV 2.x to make the code
 *  compile without changes.  */
#if CV_MAJOR_VERSION < 3
namespace cv
{
    template<typename T>
    Ptr<T> makePtr()
    {
        return Ptr<T>(new T());
    }

    template<typename T, typename A1>
    Ptr<T> makePtr(const A1 &a1)
    {
        return Ptr<T>(new T(a1));
    }

    template<typename T, typename A1, typename A2>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2)
    {
        return Ptr<T>(new T(a1, a2));
    }

    template<typename T, typename A1, typename A2, typename A3>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2,
                   const A3 &a3)
    {
        return Ptr<T>(new T(a1, a2, a3));
    }

    template<typename T, typename A1, typename A2, typename A3, typename A4>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2,
                   const A3 &a3,
                   const A4 &a4)
    {
        return Ptr<T>(new T(a1, a2, a3, a4));
    }

    template<typename T, typename A1, typename A2, typename A3, typename A4,
             typename A5>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2,
                   const A3 &a3,
                   const A4 &a4,
                   const A5 &a5)
    {
        return Ptr<T>(new T(a1, a2, a3, a4, a5));
    }

    template<typename T, typename A1, typename A2, typename A3, typename A4,
             typename A5, typename A6>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2,
                   const A3 &a3,
                   const A4 &a4,
                   const A5 &a5,
                   const A6 &a6)
    {
        return Ptr<T>(new T(a1, a2, a3, a4, a5, a6));
    }

    template<typename T, typename A1, typename A2, typename A3, typename A4,
             typename A5, typename A6, typename A7>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2,
                   const A3 &a3,
                   const A4 &a4,
                   const A5 &a5,
                   const A6 &a6,
                   const A7 &a7)
    {
        return Ptr<T>(new T(a1, a2, a3, a4, a5, a6, a7));
    }

    template<typename T, typename A1, typename A2, typename A3, typename A4,
             typename A5, typename A6, typename A7, typename A8>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2,
                   const A3 &a3,
                   const A4 &a4,
                   const A5 &a5,
                   const A6 &a6,
                   const A7 &a7,
                   const A8 &a8)
    {
        return Ptr<T>(new T(a1, a2, a3, a4, a5, a6, a7, a8));
    }

    template<typename T, typename A1, typename A2, typename A3, typename A4,
             typename A5, typename A6, typename A7, typename A8, typename A9>
    Ptr<T> makePtr(const A1 &a1,
                   const A2 &a2,
                   const A3 &a3,
                   const A4 &a4,
                   const A5 &a5,
                   const A6 &a6,
                   const A7 &a7,
                   const A8 &a8,
                   const A9 &a9)
    {
        return Ptr<T>(new T(a1, a2, a3, a4, a5, a6, a7, a8, a9));
    }

    template<typename T, typename A1, typename A2, typename A3, typename A4,
             typename A5, typename A6, typename A7, typename A8, typename A9,
             typename A10>
    Ptr<T> makePtr(const A1  &a1,
                   const A2  &a2,
                   const A3  &a3,
                   const A4  &a4,
                   const A5  &a5,
                   const A6  &a6,
                   const A7  &a7,
                   const A8  &a8,
                   const A9  &a9,
                   const A10 &a10)
    {
        return Ptr<T>(new T(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10));
    }
}

#define CAP_PROP_TRIGGER CV_CAP_PROP_TRIGGER
#define CAP_PROP_FRAME_WIDTH CV_CAP_PROP_FRAME_WIDTH
#define CAP_PROP_FRAME_HEIGHT CV_CAP_PROP_FRAME_HEIGHT
#define CAP_PROP_EXPOSURE CV_CAP_PROP_EXPOSURE
#define CAP_PROP_FPS CV_CAP_PROP_FPS
#define CAP_PROP_PVAPI_MULTICASTIP CV_CAP_PROP_PVAPI_MULTICASTIP
#define CAP_PROP_GAIN CV_CAP_PROP_GAIN
#define CAP_PROP_FORMAT CV_CAP_PROP_FORMAT
#endif

#endif /* #ifndef COMMON_HPP */
