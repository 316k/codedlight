/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASHER_H
#define HASHER_H

#include <mvg/sfinae.h>
#include <cstdio>
#include "common.h"

#if defined FUNCTIONAL_IN_STD_HEADER
#include <functional>
#else
#include <tr1/functional>
#endif

namespace cl3ds
{
    template <class T, class test = void> struct MagicShuffleNumber { };

    // SFINAE to avoid warning with 64 bit value on 32 bit system
    template <class T>
    struct MagicShuffleNumber<T, typename EnableIfCond<sizeof(T) == 8>::type >
    {
        static const size_t value = 0x278dde6e5fd29e00;
    };

    template <class T>
    struct MagicShuffleNumber<T, typename EnableIfCond<sizeof(T) == 4>::type >
    {
        static const size_t value = 0x9e3779b9;
    };

    template <typename T> class TypeParameter;

    /** Hasher class that can produce hash out of every structure used in CodedLight.
     *  It is a wrapper around std::hash for custom types. */
    class CL3DS_DECLSPEC Hasher
    {
        public:
            Hasher() : m_value(0)
            { }

            template <class T>
            Hasher & operator<<(const T &t)
            {
                hash_combine(m_value, t);

                return *this;
            }

            Hasher & operator<<(const cv::Rect &r)
            {
                hash_combine(m_value, r.x);
                hash_combine(m_value, r.y);
                hash_combine(m_value, r.width);
                hash_combine(m_value, r.height);

                return *this;
            }

            Hasher & operator<<(const cv::Size &s)
            {
                hash_combine(m_value, s.width);
                hash_combine(m_value, s.height);

                return *this;
            }

            Hasher & operator<<(const cv::Mat &m)
            {
                for (int j = 0; j < m.rows; ++j)
                {
                    size_t        cols  = static_cast<size_t>(m.cols);
                    size_t        size  =  cols * m.elemSize();
                    int           p     = 0;
                    const size_t *ptr   = m.ptr<size_t>(j);
                    size_t        count = size / sizeof(size_t);
                    for (size_t i = 0; i < count; ++i, ++p)
                    {
                        hash_combine(m_value, ptr[i]);
                    }
                    const uchar *ptr2 =
                        reinterpret_cast<const uchar *>(ptr + count);
                    int rem = size % sizeof(size_t);
                    for (int i = 0; i < rem; ++i)
                    {
                        hash_combine(m_value, ptr2[i]);
                    }
                }

                return *this;
            }

            template <typename T>
            Hasher & operator<<(const TypeParameter<T> &t)
            {
                return this->operator<<(T(t));
            }

            template <typename T>
            Hasher & operator<<(const std::vector<T> &v)
            {
                size_t        size  = v.size() * sizeof(T);
                const size_t *ptr   = reinterpret_cast<const size_t *>(&v[0]);
                size_t        count = size / sizeof(size_t);
                for (size_t i = 0; i < count; ++i)
                {
                    hash_combine(m_value, ptr[i]);
                }
                const uchar *ptr2 =
                    reinterpret_cast<const uchar *>(ptr + count);
                size_t rem = size % sizeof(size_t);
                for (size_t i = 0; i < rem; ++i)
                {
                    hash_combine(m_value, ptr2[i]);
                }

                return *this;
            }

            Hasher & operator<<(const cv::Scalar &s)
            {
                for (int i = 0; i < 4; ++i)
                {
                    hash_combine(m_value, s[i]);
                }

                return *this;
            }

            operator size_t() const { return m_value; }

        private:
            template <class T>
            inline void hash_combine(std::size_t &seed,
                                     const T     &v)
            {
                static const size_t shuffleValue =
                    MagicShuffleNumber<size_t>::value;
#if defined HASH_IN_STD_NAMESPACE
                std::hash<T> hasher;
#else
                std::tr1::hash<T> hasher;
#endif
                seed ^= hasher(v) + shuffleValue + (seed << 6) + (seed >> 2);
            }

            size_t m_value;
    };
}

#endif // HASHER_H
