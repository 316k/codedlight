/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mvg/logger.h>
#include "hasher.h"
#include "object.h"
#include "object_p.h"

using namespace std;
using namespace cv;

namespace cl3ds
{

#define PRIV_PTR Private * const priv = \
    static_cast<Private *>(static_cast<AbstractPrivate *>(m_privImpl));
#define PRIV_CONST_PTR const Private * const priv = \
    static_cast<const Private *>(static_cast<const AbstractPrivate *>(m_privImpl));

    struct Object::Private : public Object::AbstractPrivate
    {
        Private(Object          *pubImpl,
                AbstractPrivate *privImpl) :
            Object::AbstractPrivate(pubImpl),
            impl(privImpl)
        { }
        ~Private();

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Object constructor";
            }
        }

        bool read(const cv::FileNode &node)
        {
            checkImplPtr();

            // we may add invariants here
            if (!impl->read(node)) { return false; }
            // and there

            return true;
        }

        bool write(cv::FileStorage &fs) const
        {
            checkImplPtr();

            // we may add invariants here
            fs << "{";
            if (!impl->write(fs)) { return false; }
            // and there
            fs << "}";

            return true;
        }

        bool configure(std::istream *is,
                       std::ostream *os)
        {
            checkImplPtr();

            // we may add invariants here
            if (!impl->configure(is, os)) { return false; }
            // and there

            return true;
        }

        size_t hash() const
        {
            checkImplPtr();

            return Hasher() << m_pubImpl->objectName()
                            << impl->hash();
        }

        string           configFilePath;
        string           configNodeName;
        AbstractPrivate *impl;
    };

    Object::Private::~Private()
    {
        delete impl;
    }

    Object::Object(AbstractPrivate *ptr) :
        m_privImpl(new Private(this, ptr))
    { }

    Object::~Object()
    {
        delete m_privImpl;
    }

    void Object::setPrivateImplementation(AbstractPrivate *ptr)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = ptr;
    }

    Object::AbstractPrivate * Object::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const Object::AbstractPrivate * Object::privateImplementation() const
    {
        PRIV_CONST_PTR;

        return priv->impl;
    }

    bool Object::read(const cv::FileNode &node)
    {
        PRIV_PTR;

        return priv->read(node);
    }

    bool Object::write(cv::FileStorage &fs) const
    {
        PRIV_CONST_PTR;

        return priv->write(fs);
    }

    bool Object::configure(std::istream *is,
                           std::ostream *os)
    {
        PRIV_PTR;

        return priv->configure(is, os);
    }

    size_t Object::hash() const
    {
        PRIV_CONST_PTR;

        return priv->hash();
    }

    const string & Object::configFilePath() const
    {
        PRIV_CONST_PTR;

        return priv->configFilePath;
    }

    void Object::setConfigFilePath(const string &configFilePath)
    {
        PRIV_PTR;

        priv->configFilePath = configFilePath;
    }

    const string & Object::configNodeName() const
    {
        PRIV_CONST_PTR;

        return priv->configNodeName;
    }

    void Object::setConfigNodeName(const string &configNodeName)
    {
        PRIV_PTR;

        priv->configNodeName = configNodeName;
    }

    Object::AbstractPrivate::~AbstractPrivate()
    { }

    Object::AbstractPrivate::AbstractPrivate(Object *ptr) :
        m_pubImpl(ptr)
    { }
}
