/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAT_TABLE_H
#define MAT_TABLE_H

#include <map>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <mvg/logger.h>
#include "export.h"

namespace cl3ds
{
    /** This represents an associative array of matrices, which can be
     * used to store several results of algorithms. The correspondence
     * maps are stored in cam/proj MatchMat for example. */
    class CL3DS_DECLSPEC MatTable
    {
        public:
            MatTable();
            MatTable(const MatTable &rng);
            MatTable & operator=(const MatTable &rng);
            ~MatTable();

            // easier and cleaner access to most used mat
            // all const functions will crash if called with
            // a name that has not yet been registered
            const cv::Mat & camMatchMat() const;
            cv::Mat & camMatchMat();

            const cv::Mat & camHorizontalCodes() const;
            cv::Mat & camHorizontalCodes();

            const cv::Mat & camVerticalCodes() const;
            cv::Mat & camVerticalCodes();

            const cv::Mat & projMatchMat() const;
            cv::Mat & projMatchMat();

            const cv::Mat & projHorizontalCodes() const;
            cv::Mat & projHorizontalCodes();

            const cv::Mat & projVerticalCodes() const;
            cv::Mat & projVerticalCodes();

            const cv::Mat & operator[](const std::string &name) const;
            cv::Mat & operator[](const std::string &name);

            /** Returns all the names of the registered matrices. */
            std::vector<std::string> names() const;

        private:
            // will eventually disappear in future releases
            const cv::Mat & namedMat(const std::string &name) const;
            cv::Mat & namedMat(const std::string &name);

            struct Private;
            Private *m_privImpl;
    };
}
#endif
