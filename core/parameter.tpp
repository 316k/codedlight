/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETER_TPP
#define PARAMETER_TPP

#include <opencv2/core/core_c.h> // required for cvWriteComment
#include <mvg/logger.h>

namespace cl3ds
{
    namespace internal
    {
        template <typename V> struct Type2Id { };
        template <> struct Type2Id<bool>{ enum { value = 0 }; };
        template <> struct Type2Id<int>{ enum { value = 1 }; };
        template <> struct Type2Id<unsigned int>{ enum { value = 2 }; };
        template <> struct Type2Id<float>{ enum { value = 3 }; };
        template <> struct Type2Id<double>{ enum { value = 4 }; };
        template <> struct Type2Id<std::string>{ enum { value = 5 }; };
        template <> struct Type2Id<cv::Size>{ enum { value = 6 }; };
        template <> struct Type2Id<cv::Rect>{ enum { value = 7 }; };
        template <> struct Type2Id< std::vector<int> >{ enum { value = 8 }; };
        template <> struct Type2Id< std::vector<double> >{ enum { value = 9 }; };
        template <> struct Type2Id<cv::Mat>{ enum { value = 10 }; };
        template <> struct Type2Id<cv::Scalar>{ enum { value = 11 }; };
    }

    template <class T>
    ParameterConstraint<T>::~ParameterConstraint()
    { }

    template <class T, class Comparator>
    BoundConstraint<T, Comparator>::BoundConstraint(const T &min,
                                                    const T &max) :
        m_min(min),
        m_max(max),
        m_comp(Comparator(min, max))
    { }

    template <class T>
    PositiveConstraint<T>::PositiveConstraint(const T &max) :
        BoundConstraint<T>(0, max)
    { }

    template <class T>
    struct BoundComparator
    {
        BoundComparator(const T &min,
                        const T &max) :
            m_min(min),
            m_max(max)
        { }

        bool operator()(const T &t) const
        {
            return t > m_min && t < m_max;
        }
        T m_min, m_max;
    };

    template <>
    struct BoundComparator<cv::Size>
    {
        BoundComparator(const cv::Size &min,
                        const cv::Size &max) :
            m_min(min),
            m_max(max)
        { }
        bool operator()(const cv::Size &t) const
        {
            if ((t.width < m_min.width) || (t.width > m_max.width))
            {
                return false;
            }
            if ((t.height < m_min.height) ||
                (t.height > m_max.height))
            {
                return false;
            }

            return true;
        }
        cv::Size m_min, m_max;
    };

    template <class T>
    struct BoundComparator< std::vector<T> >
    {
        BoundComparator(const T &min,
                        const T &max) :
            m_min(min),
            m_max(max)
        { }
        bool operator()(const std::vector<T> &t) const
        {
            for (size_t i = 0; i < t.size(); ++i)
            {
                if ((t[i] < m_min) || (t[i] > m_max))
                {
                    return false;
                }
            }

            return true;
        }
        T m_min, m_max;
    };

    template <class T, class Comparator>
    bool BoundConstraint<T, Comparator>::validate(const T &t) const
    {
        return m_comp(t);
    }

    template <class T, class Comparator>
    std::string BoundConstraint<T, Comparator>::description() const
    {
        std::ostringstream oss;
        oss << "Range : [ " << m_min << " , " << m_max << " ] ";

        return oss.str();
    }

    template <class T>
    class BoundConstraint<std::vector<T>, BoundComparator< std::vector<T> > > :
        public ParameterConstraint< std::vector<T> >
    {
        public:
            BoundConstraint(const T &min,
                            const T &max) :
                m_min(min),
                m_max(max),
                m_comp(BoundComparator< std::vector<T> >(min, max))
            { }

            virtual bool validate(const std::vector<T> &t) const
            {
                return m_comp(t);
            }

            virtual std::string description() const
            {
                std::ostringstream oss;
                oss << "Range : [ " << m_min << " , " << m_max << " ] ";

                return oss.str();
            }

        protected:
            T                                 m_min;
            T                                 m_max;
            BoundComparator< std::vector<T> > m_comp;
    };

    template <class T>
    EnumerationConstraint<T>::EnumerationConstraint(const std::vector<T> &values) :
        m_values(values)
    { }

    template <class T>
    bool EnumerationConstraint<T>::validate(const T &t) const
    {
        return find(m_values.begin(), m_values.end(), t) != m_values.end();
    }

    template <class T>
    std::string EnumerationConstraint<T>::description() const
    {
        std::ostringstream oss;
        oss << "Valid values : [ ";
        const size_t count = m_values.size();
        oss << m_values[0];
        for (size_t i = 1; i < count; ++i)
        {
            oss << ", " << m_values[i];
        }
        oss << " ] ";

        return oss.str();
    }

    template <class T>
    TypeParameter<T>::TypeParameter(const std::string                &n,
                                    const std::string                &d,
                                    const T                          &v,
                                    cv::Ptr< ParameterConstraint<T> > c) :
        name(n),
        desc(d),
        def(v),
        constr(c)
    {
        if (!constr.empty() && !constr->validate(def))
        {
            logError() << "Default value" << def <<
                "is invalid with respect to constraint given";
        }
    }

    template <class T>
    std::string TypeParameter<T>::type() const
    {
        static const std::string types[] =
        {
            "bool",
            "int",
            "unsigned int",
            "float",
            "double",
            "string",
            "size",
            "rect",
            "vector of ints",
            "vector of doubles",
            "mat",
            "scalar"
        };

        return types[internal::Type2Id < T > ::value];
    }

    template <class T>
    std::string TypeParameter<T>::defaultValue() const
    {
        return toString(def);
    }

    template <class T>
    std::string TypeParameter<T>::description() const
    {
        std::ostringstream oss;
        if (!constr.empty()) { oss << constr->description() << "\n"; }
        oss << "Description : " << desc << "\n";
        oss << "Default value : " << def;

        return oss.str();
    }

    template <class T>
    TypeParameter<T> &TypeParameter<T>::operator=(const T &val)
    {
        if (constr)
        {
            if (constr->validate(val))
            {
                m_value = val;
            }
            else
            {
                logWarning() << "Value for parameter" << name <<
                    "is invalid, changing to default value" << def;
                m_value = def;
            }
        }
        else
        {
            m_value = val;
        }

        return *this;
    }

    template <class T>
    TypeParameter<T>::operator const T &() const
    {
        return m_value;
    }

    template <class T>
    TypeParameter<T> &TypeParameter<T>::read(const cv::FileNode &node)
    {
        T            var = def;
        cv::FileNode n   = node[name];
        if (n.empty())
        {
            logWarning() << "Parameter" << name << "was not found";
        }
        else
        {
            n >> var;
            if (constr && !constr->validate(var))
            {
                logWarning() << "Value for parameter" << name <<
                    "is invalid, changing to default value" << def;
                var = def;
            }
        }
        m_value = var;

        return *this;
    }

    template <class T>
    void TypeParameter<T>::write(cv::FileStorage &fs) const
    {
        if (constr && !constr->validate(m_value))
        {
            logWarning() << "Parameter" << name << "is invalid";
        }
        cvWriteComment(*fs, longDescription().c_str(), 0);
        fs << name << m_value;
    }

    template <class T>
    TypeParameter<T> &TypeParameter<T>::configure(std::istream *in,
                                                  std::ostream *out)
    {
        T var = def;
        if (out)
        {
            *out << longDescription();
        }
        if (in)
        {
            for (;;)
            {
                *in >> var;
                if (constr && !constr->validate(var))
                {
                    if (out)
                    {
                        *out << "Parameter with value " << var <<
                            " is invalid, retrying." << std::endl;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        m_value = var;

        return *this;
    }

}

#endif // PARAMETER_TPP
