/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPELIST_HPP
#define TYPELIST_HPP

namespace cl3ds
{

    /** The typelist is presented in MC++ by Alexandrescu. */

    // to stop recursion
    struct NullType { };

    template <class H, class T>
    struct TypeList
    {
        typedef H Head;
        typedef T Tail;
    };

    /** Macros to construct TypeList of given size. */

    /* *INDENT-OFF* */
#define TYPELIST_1(T1) TypeList<T1, NullType>
#define TYPELIST_2(T1, T2)\
    TypeList<T1, TYPELIST_1(T2) >
#define TYPELIST_3(T1, T2, T3)\
    TypeList<T1, TYPELIST_2(T2, T3) >
#define TYPELIST_4(T1, T2, T3, T4)\
    TypeList<T1, TYPELIST_3(T2, T3, T4) >
#define TYPELIST_5(T1, T2, T3, T4, T5)\
    TypeList<T1, TYPELIST_4(T2, T3, T4, T5) >
#define TYPELIST_6(T1, T2, T3, T4, T5, T6)\
    TypeList<T1, TYPELIST_5(T2, T3, T4, T5, T6) >
#define TYPELIST_7(T1, T2, T3, T4, T5, T6, T7)\
    TypeList<T1, TYPELIST_6(T2, T3, T4, T5, T6, T7) >
#define TYPELIST_8(T1, T2, T3, T4, T5, T6, T7, T8)\
    TypeList<T1, TYPELIST_7(T2, T3, T4, T5, T6, T7, T8) >
#define TYPELIST_9(T1, T2, T3, T4, T5, T6, T7, T8, T9)\
    TypeList<T1, TYPELIST_8(T2, T3, T4, T5, T6, T7, T8, T9) >
#define TYPELIST_10(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)\
    TypeList<T1, TYPELIST_9(T2, T3, T4, T5, T6, T7, T8, T9, T10) >
#define TYPELIST_11(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)\
    TypeList<T1, TYPELIST_10(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11) >
#define TYPELIST_12(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12)\
    TypeList<T1, TYPELIST_11(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12) >
#define TYPELIST_13(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13)\
    TypeList<T1, TYPELIST_12(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13) >
#define TYPELIST_14(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14)\
    TypeList<T1, TYPELIST_13(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14) >
#define TYPELIST_15(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15)\
    TypeList<T1, TYPELIST_14(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15) >
#define TYPELIST_16(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16)\
    TypeList<T1, TYPELIST_15(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16) >
#define TYPELIST_17(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17)\
    TypeList<T1, TYPELIST_16(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17) >
#define TYPELIST_18(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18)\
    TypeList<T1, TYPELIST_17(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18) >
#define TYPELIST_19(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19)\
    TypeList<T1, TYPELIST_18(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19) >
#define TYPELIST_20(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20)\
    TypeList<T1, TYPELIST_19(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20) >
#define TYPELIST_21(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21)\
    TypeList<T1, TYPELIST_20(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21) >
#define TYPELIST_22(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22)\
    TypeList<T1, TYPELIST_21(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22) >
#define TYPELIST_23(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23)\
    TypeList<T1, TYPELIST_22(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23) >
#define TYPELIST_24(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24)\
    TypeList<T1, TYPELIST_23(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24) >
#define TYPELIST_25(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25)\
    TypeList<T1, TYPELIST_24(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25) >
#define TYPELIST_26(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26)\
    TypeList<T1, TYPELIST_25(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26) >
#define TYPELIST_27(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27)\
    TypeList<T1, TYPELIST_26(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27) >
#define TYPELIST_28(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27, T28)\
    TypeList<T1, TYPELIST_27(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27, T28) >
#define TYPELIST_29(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27, T28, T29)\
    TypeList<T1, TYPELIST_28(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27, T28, T29) >
#define TYPELIST_30(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27, T28, T29, T30)\
    TypeList<T1, TYPELIST_29(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, \
    T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, \
    T26, T27, T28, T29, T30)
    /* *INDENT-ON* */

    /** Append is used to create a TypeList through insertion at compile-time. */
    template <class TList, class T> struct Append;

    template <> struct Append<NullType, NullType>
    {
        typedef NullType Result;
    };

    template <class T> struct Append<NullType, T>
    {
        typedef TYPELIST_1 (T) Result;
    };

    template <class Head, class Tail>
    struct Append<NullType, TypeList<Head, Tail> >
    {
        typedef TypeList<Head, Tail> Result;
    };

    template <class Head, class Tail, class T>
    struct Append<TypeList<Head, Tail>, T>
    {
        typedef TypeList<Head,
                         typename Append<Tail, T>::Result> Result;
    };

    /** Length is used to get the length of a TypeList at compile-time. */
    template <class TList> struct Length;
    template <> struct Length<NullType>
    {
        enum { value = 0 };
    };
    template <class T, class U>
    struct Length< TypeList<T, U> >
    {
        enum { value = 1 + Length<U>::value };
    };

    /** MatType2Int converts types to opencv's mat type. */

    /* *INDENT-OFF* */
    template <typename V> struct MatType2Int {};
    template <> struct MatType2Int<uchar> { enum { value = CV_8U }; };
    template <> struct MatType2Int<ushort> { enum { value = CV_16U }; };
    template <> struct MatType2Int<char> { enum { value = CV_8S }; };
    template <> struct MatType2Int<short> { enum { value = CV_16S }; };
    template <> struct MatType2Int<int> { enum { value = CV_32S }; };
    template <> struct MatType2Int<float> { enum { value = CV_32F }; };
    template <> struct MatType2Int<double> { enum { value = CV_64F }; };
    template <> struct MatType2Int<cv::Vec2b> { enum { value = CV_8UC2 }; };
    template <> struct MatType2Int<cv::Vec2w> { enum { value = CV_16UC2 }; };
    template <> struct MatType2Int<cv::Vec<char, 2> > { enum { value = CV_8SC2 }; };
    template <> struct MatType2Int<cv::Vec2s> { enum { value = CV_16SC2 }; };
    template <> struct MatType2Int<cv::Vec2i> { enum { value = CV_32SC2 }; };
    template <> struct MatType2Int<cv::Vec2f> { enum { value = CV_32FC2 }; };
    template <> struct MatType2Int<cv::Vec2d> { enum { value = CV_64FC2 }; };
    template <> struct MatType2Int<cv::Vec3b> { enum { value = CV_8UC3 }; };
    template <> struct MatType2Int<cv::Vec3w> { enum { value = CV_16UC3 }; };
    template <> struct MatType2Int<cv::Vec<char, 3> > { enum { value = CV_8SC3 }; };
    template <> struct MatType2Int<cv::Vec3s> { enum { value = CV_16SC3 }; };
    template <> struct MatType2Int<cv::Vec3i> { enum { value = CV_32SC3 }; };
    template <> struct MatType2Int<cv::Vec3f> { enum { value = CV_32FC3 }; };
    template <> struct MatType2Int<cv::Vec3d> { enum { value = CV_64FC3 }; };
    template <> struct MatType2Int<cv::Vec4b> { enum { value = CV_8UC4 }; };
    template <> struct MatType2Int<cv::Vec4w> { enum { value = CV_16UC4 }; };
    template <> struct MatType2Int<cv::Vec<char, 4> > { enum { value = CV_8SC4 }; };
    template <> struct MatType2Int<cv::Vec4s> { enum { value = CV_16SC4 }; };
    template <> struct MatType2Int<cv::Vec4i> { enum { value = CV_32SC4 }; };
    template <> struct MatType2Int<cv::Vec4f> { enum { value = CV_32FC4 }; };
    template <> struct MatType2Int<cv::Vec4d> { enum { value = CV_64FC4 }; };
    /* *INDENT-ON* */

#if 0
    typedef TYPELIST_28 (uchar, ushort, char, short, int, float, double,
                         cv::Vec2b, cv::Vec2w, cv::Vec<2, char>, cv::Vec2s,
                         cv::Vec2i, cv::Vec2f, cv::Vec2d,
                         cv::Vec3b, cv::Vec3w, cv::Vec<3, char>, cv::Vec3s,
                         cv::Vec3i, cv::Vec3f, cv::Vec3d,
                         cv::Vec4b, cv::Vec4w, cv::Vec<4, char>, cv::Vec4s,
                         cv::Vec4i, cv::Vec4f, cv::Vec4d
                         ) MAT_TYPES_LIST;
#else
    // limited support of types !!
    typedef TYPELIST_2 (uchar, ushort) MAT_TYPES1D_LIST;
    typedef TYPELIST_2 (cv::Vec2b, cv::Vec2w) MAT_TYPES2D_LIST;
    typedef TYPELIST_2 (cv::Vec3b, cv::Vec3w) MAT_TYPES3D_LIST;
    typedef TYPELIST_2 (cv::Vec4b, cv::Vec4w) MAT_TYPES4D_LIST;
    typedef TYPELIST_8 (uchar, ushort, cv::Vec2b, cv::Vec2w, cv::Vec3b,
                        cv::Vec3w, cv::Vec4b, cv::Vec4w) MAT_TYPES_LIST;
#endif
}

#endif /* #ifndef TYPELIST_HPP */
