/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_P_H
#define OBJECT_P_H

#include "hasher.h"
#include "object.h"

namespace cl3ds
{
#define CL3DS_PUB_IMPL(Class) \
    public: \
        friend class Class; \
        inline Class *pubPtr() \
        { \
            return static_cast<Class *>(m_pubImpl); \
        } \
        inline const Class *pubPtr() const \
        { \
            return static_cast<const Class *>(m_pubImpl); \
        } \

    /** Use this macro to get the pimpl pointer using private getters. */
#define CL3DS_PRIV_PTR(Class)  Class::Private * const priv            = privPtr()
#define CL3DS_PRIV_CONST_PTR(Class) const Class::Private * const priv = privPtr()

    /** Use this macro to get the back pointer to public interface pointer from
     *  pimpl pointer. */
#define CL3DS_PUB_PTR(Class) Class * const pub = pubPtr()

    /** Abstract base class for an opaque implementation of an object. */
    struct CL3DS_DECLSPEC Object::AbstractPrivate
    {
        AbstractPrivate(Object *ptr);
        virtual ~AbstractPrivate();

        /** Serialization, deserialization and configuration from files
         *  and streams. */
        virtual bool read(const cv::FileNode &fn)     = 0;
        virtual bool write(cv::FileStorage &fs) const = 0;
        virtual bool configure(std::istream *is,
                               std::ostream *os) = 0;

        /** Hash representation. */
        virtual size_t hash() const = 0;

        Object *m_pubImpl;

        private:
            AbstractPrivate(const AbstractPrivate &) { }
            AbstractPrivate & operator=(const AbstractPrivate &) { return *this; }
    };
}

#endif // OBJECT_P_H

