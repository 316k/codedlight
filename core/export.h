/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Inspired by : OpenThreads library, Copyright (C) 2002 - 2007  The Open Thread Group
 */

/**
 * This header only declares macros useful for exporting symbols for MSVC
 */

#include "config.h"

#ifndef OS_WIN
    #define CL3DS_DECLSPEC
#else
    #if defined(CODEDLIGHT_STATIC_LIBRARY)
        #define CL3DS_DECLSPEC
    #elif defined(CODEDLIGHT_BUILDING)
        #define CL3DS_DECLSPEC __declspec(dllexport)
    #else
        #define CL3DS_DECLSPEC __declspec(dllimport)
    #endif
#endif
